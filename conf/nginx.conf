
# This NGINX configuration was made to serve the static content expecting that it will be fronted by
# CloudFront in the setup. This is not optimized to serve directly to users (ex. GZIP is off).

user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
  worker_connections  1024;
}


http {
  include /etc/nginx/mime.types;
  default_type  application/octet-stream;

  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
  '$status $body_bytes_sent "$http_referer" '
  '"$http_user_agent" "$http_x_forwarded_for"';

  access_log off;

  sendfile        on;
  tcp_nopush      on;
  tcp_nodelay     on;

  keepalive_timeout  65;

  #gzip  on; # since we front this with CloudFront which handles GZIP, we do not benefit from GZIP here.

  # ---- Caching Setup -----
  # Expires map
  map $sent_http_content_type $expires {
    default                    off;
    text/html                  -1;
    text/css                   max;
    application/javascript     max;
    ~image/                    max;
    ~font/                     max;
    ~font/                     max;
    ~video/                    max;
    ~audio/                    max;
  }

  server {
    listen       80;
    server_name  _;

    root   /usr/share/nginx/html;
    index  index.html index.htm;
    try_files $uri $uri /index.html =404;

    # redirect server error pages to the static page /50x.html
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
      root   /usr/share/nginx/html;
    }

    expires $expires;
  }
}
