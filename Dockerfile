FROM nginx:alpine
COPY dist/pixglo-studio /usr/share/nginx/html
COPY conf/nginx.conf /etc/nginx/nginx.conf

