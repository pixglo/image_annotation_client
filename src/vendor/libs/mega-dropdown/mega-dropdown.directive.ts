/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Directive, ElementRef, Input, OnDestroy, AfterViewInit, NgZone } from '@angular/core';

const MegaDropdown = require('./mega-dropdown.js').MegaDropdown;

@Directive({
  selector: '[megaDropdown]', // tslint:disable-line
  exportAs: 'megaDropdown'
})
export class MegaDropdownDirective implements OnDestroy, AfterViewInit {
  public megaDropdown;

  @Input() trigger = 'click';

  constructor(private el: ElementRef, private zone: NgZone) {
    this.el.nativeElement.classList.add('mega-dropdown');
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this.megaDropdown = new MegaDropdown(
        this.el.nativeElement.querySelector('.dropdown-toggle'),
        { trigger: this.trigger }
      );
    });
  }

  ngOnDestroy() {
    if (this.megaDropdown) {
      this.zone.runOutsideAngular(() => this.megaDropdown.destroy());
    }
    this.megaDropdown = null;
    this.el = null;
  }
}
