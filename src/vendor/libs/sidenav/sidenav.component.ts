/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {
  Component, ElementRef, Input, OnDestroy, NgZone, ChangeDetectorRef, AfterViewInit, HostBinding, Output, EventEmitter
} from '@angular/core';

import PerfectScrollbar from 'perfect-scrollbar';
const SideNav = require('./sidenav.js').SideNav;

@Component({
  selector: 'sidenav', // tslint:disable-line
  exportAs: 'sidenav',
  template: '<ng-content></ng-content>',
  styleUrls: ['../ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss']
})
export class SidenavComponent implements AfterViewInit, OnDestroy {
  public sidenav;

  @Input() orientation = 'vertical';
  @Input() animate = true;
  @Input() accordion = true;
  @Input() closeChildren = false;
  @Input() showDropdownOnHover = false;

  @Input() onOpen: Function;
  @Input() onOpened: Function;
  @Input() onClose: Function;
  @Input() onClosed: Function;

  @Output() destroy = new EventEmitter<SidenavComponent>();

  @HostBinding('class.sidenav') private hostClassMain = true;
  @HostBinding('class.sidenav-horizontal') private hostClassHorizontal = false;
  @HostBinding('class.sidenav-vertical') private hostClassVertical = false;

  constructor(private el: ElementRef, private zone: NgZone, private ref: ChangeDetectorRef) {
    this.ref.detach();

    // Set host classes
    this.hostClassHorizontal = this.orientation === 'horizontal';
    this.hostClassVertical = !this.hostClassHorizontal;
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this.sidenav = new SideNav(this.el.nativeElement, {
        orientation: this.orientation,
        animate: this.animate,
        accordion: this.accordion,
        closeChildren: this.closeChildren,
        showDropdownOnHover: this.showDropdownOnHover,

        onOpen: this.onOpen,
        onOpened: this.onOpened,
        onClose: this.onClose,
        onClosed: this.onClosed
      }, PerfectScrollbar);
    });
  }

  ngOnDestroy() {
    this.destroy.emit(this);
    if (this.sidenav) { this.zone.runOutsideAngular(() => this.sidenav.destroy()); }
    this.sidenav = null;
    this.el = null;
  }
}
