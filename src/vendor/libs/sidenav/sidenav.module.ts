/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SidenavComponent } from './sidenav.component';
import { SidenavLinkComponent } from './sidenav-link.component';
import { SidenavRouterLinkComponent } from './sidenav-router-link.component';
import { SidenavMenuComponent } from './sidenav-menu.component';
import { SidenavBlockComponent } from './sidenav-block.component';
import { SidenavDividerComponent } from './sidenav-divider.component';
import { SidenavHeaderComponent } from './sidenav-header.component';

@NgModule({
  imports: [ CommonModule, RouterModule ],
  declarations: [
    SidenavComponent,
    SidenavLinkComponent,
    SidenavRouterLinkComponent,
    SidenavMenuComponent,
    SidenavBlockComponent,
    SidenavDividerComponent,
    SidenavHeaderComponent
  ],
  exports: [
    SidenavComponent,
    SidenavLinkComponent,
    SidenavRouterLinkComponent,
    SidenavMenuComponent,
    SidenavBlockComponent,
    SidenavDividerComponent,
    SidenavHeaderComponent
  ]
})
export class SidenavModule { }
