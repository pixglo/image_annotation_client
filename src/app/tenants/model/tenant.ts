/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export class Tenant {

  static from(obj): Tenant {
    const tenant = new Tenant();
    Object.assign(tenant, obj);
    return tenant;
  }

  constructor(
    public id?: number,
    public name?: string,
    public websiteUrl?: string,
    public email?: string,
    public contactPerson?: string,
    public contactNumber?: string,
    public regions? : [0],
    public active?: boolean,
    public logoUploaded?: boolean,
  ) { }
}
