/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Tenant } from './model/tenant';
import { Page } from '../shared/model/paging/page';
import { NotificationService } from '../shared/notification.service';
import { Media } from '../media/model/media';
import { Template } from '../templates/model/template';

@Injectable({
  providedIn: 'root'
})
export class TenantService {

  private readonly apiBase = '/v1/tenants';

  constructor(
    private httpClient: HttpClient,
    private notificationService: NotificationService
  ) { }

  async search(query?: string, type?: string, page?: number, size?: number, sort?: string): Promise<Page<Tenant>> {

    let params = new HttpParams();
    if (query) { params = params.set('query', query); }
    if (type) { params = params.set('type', type); }
    if (page) { params = params.set('page', page.toString()); }
    if (size) { params = params.set('size', size.toString()); }
    if (sort) { params = params.set('sort', sort); }

    console.log('Searching for tenants with params', params.toString());
    const response = await this.httpClient.get(this.apiBase, { params: params }).toPromise();
    return Page.build(response, Tenant.from);
  }

  async findById(tenantId: string): Promise<Tenant> {
    if (!tenantId) {
      throw new Error('Tenant ID is required for findById');
    }
    return <Tenant>await this.httpClient.get(`${this.apiBase}/${tenantId}`).toPromise();
  }

  async findByIds(tenantIds): Promise<any> {
    let params = new HttpParams();
    params = params.set('tenantIds', tenantIds.join(','));
    return this.httpClient.get(`${this.apiBase}`, { params: params }).toPromise();
  }

  create(tenant: Tenant): Promise<Tenant> {
    return this.httpClient.post(this.apiBase, tenant).toPromise();
  }

  update(tenant: Tenant): Promise<Tenant> {
    return this.httpClient.put(`${this.apiBase}/${tenant.id}`, tenant).toPromise();
  }

  /**
   * Update the tenant profile
   */
  async updateProfile(tenantProfile): Promise<void> {
    console.log(tenantProfile);
    await this.httpClient.put(`${this.apiBase}/${tenantProfile.id}/profile`, tenantProfile).toPromise();
  }

  async activate(tenant: Tenant): Promise<void> {
    const message = 'tenant.activate.confirm';
    const messageValues = { name: tenant.name };
    await this.notificationService.showYesNoConfirmation(message, messageValues).then(async () => {
      await this.updateActivation(tenant, true);
      this.notificationService.showSuccess('tenant.activate.success', messageValues);
    });
  }

  async deactivate(tenant: Tenant): Promise<void> {
    const message = 'tenant.deactivate.confirm';
    const messageValues = { name: tenant.name };

    await this.notificationService.showYesNoConfirmation(message, messageValues).then(async () => {
      await this.updateActivation(tenant, false);
      this.notificationService.showSuccess('tenant.deactivate.success', messageValues);
    });
  }

  async del(tenant: Tenant): Promise<void> {
    const message = 'tenant.delete.confirm';
    const messageValues = { name: tenant.name };
    await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
      await this.httpClient.delete(`${this.apiBase}/${tenant.id}`).toPromise();
      this.notificationService.showSuccess('tenant.delete.success', { name: tenant.name });
    });
  }

  private async updateActivation(tenant: Tenant, state: boolean): Promise<void> {
    const payload = {
      active: state
    };
    await this.httpClient.put(`${this.apiBase}/${tenant.id}/active`, payload).toPromise();
  }

  async findTenantList(): Promise<Array<any>> {
    return <Array<any>>await this.httpClient.get(this.apiBase + '/list', {}).toPromise();
  }

  /**
   * Delete tenant logo.
   *
   * @param tenant
   */
  async deleteLogo(tenantId) {
    const message = 'tenant.logo.delete.confirm';
    const messageValues = 'Logo';

    await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
      await this.httpClient.delete(`${this.apiBase}/${tenantId}/logo`).toPromise();
      console.log('Logo Deleted for tenant', tenantId);
      this.notificationService.showSuccess('tenant.logo.delete.success', messageValues);
    });
  }

  getRegionsByTenant(tenantId): Promise<any> {
    return this.httpClient.get(`${this.apiBase}/${tenantId}/regions`).toPromise();
  }

  getRegionsByIdsAndTenant(regionIds, tenantId): Promise<any> {
    let params = new HttpParams();
    params = params.set('regionIds', regionIds.join(','));
    return this.httpClient.get(`${this.apiBase}/${tenantId}/regions`, { params: params }).toPromise();
  }

  async deleteRegionById(id, tenantId): Promise<void> {
    await this.httpClient.delete(`${this.apiBase}/${tenantId}/regions/${id}`).toPromise();
  }

  async deleteProfileRegion(id, tenantId) {
    const message = 'tenant.region.delete.confirm';

    await this.notificationService.showDeleteConfirmation(message).then(async () => {
      console.log('Deleting region');
      await this.deleteRegionById(id, tenantId);
      console.log('Region Deleted for tenant', tenantId);

      this.notificationService.showSuccess('tenant.region.delete.success');
    });
  }

}
