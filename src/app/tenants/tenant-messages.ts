/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
  'tenant.in.use.by.users' : 'Cannot delete this tenant as there are ${userCount} users that belong to this tenant.',
  'tenant.not.found' : 'Tenant (${value}) does not exist.',
  'tenant.name.already.exists' : 'Tenant name \'${value}\' is in use. Please use a different name.',

  'tenant.create.success' : 'Tenant was created successfully.',
  'tenant.create.success.title' : 'Tenant Created',
  'tenant.update.success' : 'Tenant was updated successfully.',
  'tenant.update.success.title' : 'Tenant Updated',

  'tenant.delete.confirm' : 'Are you sure to delete the tenant \'${name}\' ?',
  'tenant.delete.confirm.title' : 'Delete Tenant',
  'tenant.delete.success' : 'Tenant \'${name}\' was deleted successfully.',
  'tenant.delete.success.title' : 'Tenant Deleted',

  'tenant.activate.confirm' : 'Are you sure to activate tenant \'${name}\'?',
  'tenant.activate.confirm.title' : 'Activate Tenant',
  'tenant.activate.success' : 'Tenant \'${name}\' was activated successfully.',
  'tenant.activate.success.title' : 'Tenant Activated',

  'tenant.deactivate.confirm' : 'Are you sure to deactivate tenant "${name}"? <br /><br/> ' +
    'Note that users of this tenant will no longer be able to access the system.',
  'tenant.deactivate.confirm.title' : 'Deactivate Tenant',
  'tenant.deactivate.success' : 'Tenant \'${name}\' was deactivated successfully.',
  'tenant.deactivate.success.title' : 'Tenant Deactivated',

  'tenant.logo.delete.confirm' : "Are you sure to delete the tenant logo ?",
  'tenant.logo.delete.success' : "Tenant logo deleted",
  'tenant.logo.updated.success' : "Tenant logo updated successfully",

  'tenant.profile.update.success' : "Tenant profile update success",

  'tenant.region.delete.confirm' : "Are you sure to delete the region ?",
  'tenant.region.delete.success' : "Tenant region deleted successfully",

};
