/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutModule} from '../layout/layout.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {TenantsRoutingModule} from './tenants-routing.module';
import {TenantsListComponent} from './components/tenants-list/tenants-list.component';
import {TenantCreateComponent} from './components/tenant-create/tenant-create.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MessageService} from '../shared/message.service';
import {messages} from './tenant-messages';
import {TagInputModule} from 'ngx-chips';
import {DropzoneModule} from "ngx-dropzone-wrapper";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {TenantProfileComponent} from "./components/tenant-profile/tenant-profile.component";

@NgModule({
  declarations: [TenantsListComponent, TenantCreateComponent, TenantProfileComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,

    SharedModule,
    NgxDatatableModule,
    TagInputModule,
    DropzoneModule,
    ReactiveFormsModule,
    TenantsRoutingModule,
    NgbModule
  ]
})
export class TenantsModule {
  constructor(
    private messageService: MessageService
  ) {
    this.messageService.register(messages);
  }
}
