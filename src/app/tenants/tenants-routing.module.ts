/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Layout2Component} from '../layout/layout-2/layout-2.component';
import {TenantsListComponent} from './components/tenants-list/tenants-list.component';
import {TenantCreateComponent} from './components/tenant-create/tenant-create.component';
import {TenantProfileComponent} from "./components/tenant-profile/tenant-profile.component";

const routes: Routes = [
  {
    path: '',
    component: Layout2Component,
    children: [
      { path: '', component: TenantsListComponent, data: { permission: 'TENANT_VIEW' } },
      { path: 'create', component: TenantCreateComponent, data: { permission: 'TENANT_CREATE_OR_UPDATE' } },
      { path: 'edit/:id', component: TenantCreateComponent, data: { permission: 'TENANT_CREATE_OR_UPDATE' }},
      { path: ':id/profile', component: TenantProfileComponent, data: { permission: 'TENANT_PROFILE' }},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TenantsRoutingModule {
}
