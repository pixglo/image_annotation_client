/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TenantService} from '../../tenant.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {PageMetadata} from '../../../shared/model/paging/page-metadata';
import {Subject} from 'rxjs';
import {AppService} from '../../../app.service';
import {debounceTime, distinctUntilChanged, mergeMap} from 'rxjs/operators';
import {NotificationService} from '../../../shared/notification.service';
import {Tenant} from '../../model/tenant';

@Component({
  selector: 'app-tenants-list',
  templateUrl: './tenants-list.component.html',
  styleUrls: ['./tenants-list.component.scss']
})
export class TenantsListComponent implements OnInit, OnDestroy {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  loading = true;
  content = [];
  page = PageMetadata.empty(10, 'name');

  // Search
  searchQuery: string;
  searchTextChanged = new Subject<string>();
  searchSubscription;

  tenantType = "ALL";

  constructor(private appService: AppService,
              private tenantService: TenantService,
              private notificationService: NotificationService,
              private router: Router,
              private route: ActivatedRoute) {
    this.appService.setHeader('Tenants', ['Administration', 'Tenants']);
  }

  async ngOnInit() {
    // Debounce Search
    this.searchSubscription = this.searchTextChanged
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .pipe(mergeMap(search => this.search(this.page.page)))
      .subscribe(() => { });
    this.search();
  }

  ngOnDestroy(): void {
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
  }

  async search(page = 1) {
    this.loading = true;
    try {
      const pageResponse = await this.tenantService.search(this.searchQuery, this.tenantType, page, this.page.size,  this.page.sort);
      this.content = pageResponse.content;
      this.page = pageResponse.metadata;
    } catch (e) {
      // Something went wrong
    } finally {
      this.loading = false;
    }
  }

  async del(tenant: Tenant) {
    await this.tenantService.del(tenant);
    this.search();
  }

  edit(tenant: Tenant) {
    this.router.navigate(['edit', tenant.id], {relativeTo: this.route});
  }

  async activate(tenant: Tenant) {
    await this.tenantService.activate(tenant);
    this.search();
  }

  async deactivate(tenant: Tenant) {
    await this.tenantService.deactivate(tenant);
    this.search();
  }

  triggerSearch($event) {
    // Called by the search bar
    this.searchTextChanged.next($event.target.value);
  }

  changeTenantStatus(status) {
    if(this.tenantType === status) {
      return;
    } else {
      this.tenantType = status;
      this.search();

    }
  }
}
