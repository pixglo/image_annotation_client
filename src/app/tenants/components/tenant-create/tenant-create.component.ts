/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../../shared/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Tenant } from '../../model/tenant';
import { TenantService } from '../../tenant.service';
import { DropzoneComponent, DropzoneConfigInterface, DropzoneDirective } from 'ngx-dropzone-wrapper';
import { MediaService } from '../../../media/media.service';

@Component({
  selector: 'app-tenant-create',
  templateUrl: './tenant-create.component.html',
  styleUrls: ['./tenant-create.component.scss']
})
export class TenantCreateComponent implements OnInit {

  form: FormGroup;
  isEditMode = false;
  isFileRemoved = false;
  isLogoAdded = false;
  isLogoUploaded = false;
  currentTenant: Tenant; // In Edit Mode

  public config: DropzoneConfigInterface;
  @ViewChild(DropzoneDirective) directiveRef: DropzoneDirective;
  @ViewChild(DropzoneComponent) componentRef: DropzoneComponent;

  private apiBase = '/v1/tenants';
  private tenantId;
  private isFileAdded = false;
  private progress = 0;

  constructor(
    private appService: AppService,
    private tenantService: TenantService,
    private mediaService: MediaService,
    private notificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.form = this.buildForm();
  }

  async ngOnInit() {

    this.config = {
      url: `${this.apiBase}`,
      parallelUploads: 1,
      uploadMultiple: false,
      autoProcessQueue: false,
      maxFiles: 1,
      acceptedFiles: '.png,.jpg,.jpeg,.bmp',
      maxFilesize: this.mediaService.getMaxFileSizeDefaultMB(),
      filesizeBase: 1024,
      timeout: 60000 * 6,
      thumbnailWidth: 275,
      thumbnailHeight: 150,
      addRemoveLinks: false,
      previewTemplate: document.getElementById('asset-preview-template').innerHTML
    };

    // Check route to figure out the mode
    this.route.params.subscribe(params => {
      const tenantId = params['id'];
      this.tenantId = params['id'];
      if (tenantId) {
        this.activateEditMode(tenantId);
      } else {
        Object.assign(this.config, { previewTemplate: document.getElementById('asset-preview-template').innerHTML });
        this.activateCreateMode();
      }
    });

  }

  async save() {
    if (this.form.invalid) {
      return;
    }

    if (this.isEditMode) {
      await this.update();
    } else {
      await this.create();
    }
  }


  del() {
    this.tenantService.del(this.currentTenant).then(() => {
      this.router.navigate(['tenants']);
    });
  }

  async cancel() {
    if (this.form.dirty) {
      await this.notificationService.showChangesConfirmation();
      // This will not return if the user cancels the dialog
    }
    this.router.navigate(['/tenants']);
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      websiteUrl: [''],
      email: ['', Validators.email],
      contactPerson: [''],
      contactNumber: [''],
      active: [true]
    });
  }

  private async create() {
    const tenant = await this.tenantService.create(this.form.value);

    if (this.isFileAdded) {
      const dropzone = this.directiveRef.dropzone();
      dropzone.options.url = `${this.apiBase}/${tenant.id}/logo`;
      dropzone.processQueue();
    } else {
      this.notificationService.showSuccess('tenant.create.success');
      this.router.navigate(['tenants']);
    }

  }

  private async update() {
    const tenant = await this.tenantService.update(this.form.value);

    if (this.isFileAdded) {
      const dropzone = this.directiveRef.dropzone();
      dropzone.options.url = `${this.apiBase}/${tenant.id}/logo`;
      dropzone.processQueue();
    } else {
      this.notificationService.showSuccess('tenant.update.success');
      this.router.navigate(['tenants']);
    }
  }

  async activate() {
    await this.tenantService.activate(this.currentTenant);
    this.router.navigate(['tenants']);
  }

  async deactivate() {
    await this.tenantService.deactivate(this.currentTenant);
    this.router.navigate(['tenants']);
  }

  private activateCreateMode() {
    this.isEditMode = false;
    this.appService.setHeader('Tenants', ['Administration', 'Tenants', 'Create Tenant']);
  }

  private async activateEditMode(tenantId: string) {
    this.isEditMode = true;
    this.appService.setHeader('Tenants', ['Administration', 'Tenants', 'Edit Tenant']);

    // Load Tenant
    this.currentTenant = await this.tenantService.findById(tenantId);
    this.isLogoUploaded = this.currentTenant.logoUploaded;
    console.log('Loading tenant', this.currentTenant);
    this.form.patchValue(this.currentTenant);

  }

  uploadSuccess(event: any) {
    console.log('Tenant upload success');
    this.isFileAdded = false;
    if (!this.isEditMode) {
      this.notificationService.showSuccess('tenant.create.success');
      this.router.navigate(['tenants']);
    } else {
      this.notificationService.showSuccess('tenant.update.success');
      this.router.navigate(['tenants']);
    }
  }

  uploadProgress(event: any) {
    const progress = event[1];
    this.progress = progress;
  }

  onUploadError(e) {
    console.log('Error in uploading logo.');

    console.log(e);
    setTimeout(() => {
      this.progress = 0;
    }, 1000);
  }

  onFileAdded(e) {
    this.isFileAdded = true;
    console.log('File added.');
  }

  removeFile(e) {
    this.isFileAdded = false;
    console.log('File removed.');
  }

  async removeLogo() {
    await this.tenantService.deleteLogo(this.tenantId);
    this.isLogoAdded = false;
    this.isFileRemoved = true;
    this.isLogoAdded = false;
  }

}
