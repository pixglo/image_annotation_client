/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../../shared/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Tenant } from '../../model/tenant';
import { TenantService } from '../../tenant.service';
import { DropzoneComponent, DropzoneConfigInterface, DropzoneDirective } from 'ngx-dropzone-wrapper';
import { MediaService } from '../../../media/media.service';

@Component({
  selector: 'app-tenant-profile',
  templateUrl: './tenant-profile.component.html',
  styleUrls: ['./tenant-profile.component.scss']
})
export class TenantProfileComponent implements OnInit {

  form: FormGroup;
  isEditMode = false;
  isFileRemoved = false;
  isLogoAdded = false;
  isLogoUploaded = false;
  tenantId = undefined;
  tenant: Tenant;

  isRegionFormValid = false;

  public config: DropzoneConfigInterface;
  @ViewChild(DropzoneDirective) directiveRef: DropzoneDirective;
  @ViewChild(DropzoneComponent) componentRef: DropzoneComponent;

  private apiBase = '/v1/tenants';

  private isFileAdded = false;
  private progress = 0;

  constructor(
    private appService: AppService,
    private tenantService: TenantService,
    private mediaService: MediaService,
    private notificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.form = this.buildForm();
  }

  async ngOnInit() {
    this.config = {
      url: `${this.apiBase}`,
      parallelUploads: 1,
      uploadMultiple: false,
      autoProcessQueue: false,
      maxFiles: 1,
      acceptedFiles: '.png,.jpg,.jpeg,.bmp',
      maxFilesize: this.mediaService.getMaxFileSizeDefaultMB(),
      filesizeBase: 1024,
      timeout: 60000 * 6,
      thumbnailWidth: 275,
      thumbnailHeight: 150,
      addRemoveLinks: false,
      previewTemplate: document.getElementById('asset-preview-template').innerHTML
    };

    this.appService.setHeader('Tenants', ['Tenant Profile', 'Update']);

    const authUser = JSON.parse(localStorage.getItem('app-auth-user'));

    if (authUser.tenantId !== null) {
      console.log(`Load the tenant profile for tenant id : ${authUser.tenantId}`);
      this.tenantId = authUser.tenantId;
      await this.loadTenantProfile();
    }

  }

  /**
   * Load the Tenant profile to edit
   */
  async loadTenantProfile() {
    console.log('Loading tenant profile..');
    const [tenant, regions] = await Promise.all([
      this.tenantService.findById(this.tenantId),
      this.tenantService.getRegionsByTenant(this.tenantId)
    ]);

    this.tenant = tenant;
    this.isLogoUploaded = this.tenant.logoUploaded;
    console.log(`Loading tenant : ${this.tenant}`);
    this.form.patchValue(this.tenant);

    if (regions !== undefined) {
      if (regions.length > 1) {
        for (let i = 0; i < regions.length - 1; i++) {
          this.addRegion();
        }
      }

      const tempRegions = regions.map(function (region) {
        return {
          id: region.id,
          name: region.name
        };
      });

      this.form.controls['regions'].patchValue(tempRegions);
    }
  }

  async save() {
    if (this.form.invalid) {
      return;
    }

    await this.tenantService.updateProfile(this.form.value);

    if (this.isFileAdded) {
      const dropzone = this.directiveRef.dropzone();
      dropzone.options.url = `${this.apiBase}/${this.form.value.id}/logo`;
      dropzone.processQueue();
    } else {
      this.notificationService.showSuccess('tenant.profile.update.success');
      this.router.navigate(['home']);
    }
  }

  async cancel() {
    if (this.form.dirty) {
      await this.notificationService.showChangesConfirmation();
    }
    this.router.navigate(['/home']);
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      websiteUrl: [''],
      email: ['', Validators.email],
      contactPerson: [''],
      contactNumber: [''],
      active: [true],
      regions: this.formBuilder.array([this.createRegion()])
    });
  }

  private createRegion(id?, name?) {
    return this.formBuilder.group({
      id: '',
      name: ['', Validators.required],
    });
  }

  public addRegion(id?, name?) {
    (this.form.controls['regions'] as FormArray).push(this.createRegion(id, name))
  }

  private async update() {
    const tenant = await this.tenantService.update(this.form.value);

    if (this.isFileAdded) {
      const dropzone = this.directiveRef.dropzone();
      dropzone.options.url = `${this.apiBase}/${tenant.id}/logo`;
      dropzone.processQueue();
    } else {
      this.notificationService.showSuccess('tenant.profile.update.success');
      this.router.navigate(['home']);
    }
  }

  uploadSuccess(event: any) {
    console.log('Logo upload success');
    this.isFileAdded = false;
    this.notificationService.showSuccess('tenant.profile.update.success');
    this.router.navigate(['home']);
  }

  uploadProgress(event: any) {
    const progress = event[1];
    this.progress = progress;
  }

  onUploadError(e) {
    console.log('Error in uploading logo');
    setTimeout(() => {
      this.progress = 0;
    }, 1000);
  }

  onFileAdded(e) {
    this.isFileAdded = true;
    this.form.markAsDirty();
    console.log('File added.');
  }

  removeFile(e) {
    this.isFileAdded = false;
    console.log('File removed.');
  }

  async removeLogo() {
    await this.tenantService.deleteLogo(this.tenantId);
    this.isLogoAdded = false;
    this.isFileRemoved = true;
    this.isLogoAdded = false;
  }

  async removeRegion(index) {
    const control = <FormArray>this.form.controls['regions'];
    const selectedRegionId = this.form.controls['regions'].value[index].id;
    if (selectedRegionId !== undefined && selectedRegionId.trim().length > 0) {
      await this.tenantService.deleteProfileRegion(selectedRegionId, this.tenantId);
    }
    control.removeAt(index);
  }

  get formData() { return <FormArray>this.form.get('regions'); }

}
