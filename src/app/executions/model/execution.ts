/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export class Execution {

  static from(obj): Execution {
    const execution = new Execution();
    Object.assign(execution, obj);
    return execution;
  }

  constructor(
    id?: string,
    type?: string,
    tenantId?: string,
    tenantName?: string,
    dockerImage?: string,
    dockerImageTag?: string,
    args?: string,
    sourceRefId?: string,
    status?: string,
    duration?: string,
    kubernetesMetadata?: any
  ) { }

}
