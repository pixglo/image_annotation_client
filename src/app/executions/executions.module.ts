/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExecutionListComponent} from './components/execution-list/execution-list.component';
import {ExecutionsRoutingModule} from './executions-routing.module';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {LayoutModule} from '../layout/layout.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MessageService} from '../shared/message.service';
import {messages} from './executions-messages';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    ExecutionListComponent,
  ],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    SharedModule,
    ExecutionsRoutingModule,
    NgxDatatableModule,
    NgbModule,
    NgSelectModule
  ],
  entryComponents: []
})

export class ExecutionsModule {
  constructor(
    private messageService: MessageService
  ) {
    this.messageService.register(messages);
  }
}
