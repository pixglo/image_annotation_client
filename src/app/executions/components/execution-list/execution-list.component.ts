/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';
import { ExecutionService } from '../../execution.service';
import { ExecutionSearchParams } from '../../model/execution-search-params';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { TenantService } from '../../../tenants/tenant.service';

@Component({
  selector: 'app-execution-list',
  templateUrl: './execution-list.component.html',
  styleUrls: [
    './execution-list.component.scss'
  ]
})
export class ExecutionListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  content = [];
  executionTypes = [];
  executionStatuses = [];
  page = PageMetadata.empty(10);
  searchQuery: string;
  searchTextChanged = new Subject<string>();
  searchSubscription;
  advanceFiltersEnabled = false;
  isLoadingContent = false;
  model: NgbDateStruct;
  hoveredDate: NgbDateStruct;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  currentPeriodSelection = '';
  relativeFilterOptions = [];
  executionSearchParams: ExecutionSearchParams;
  tenants = [];

  constructor(
    private appService: AppService,
    private executionService: ExecutionService,
    private tenantService: TenantService,
    private router: Router,
    private calendar: NgbCalendar
  ) {
    this.appService.setHeader('Executions', ['Execution Management', 'Executions']);
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getToday();
    this.executionSearchParams = new ExecutionSearchParams();
  }

  async ngOnInit() {
    this.relativeFilterOptions = this.executionService.getRelativeFilterOptions();

    const [types, statuses] = await Promise.all([
      this.executionService.getExecutionTypes(),
      this.executionService.getExecutionStatuses()
    ]);

    this.executionTypes = types;
    this.executionStatuses = statuses;

    // Debounce Search
    this.searchSubscription = this.searchTextChanged
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .pipe(mergeMap(search => this.search(this.page.page)))
      .subscribe(() => {
      });
    this.search();
  }

  async search(page = 1) {
    this.isLoadingContent = false;
    try {
      const pageResponse = await this.executionService.search(this.executionSearchParams, page, this.page.size);

      // Get tenants
      this.tenants = await this.tenantService.findByIds(this.getUniqueTenantIds(pageResponse.content));

      this.content = pageResponse.content.map((exec: any) => {
        const tenantName = this.tenants.find(tenant => exec.tenantId === tenant.id).name || 'N/A';
        return Object.assign({ tenantName }, exec);
      });

      this.page = pageResponse.metadata;
    } catch (err) {
      console.log(err);
    } finally {
      this.isLoadingContent = false;
    }
  }

  getUniqueTenantIds(executionArr) {
    const tenantIdArr = [];
    const map = new Map();

    for (const item of executionArr) {
      if (!map.has(item.tenantId)) {
        map.set(item.tenantId, true);
        tenantIdArr.push(item.tenantId);
      }
    }

    return tenantIdArr;
  }

  loadPage(e) {
    this.search(e.offset + 1);
  }

  triggerSearch(e) {
    this.searchTextChanged.next(e.target.value);
  }

  /**
   * Show / hde advance filter options
   * @param enabled
   */
  toggleAdvanceFilters(enabled: boolean) {
    this.advanceFiltersEnabled = enabled;
    if (!enabled) {
      this.resetPeriodFilter();
      // Note: ng-selects get reset automatically when component is hidden,
      // therefore there is no need for an explicit reset.
    }
  }

  /**
   * Reset datepicker
   */
  resetDatePicker() {
    this.fromDate = this.toDate = this.calendar.getToday();
  }

  /**
   * Reset whole period filter
   */
  resetPeriodFilter() {
    this.resetDatePicker();
    this.executionSearchParams.lastUpdatedFrom = null;
    this.executionSearchParams.lastUpdatedTo = null;
    this.currentPeriodSelection = '';
    this.search(this.page.page);
  }

  /**
   * Triggered on relative dates value change
   * @param option
   */
  onRelativeOptionChange(option: any) {
    this.currentPeriodSelection = option.label;
    this.resetDatePicker();

    this.executionSearchParams.lastUpdatedFrom = this.executionService.getIsoDateStr(undefined, undefined, option.mins);
    this.executionSearchParams.lastUpdatedTo = this.executionService.getIsoDateStr(undefined, true);

    this.search(this.page.page);
  }

  /**
   * Trigerred on datepicker value change
   * @param date
   */
  onDateChange(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && this.executionService.isDateAfter(date, this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    const fromDateStr = this.executionService.getDateStringFromNgbDateStruct(this.fromDate);
    const toDateStr = this.executionService.getDateStringFromNgbDateStruct(this.toDate);

    this.currentPeriodSelection = `${fromDateStr}${this.toDate ? ' - ' + toDateStr : ''}`;

    this.executionSearchParams.lastUpdatedFrom =
      this.executionService.getIsoDateStr(this.executionService.getMomentDateFromDateStr(fromDateStr));

    // toDate can be null since we might not always have a range
    this.executionSearchParams.lastUpdatedTo =
      toDateStr ? this.executionService.getIsoDateStr(this.executionService.getMomentDateFromDateStr(toDateStr), true) : null;

    this.search(this.page.page);
  }

  /**
   * Datepicker functions below
   */
  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate
    && this.executionService.isDateAfter(date, this.fromDate)
    && this.executionService.isDateBefore(date, this.hoveredDate);

  isInside = date => this.executionService.isDateAfter(date, this.fromDate) && this.executionService.isDateBefore(date, this.toDate);

  isFrom = date => this.executionService.isDateEqual(date, this.fromDate);

  isTo = date => this.executionService.isDateEqual(date, this.toDate);
}
