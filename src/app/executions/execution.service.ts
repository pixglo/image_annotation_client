/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Page } from '../shared/model/paging/page';
import { Execution } from './model/execution';
import { ExecutionSearchParams } from './model/execution-search-params';
import { Subscription } from 'rxjs';
import { AppService } from '../app.service';
import { AppConfig } from '../shared/model/config/app-config';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from'moment';


@Injectable({
  providedIn: 'root'
})
export class ExecutionService implements OnDestroy {
  private readonly executionsApi = '/v1/executions';
  appConfigSubscription: Subscription;
  config = new AppConfig();

  constructor(
    private appService: AppService,
    private httpClient: HttpClient
  ) {
    this.appConfigSubscription = this.appService.appConfig$.subscribe((config) => {
      this.config = config;
    });
  }

  async search(searchParams: ExecutionSearchParams, page?: number, size?: number, sort?: string): Promise<Page<Execution>> {
    let params = new HttpParams();
    if (searchParams.term) { params = params.set('term', searchParams.term); }
    if (searchParams.types) { params = params.set('types', searchParams.types.join(',')); }
    if (searchParams.statuses) { params = params.set('statuses', searchParams.statuses.join(',')); }
    if (searchParams.lastUpdatedFrom) { params = params.set('lastUpdatedFrom', searchParams.lastUpdatedFrom); }
    if (searchParams.lastUpdatedTo) { params = params.set('lastUpdatedTo', searchParams.lastUpdatedTo); }
    if (page) { params = params.set('page', page.toString()); }
    if (size) { params = params.set('size', size.toString()); }
    if (sort) { params = params.set('sort', sort); }

    console.log('Searching for executions with params', params.toString());
    const response = await this.httpClient.get(this.executionsApi, { params: params }).toPromise();
    return Page.build(response, Execution.from);
  }

  async getExecutionTypes(): Promise<Array<any>> {
    return <Array<any>>await this.httpClient.get(`${this.executionsApi}/types`, {}).toPromise();
  }

  async getExecutionStatuses(): Promise<Array<any>> {
    return <Array<any>>await this.httpClient.get(`${this.executionsApi}/statuses`, {}).toPromise();
  }

  getRelativeFilterOptions(): Array<Object> {
    return [
      { label: 'Last 15 Minutes', mins: 15 },
      { label: 'Last 30 Minutes', mins: 30 },
      { label: 'Last 3 Hours', mins: 180 },
      { label: 'Last 6 Hours', mins: 360 },
      { label: 'Last 24 Hours', mins: 1440 },
      { label: 'Last 3 Days', mins: 4320 },
      { label: 'Last 7 Days', mins: 10080 },
      { label: 'Last Month', mins: 43200 },
      { label: 'Last 3 Months', mins: 129600 }
    ];
  }

  getIsoDateStr(momentDate = moment.utc(), isEOD = false, minutes = 0) {
    let date = momentDate.subtract(minutes, 'minutes');

    // If an "End of Day" date is required (usually requires for toDate)
    if (isEOD) {
      date = date.endOf('day');
    }

    return date.toISOString();
  }

  getMomentDateFromDateStr(date: string) {
    return moment.utc(date, 'YYYY-MM-DD');
  }

  getDateStringFromNgbDateStruct(date: NgbDateStruct): string {
    return date ? `${date.year}/${date.month}/${date.day}` : null;
  }

  isDateEqual = (date1: NgbDateStruct, date2: NgbDateStruct) =>
    date1 && date2 && date2.year === date1.year && date2.month === date1.month && date2.day === date1.day;

  isDateBefore = (date1: NgbDateStruct, date2: NgbDateStruct) =>
    !date1 || !date2 ? false : date1.year === date2.year ? date1.month === date2.month ? date1.day === date2.day
      ? false : date1.day < date2.day : date1.month < date2.month : date1.year < date2.year;

  isDateAfter = (date1: NgbDateStruct, date2: NgbDateStruct) =>
    !date1 || !date2 ? false : date1.year === date2.year ? date1.month === date2.month ? date1.day === date2.day
      ? false : date1.day > date2.day : date1.month > date2.month : date1.year > date2.year;

  ngOnDestroy(): void {
    if (this.appConfigSubscription) {
      this.appConfigSubscription.unsubscribe();
    }
  }
}
