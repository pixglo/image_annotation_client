/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../../../app.service';
import { NotificationService } from '../../../shared/notification.service';
import { TagService } from '../../tags.service';
import { Tag } from '../../model/tags';
import { ValidateEmpty } from '../../../validators/empty.validator';

@Component({
    selector: 'app-tags-create',
    templateUrl: './tags-create.component.html',
    styleUrls: ['./tags-create.component.scss']
})
export class TagsCreateComponent implements OnInit {

    tagForm: FormGroup;
    isEditMode = true;
    currentTag: Tag;
    objSelections = { parent: [] };
    objReceived = { parent: {} };
    constructor(
        private appService: AppService,
        private tagService: TagService,
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService,
        private formBuilder: FormBuilder
    ) {
        this.tagForm = this.buildForm();
    }

    async ngOnInit() {
        // Note: We await here so that we have all the dependant data loaded before subscribing to route
        // which triggers the edit mode
        // Check route to figure out the mode
        this.route.params.subscribe(params => {
            const tagId = params['id'];
            if (tagId) {
                this.activateEditMode(tagId);
            } else {
                this.activateCreateMode();
            }
        });
        this.getSelections('');
    }

    private activateCreateMode(): void {
        this.isEditMode = false;
        this.appService.setHeader('Tag Management', ['Image Management', 'Tag Management', 'Create Tag']);
    }

    private async activateEditMode(id: any) {
        this.isEditMode = true;
        this.appService.setHeader('Tag Management', ['Image Management', 'Tag Management', 'Edit Tag']);

        // Load Tag
        const getRecordData = <Tag>await this.tagService.findById(id);
        this.currentTag = getRecordData['item'];
        console.log('Loading Tag', this.currentTag);
        this.tagForm.patchValue(this.currentTag);

    }

    private buildForm(): FormGroup {
        return this.formBuilder.group({
            name: ['', [Validators.required, ValidateEmpty]],
            parent: ['']
        });
    }

    async save() {
        if (this.tagForm.invalid) {
            return;
        }

        if (this.isEditMode) {
            await this.update();
        } else {
            await this.create();
        }
    }

    private async create() {
        await this.tagService.create(this.tagForm.value);
        this.router.navigate(['tags']);
    }

    private async update() {
        await this.tagService.update(Object.assign({ _id: this.currentTag['_id'] }, this.tagForm.value));
        this.router.navigate(['tags']);
    }

    async del() {
        await this.tagService.del(this.currentTag);
        this.router.navigate(['tags']);
    }

    async cancel() {
        if (this.tagForm.dirty) {
            await this.notificationService.showChangesConfirmation();
        }
        this.router.navigate(['/tags']);
    }

    getSelections(text: string): void {
        const params = { q: text, sortBy: 'name' };
        // params['parent'] = this.formData.client || undefined;

        this.tagService.getList(params).subscribe((data: any) => {
            this.objSelections['parent'] = data.list;
        });
    }

    /*!
  * event: For selected item
  * frmField: input field item
  * type: user types
  */
    selectedField(event): void {
        /*if (event.length > 0 && this.formData[frmField] !== event[0].data._id) {
          this.formData[frmField] = event[0].data._id;
          if (eleRef !== 'client-users') {
            this.objReceived[eleRef] = event[0].data;
          }
          switch (eleRef) {
            case 'clients':
              this.objSelections['plantManagers'] = [];
              this.objSelections['plantUsers'] = [];
              this.formData['plantManager'] = null;
              this.formData['plantUser'] = null;
              break;
            case 'client-users':
              if (type === 'PM') {
                this.objReceived['plantManagers'] = event[0].data;
              } else if (type === 'PU') {
                this.objReceived['plantUsers'] = event[0].data;
              }
              break;
          }
        }*/
        console.log(this.tagForm.controls);
    }

    // Use for return the array - merge two array and removed duplicate
    uniqueArray(a, b) {
        if (a && b && a.length === 0 && b.length === 0) {
            return [];
        } else if (a && a.length === 0) {
            return b;
        } else if (b && b.length === 0) {
            return a;
        }

        const temp = a.filter((el) => {
            return el._id === b[0]._id;
        });

        if (temp.length === 0) { a.push(b[0]); }
        return a;
    }

    get preAvailableParent(): any {
        return [this.objReceived.parent];
    }
}
