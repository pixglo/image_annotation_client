/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { Subject } from 'rxjs';
import { AppService } from '../../../app.service';
import { NotificationService } from '../../../shared/notification.service';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { TagService } from '../../tags.service';
import { Tag } from '../../model/tags';

@Component({
    selector: 'app-tags-list',
    templateUrl: './tags-list.component.html',
    styleUrls: ['./tags-list.component.scss']
})
export class TagsListComponent implements OnInit, OnDestroy {

    @ViewChild(DatatableComponent) table: DatatableComponent;

    currentTagId;
    loading = true;
    content = [];
    page = PageMetadata.empty(10, 'createdAt');

    // Search
    searchQuery: string;
    searchTextChanged = new Subject<string>();
    searchSubscription;

    filterFields = [
        {
            label: 'Name', val: 'name', isFilterable: true, elementType: 'text'
        },
        {
            label: 'Parent', val: 'parent', isFilterable: true, elementType: 'select', elementRef: 'tags'
        },
        {
            label: 'Created On', val: 'createdAt', isFilterable: true, elementType: 'dateRange'
        }
    ];

    constructor(private appService: AppService,
        private tagService: TagService,
        private router: Router,
        private route: ActivatedRoute) {
        this.appService.setHeader('Tag Management', ['Image Management', 'Tag Management']);
    }

    async ngOnInit() {

        // Debounce Search
        this.searchSubscription = this.searchTextChanged
            .pipe(debounceTime(500))
            .pipe(distinctUntilChanged())
            .pipe(mergeMap(search => this.search(this.page.page)))
            .subscribe(() => {
            });
        this.search();
    }

    ngOnDestroy() {
        if (this.searchSubscription) {
            this.searchSubscription.unsubscribe();
        }
    }
    // Handler: On sort
    onSort($event){
        // Preserve paging during sorting
        this.table.offset = +this.page.page - 1;
    }

    async search(page = 1, isSearch = false, objFilter = {}) {
        this.loading = true;
        try {
            const pageResponse = await this.tagService.search(this.searchQuery, page, this.page.size, this.page.sort, isSearch, objFilter);
            this.content = pageResponse.content;
            this.page = pageResponse.metadata;
        } catch (err) {
            console.log(err);
        } finally {
            this.loading = false;
        }
    }

    async del(tag: Tag) {
        await this.tagService.del(tag);
        this.search();
    }

    edit(tag: Tag) {
        this.router.navigate(['edit', tag._id], { relativeTo: this.route });
    }

    triggerSearch($event) {
        // Called by the search bar
        this.searchTextChanged.next($event.target.value);
    }

    /*Function to receive response emitted by filter*/
    public receiveFilterResponse(data: any): void {
        this.search(this.page.page, true, data);
    }
}
