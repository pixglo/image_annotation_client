/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Tag } from './model/tags';
import { Page } from '../shared/model/paging/page';
import { NotificationService } from '../shared/notification.service';

@Injectable({
    providedIn: 'root'
})
export class TagService {

    private apiBase = '/api/tags';

    constructor(private httpClient: HttpClient, private notificationService: NotificationService) { }

    async create(tag: Tag) {
        await this.httpClient.post(`${this.apiBase}` + '/create', tag).toPromise();
        console.log('Created tag', tag);
        this.notificationService.showSuccess('tag.create.success');
    }

    async update(tag: Tag) {
        await this.httpClient.post(`${this.apiBase}/update/${tag._id}`, tag).toPromise();
        console.log('Updated tag', tag);
        this.notificationService.showSuccess('tag.update.success');
    }

    async del(tag: Tag) {
        const message = 'tag.delete.confirm';
        const messageValues = { name: `${tag.name}` };
        await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
            console.log('Deleting', tag);
            await this.httpClient.post(`${this.apiBase}/delete/${tag._id}`, {}).toPromise();
            console.log('Deleted', tag);
            this.notificationService.showSuccess('tag.delete.success', messageValues);
        });
    }

    async findById(recordId: string) {
        if (!recordId) {
            throw new Error('Tag ID is required for findById');
        }
        return this.httpClient.post(`${this.apiBase}/get/${recordId}`, {}).toPromise();
    }

    async search(query?: string, page?: number, size?: number, sort?: string, isSearch?: boolean, objFilter?: Object): Promise<Page<Tag>> {

        // let params = new HttpParams();
        const queryParam = {
            params: {
                query: '',
                page: '',
                size: '',
                sort: ''
            },
            filter: {}
        };
        if (query) { queryParam.params.query = query; } else { queryParam.params.query = undefined; }
        if (page) { queryParam.params.page = page.toString(); } else { queryParam.params.page = undefined; }
        if (size) { queryParam.params.size = size.toString(); } else { queryParam.params.size = undefined; }
        if (sort) { queryParam.params.sort = sort; } else { queryParam.params.sort = undefined; }
        if (isSearch) { queryParam.filter = objFilter; } else { queryParam.filter = undefined; }

        console.log('Searching for tag with params', queryParam);
        const response = await this.httpClient.post(`${this.apiBase}` + '/list', queryParam).toPromise();
        return Page.build(response, Tag.from);
    }

    getList(params) {
        return this.httpClient.post(`${this.apiBase}/search`, params);
    }

}
