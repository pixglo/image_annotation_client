/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Layout2Component } from '../layout/layout-2/layout-2.component';
import { TagsCreateComponent } from './components/tags-create/tags-create.component';
import { TagsListComponent } from './components/tags-list/tags-list.component';
import { INgxSelectOptions, NgxSelectModule } from 'ngx-select-ex';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const CustomSelectOptions: INgxSelectOptions = { // Check the interface for more options
    optionValueField: '_id',
    optionTextField: 'name',
    keepSelectedItems: true
};

const routes: Routes = [
    {
        path: '',
        component: Layout2Component,
        children: [
            { path: '', component: TagsListComponent, data: { permission: 'TAGS_VIEW' } },
            { path: 'create', component: TagsCreateComponent, data: { permission: 'TAGS_CREATE_OR_UPDATE' } },
            { path: 'edit/:id', component: TagsCreateComponent, data: { permission: 'TAGS_CREATE_OR_UPDATE' } },
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        NgxSelectModule.forRoot(CustomSelectOptions),
        FormsModule
    ],
    exports: [
        RouterModule, NgxSelectModule, FormsModule
    ]
})
export class TagsRoutingModule {
}
