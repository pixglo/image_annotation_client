/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
    'tag.not.found': 'Tag (${value}) does not exist.',
    'tag.name.already.exists': 'Tag name "${value}" is in use. Please use a different name.',

    'tag.create.success': 'Tag was created successfully.',
    'tag.create.success.title': 'Tag Created',
    'tag.update.success': 'Tag was updated successfully.',
    'tag.update.success.title': 'Tag Updated',

    'tag.delete.confirm': 'Are you sure to delete "${name}" ?',
    'tag.delete.confirm.title': 'Delete Tag',
    'tag.delete.success': 'Tag "${name}" was deleted successfully.',
    'tag.delete.success.title': 'Tag Deleted'
};
