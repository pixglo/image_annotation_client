/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

 import { Scene } from './scene';

export class Media {

  static from(obj): Media {
    const media = new Media();
    Object.assign(media, obj);
    return media;
  }

  constructor(
    public id?: string,
    public tenantId?: string,
    public fileName?: string,
    public name?: string,
    public mediaType?: string,
    public fileSizeBytes?: string,
    public sha1Hash?: string,
    public createdAt?: string,
    public createdBy?: string,
    public frameCount?: string,
    public bitrate?: string,
    public resolution?: string,
    public durationMs?: string,
    public status?: string,
    public preprocessProgress?: string,
    public preprocessedOn?: string,
    public annotatedOn?: string,
    public videoMetadata?: any,
    public categoryTypes?: any[],
    public scenes?: Scene[]
  ) { }
}
