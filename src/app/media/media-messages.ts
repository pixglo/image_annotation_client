/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
  // Error messages
  'media.file.not.found': 'Media file does not exist.',
  'media.content.upload.failed': 'File upload failed',
  'media.content.delete.failed': 'File delete failed',

  // Success messages
  'media.create.success': 'File was uploaded successfully.',

  'media.delete.confirm': 'This action will also delete any associated rendering Jobs, are you sure you want to delete "${name}"',
  'media.delete.confirm.title' : 'Delete Media',
  'media.delete.success' : 'Media "${name}" was deleted successfully.',
  'media.delete.success.title' : 'Media Deleted',
};
