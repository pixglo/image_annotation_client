/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaCreateComponent } from './components/media-create/media-create.component';
import { MediaListComponent } from './components/media-list/media-list.component';
import { MediaPreviewComponent } from './components/media-preview/media-preview.component';
import { MediaRoutingModule } from './media-routing.module';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { LayoutModule } from '../layout/layout.module';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';
import { MessageService } from '../shared/message.service';
import { messages } from './media-messages';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    MediaCreateComponent,
    MediaListComponent,
    MediaPreviewComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    SharedModule,
    MediaRoutingModule,
    DropzoneModule,
    NgxDatatableModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    NgbModule,
    NgSelectModule
  ],
  entryComponents: [
    MediaPreviewComponent
  ]
})

export class MediaModule {
  constructor(
    private messageService: MessageService
  ) {
    this.messageService.register(messages);
  }
}
