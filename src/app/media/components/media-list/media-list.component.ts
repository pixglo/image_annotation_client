/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';
import { MediaService } from '../../media.service';
import { Media } from '../../model/media';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MediaPreviewComponent } from '../media-preview/media-preview.component';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-media-list',
  templateUrl: './media-list.component.html',
  styleUrls: [
    './media-list.component.scss'
  ]
})
export class MediaListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  videoPlayer: any;
  loading = true;
  content = [];
  page = PageMetadata.empty(10);
  searchQuery: string;
  searchTextChanged = new Subject<string>();
  searchSubscription;
  isTileView = false;

  constructor(
    private appService: AppService,
    private mediaService: MediaService,
    private router: Router,
    private modalService: NgbModal,
  ) {
    this.appService.setHeader('Media', ['Content Management', 'Media']);
  }

  ngOnInit() {
    // Debounce Search
    this.searchSubscription = this.searchTextChanged
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .pipe(mergeMap(search => this.search(this.page.page)))
      .subscribe(() => {
      });
    this.search();
  }

  async search(page = 1) {
    this.loading = true;
    try {
      const pageResponse = await this.mediaService.search(this.searchQuery, page, this.page.size);
      this.content = pageResponse.content;
      this.page = pageResponse.metadata;
    } catch (err) {
      console.log(err);
    } finally {
      this.loading = false;
    }
  }

  loadPage(evt) {
    this.search(evt.offset + 1);
  }

  triggerSearch($event) {
    this.searchTextChanged.next($event.target.value);
  }

  previewMediaContent(media: Media) {
    const modalRef = this.modalService.open(MediaPreviewComponent, { size: 'lg', windowClass: 'modal-fill-in modal-lg' });
    modalRef.componentInstance.mediaFileInput = media;
  }

  editMediaContent(media: Media) {
    this.router.navigate([`/media/edit/${media.id}`]);
  }

  switchView(view = 'grid') {
    this.isTileView = view === 'tile';
  }

  onPageChange(page) {
    this.search(page);
  }

  async delete(media: Media) {
    await this.mediaService.delete(media);
    this.search();
  }

}
