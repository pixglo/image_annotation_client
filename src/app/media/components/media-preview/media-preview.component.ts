/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../../app.service';
import { MediaService } from '../../media.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Media } from '../../model/media';

@Component({
  selector: 'app-media-preview',
  templateUrl: './media-preview.component.html',
  styleUrls: [
    './media-preview.component.scss'
  ]
})
export class MediaPreviewComponent implements OnInit {
  @Input() mediaFileInput: Media;
  mediaFile: Media;
  contentSource: string;
  isPreviewSupported = true;

  constructor(
    private appService: AppService,
    private mediaService: MediaService,
    public activeModal: NgbActiveModal
  ) {
    this.appService.setHeader('Media', ['Content Management', 'Media']);
  }

  ngOnInit() {
    this.isPreviewSupported = this.mediaFileInput.mediaType === 'video/mp4';
    this.mediaFile = this.mediaFileInput;
    this.contentSource = this.mediaService.getContentSource(this.mediaFileInput.id);
  }
}
