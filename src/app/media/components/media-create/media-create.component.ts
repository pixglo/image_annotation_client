/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../../shared/notification.service';
import { MediaService } from '../../media.service';
import { Media } from '../../model/media';
import { CustomPreProcessor } from './custom-pre-processor.service';

const Uppy = require('@uppy/core');
const Dashboard = require('@uppy/dashboard');
const GoogleDrive = require('@uppy/google-drive');
const Dropbox = require('@uppy/dropbox');
const Tus = require('@uppy/tus');
const Url = require('@uppy/url');

@Component({
  selector: 'app-media-create',
  templateUrl: './media-create.component.html',
  styleUrls: [
    './media-create.component.scss'
  ]
})
export class MediaCreateComponent implements OnInit {
  readonly baseUrl = 'v1/media';
  isFileAdded = false;
  progress = 0;
  isValidating = false; // Fake validation while awaiting the S3 upload completion
  mediaCategories = [];
  selectedMediaCategories = [];
  mediaFile = new Media();
  isEditMode = false;
  private uppy;

  constructor(
    private appService: AppService,
    private router: Router,
    private notificationService: NotificationService,
    private mediaService: MediaService,
    private route: ActivatedRoute
  ) {
    this.mediaCategories = mediaService.getMediaCetegoryDropdownOptions();
    this.appService.setHeader('Media', ['Content Management', 'Media']);
    // this.mediaFile.categoryTypes = [];
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.id !== undefined) {
        this.isEditMode = true;
        this.activateEditMode(params.id);
      } else {
        // Initialize Uppy component
        this.activateCreateMode();
      }
    });
  }

  activateCreateMode() {
    this.uppy = Uppy({
      debug: true,
      autoProceed: false,
      restrictions: {
        maxFileSize: this.mediaService.getMaxFileSizeMediaMB,
        maxNumberOfFiles: 1,
        minNumberOfFiles: 1,
        allowedFileTypes: ['.mp4']
      }
    })
      .use(CustomPreProcessor, {
        mediaService: this.mediaService
      })
      .use(Dashboard, {
        trigger: '.UppyModalOpenerBtn',
        inline: true,
        target: '#drag-drop-area',
        replaceTargetContent: true,
        showProgressDetails: true,
        note: 'Videos up to 1GB',
        height: 470,
        width: '100%',
        metaFields: [
          { id: 'name', name: 'Name', placeholder: 'file name' }
        ],
        browserBackButtonClose: true,
        proudlyDisplayPoweredByUppy: false
      })
      // .use(Url, {
      //   target: Dashboard,
      //   companionUrl: 'https://companion.uppy.io/',
      //   locale: {}
      // })
      // .use(GoogleDrive, {
      //   target: Dashboard,
      //   companionUrl: 'https://companion.uppy.io'
      // })
      // .use(Dropbox, {
      //   target: Dashboard,
      //   companionUrl: 'https://companion.uppy.io'
      // })
      .use(Tus, {
        endpoint: `${this.baseUrl}`,
        resume: true,
        autoRetry: true,
        retryDelays: [0, 1000, 3000, 5000]
      });

    this.uppy.on('file-removed', (file) => {
      console.log('Removed file', file);
      this.mediaFile = new Media();
    });

    this.uppy.on('complete', async (result) => {
      console.log('successful files:', result.successful);
      console.log('failed files:', result.failed);

      this.mediaFile = this.mediaService.getMediaFile();

      // Copy content to EFS and clean up TUS upload
      await this.mediaService.processMediaContent(this.mediaFile.id, result.successful[0].uploadURL);
      this.router.navigate(['/media']);
    });
  }

  async activateEditMode(id: any) {
    this.isEditMode = true;

    // Load media file
    this.mediaFile = <Media>await this.mediaService.findById(id);
    this.selectedMediaCategories = this.mediaFile.categoryTypes;
  }

  cancel() {
    this.router.navigate(['/media']);
  }

  async updateMediaFile() {
    await this.mediaService.updateMediaFile(this.mediaFile);
  }

}
