/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Page } from '../shared/model/paging/page';
import { Media } from './model/media';
import { Subscription } from 'rxjs';
import { AppService } from '../app.service';
import { AppConfig } from '../shared/model/config/app-config';
import { NotificationService } from '../shared/notification.service';
import { Scene } from './model/scene';

@Injectable({
  providedIn: 'root'
})
export class MediaService implements OnDestroy {
  private readonly mediaApi = '/v1/media';
  private readonly sceneApi = '/v1/scenes';
  private mediafile: Media;

  appConfigSubscription: Subscription;
  config = new AppConfig();

  constructor(
    private appService: AppService,
    private notificationService: NotificationService,
    private httpClient: HttpClient
  ) {
    this.appConfigSubscription = this.appService.appConfig$.subscribe((config) => {
      this.config = config;
    });
  }

  async search(
    query?: string,
    page?: number,
    size?: number,
    sort?: string,
    preProcessed = false,
    jobNonAssigned = false
  ): Promise<Page<Media>> {
    let params = new HttpParams();
    if (query) { params = params.set('fileName', query); }
    if (page) { params = params.set('page', page.toString()); }
    if (size) { params = params.set('size', size.toString()); }
    if (sort) { params = params.set('sort', sort); }
    params = params.set('preProcessed', preProcessed.toString());
    params = params.set('jobNonAssigned', jobNonAssigned.toString());

    console.log('Searching for media files with params', params.toString());
    const response = await this.httpClient.get(this.mediaApi, { params: params }).toPromise();
    return Page.build(response, Media.from);
  }

  getContentSource(mediaFileId: string): string {
    return `${this.config.baseUrl}${this.mediaApi}/${mediaFileId}/content`;
  }

  getSceneContentSource(sceneId: string): string {
    return `${this.config.baseUrl}${this.sceneApi}/${sceneId}/content`;
  }

  getMaxFileSizeMediaMB(): number {
    return this.config.maxFileSizeMediaMB;
  }

  getMaxFileSizeDefaultMB(): number {
    return this.config.maxFileSizeDefaultMB;
  }

  ngOnDestroy(): void {
    if (this.appConfigSubscription) {
      this.appConfigSubscription.unsubscribe();
    }
  }

  async delete(media: Media) {
    const message = 'media.delete.confirm';
    const messageValues = { name: `${media.fileName}` };
    await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
      console.log('Deleting', media);
      await this.httpClient.delete(`${this.mediaApi}/${media.id}`).toPromise();
      console.log('Deleted', media);
      this.notificationService.showSuccess('media.delete.success', messageValues);
    });
  }

  async findById(mediaId: string) {
    return this.httpClient.get(`${this.mediaApi}/${mediaId}`).toPromise();
  }

  async updateMediaFile(media: Media) {
    await this.httpClient.put(`${this.mediaApi}/${media.id}`, media).toPromise();
    this.notificationService.showSuccess('media.update.success');
  }

  async findScenesByMediaId(mediaId: string) {
    return this.httpClient.get(`${this.sceneApi}?mediaFileId=${mediaId}`).toPromise();
  }

  async saveMediaFile(mediaFile: Media) {
    return this.httpClient.post(`${this.mediaApi}`, mediaFile).toPromise();
  }

  async processMediaContent(mediaId: string, uploadURL: string) {
    return this.httpClient.post(`${this.mediaApi}/${mediaId}`, {uploadURL}).toPromise();
  }

  setMediaFile(mediaFile: Media) {
    this.mediafile = mediaFile;
  }

  getMediaFile() {
    return this.mediafile;
  }

  getMediaCetegoryDropdownOptions() {
    return [
      {
        name: 'Mediterranean',
        categoryType: 'Cooking'
      },
      {
        name: 'Soccer',
        categoryType: 'Sports'
      }
    ];
  }

}
