/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
  'template.not.found': 'Template (${value}) does not exist.',
  'template.name.already.exists': 'Template name \'${value}\' is in use. Please use a different name.',

  'template.create.success': 'Template was created successfully.',
  'template.create.success.title': 'Template Created',
  'template.update.success': 'Template was updated successfully.',
  'template.update.success.title': 'Template Updated',

  'template.delete.confirm': 'Are you sure to delete \'${name}\' ?',
  'template.delete.confirm.title': 'Delete Template',
  'template.delete.success': 'Template \'${name}\' was deleted successfully.',
  'template.delete.success.title': 'Template Deleted',

  'template.archive.confirm': 'Are you sure to archive \'${name}\' ?',
  'template.archive.confirm.title': 'Archive Template',
  'template.archive.success': 'Template \'${name}\' was archived successfully.',
  'template.archive.success.title': 'Template Archived',

  'template.annotate.success': 'Template annotation completed successfully.',
  'template.annotations.delete.confirm': 'Are you sure you want to delete all annotation points?',
  'template.annotations.delete.success': 'Annotation points cleared successfully'

};
