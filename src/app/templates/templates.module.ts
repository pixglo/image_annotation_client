/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { TagInputModule } from 'ngx-chips';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TemplatesRoutingModule } from './templates-routing.module';
import { TemplateListComponent } from './components/template-list/template-list.component';
import { TemplateUploadComponent } from './components/template-upload/template-upload.component';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { NgSelectModule } from '@ng-select/ng-select';
import { MessageService } from '../shared/message.service';
import { messages } from './templates-messages';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AceEditorModule } from 'ng2-ace-editor';
import { ZoomableCanvasComponent } from '@durwella/zoomable-canvas';
import { TemplateAnnotationComponent } from './components/template-annotation/template-annotation.component';

@NgModule({
  declarations: [TemplateListComponent, TemplateUploadComponent, ZoomableCanvasComponent, TemplateAnnotationComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    SharedModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    DropzoneModule,
    NgSelectModule,
    NgbModule,
    AceEditorModule,
    TemplatesRoutingModule,
    TagInputModule,
  ]
})
export class TemplatesModule {
  constructor(
    private messageService: MessageService
  ) {
    this.messageService.register(messages);
  }
}
