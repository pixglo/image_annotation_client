/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export class Template {

  constructor(public id?: string,
              public name?: string,
              public tenantId?: string,
              public types?: any[],
              public fileName?: string,
              public fileSizeBytes?: number,
              public thumbnailSizeBytes?: number,
              public contentType?: string,
              public fileSha1?: string,
              public width?: number,
              public height?: number,
              public contentUploaded?: boolean,
              public archived?: boolean,
              public annotated?: boolean,
              public annotatedString?: string,
              public extracted?: boolean,
              public tags?: any[],
              public actionUrl?: string
              ) {}

  static from(obj): Template {
    const asset = new Template();
    Object.assign(asset, obj);
    return asset;
  }
}
