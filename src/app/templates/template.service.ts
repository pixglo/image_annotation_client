/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Template } from './model/template';
import { Page } from '../shared/model/paging/page';
import { NotificationService } from '../shared/notification.service';
import { TemplateConfiguration } from '../rendering-jobs/model/template-configuration';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  private apiBase = '/v1/templates';

  constructor(private httpClient: HttpClient, private notificationService: NotificationService) { }

  async search(
    query?: string,
    page?: number,
    size?: number,
    sort?: string,
    archived = false,
    types = ['TEMPLATE', 'OVERLAY'],
    mediaFileId = null,
    templateConfiguration?: TemplateConfiguration[]
  ): Promise<Page<Template>> {
    let params = new HttpParams();
    if (query) { params = params.set('name', query); }
    if (page) { params = params.set('page', page.toString()); }
    if (size) { params = params.set('size', size.toString()); }
    if (sort) { params = params.set('sort', sort); }
    params = params.set('archived', archived.toString());
    params = params.set('types', types.join(','));
    if (mediaFileId) { params = params.set('mediaFileId', mediaFileId); }
    if (templateConfiguration !== undefined) {
      params = params.set('sourceTemplateIds',
        templateConfiguration.map(tempConfig => tempConfig.sourceTemplateId).join(','));
    }
    console.log('Searching for templates with params', params.toString());

    const response = await this.httpClient.get(this.apiBase, { params: params }).toPromise();
    return Page.build(response, Template.from);
  }

  create(template: Template) {
    return this.httpClient.post(`${this.apiBase}`, template).toPromise();
  }

  async update(template: Template) {
    await this.httpClient.put(`${this.apiBase}/${template['id']}`, template).toPromise();
    console.log('Updated asset', template);
    this.notificationService.showSuccess('template.update.success');
  }

  async getTypes(): Promise<Array<any>> {
    return <Array<any>>await this.httpClient.get(`${this.apiBase}/types`, {}).toPromise();
  }

  async findById(templateId: string) {
    if (!templateId) {
      throw new Error('Template ID is required for findById');
    }
    return this.httpClient.get(`${this.apiBase}/${templateId}`).toPromise();
  }

  findAnnotatedTemplatesByIds(templateIds: string): any {
    return this.httpClient.get(`${this.apiBase}/${templateIds}/annotated`).toPromise();
  }

  findTemplatesByIds(templateIds: string): any {
    return this.httpClient.get(`${this.apiBase}/regional-overlays/${templateIds}`).toPromise();
  }

  loadImage(templateId: string) {
    if (!templateId) {
      throw new Error('Template ID is required to load the image.');
    }
    return this.httpClient.get(`${this.apiBase}/${templateId}/thumbnail`).toPromise();
  }

  async archive(template: Template, archiveObj: any) {
    if (!template['id']) {
      throw new Error('Template ID is required to archive the template.');
    }

    const message = 'template.archive.confirm';
    const messageValues = { name: template.name };
    this.notificationService.showYesNoConfirmation(message, messageValues).then(async () => {
      console.log('Deleting', template);
      await this.httpClient.put(`${this.apiBase}/${template.id}/archived`, archiveObj).toPromise();
      console.log('Deleted', template);
      this.notificationService.showSuccess('template.archive.success', messageValues);
    });
  }

  async del(template: Template) {
    const message = 'template.delete.confirm';
    const messageValues = { name: template.name };
    await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
      console.log('Deleting', template);
      await this.httpClient.delete(`${this.apiBase}/${template.id}`).toPromise();
      console.log('Deleted', template);
      this.notificationService.showSuccess('template.delete.success', messageValues);
    });
  }

  async deleteAllAnnotations(template: Template) {
    const message = 'template.annotations.delete.confirm';
    await this.notificationService.showDeleteConfirmation(message).then(async () => {
      await this.httpClient.put(`${this.apiBase}/${template['id']}`, template).toPromise();
      this.notificationService.showSuccess('template.annotations.delete.success');
    });
  }

  async annotate(template: Template) {
    await this.httpClient.put(`${this.apiBase}/${template['id']}`, template).toPromise();
    console.log('Template annotated', template);
    this.notificationService.showSuccess('template.annotate.success');
  }

  getAnnotatedTemplatesByIds(templateIds: any): Promise<any> {
    let params = new HttpParams();
    params = params.set('templateIds', templateIds.join(','));
    return this.httpClient.get(`${this.apiBase}/annotated`, { params: params }).toPromise();
  }
}
