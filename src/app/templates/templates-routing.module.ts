/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Layout2Component } from '../layout/layout-2/layout-2.component';
import { TemplateListComponent } from './components/template-list/template-list.component';
import { TemplateUploadComponent } from './components/template-upload/template-upload.component';
import { TemplateAnnotationComponent } from './components/template-annotation/template-annotation.component';

const routes: Routes = [
  {
    path: '',
    component: Layout2Component,
    children: [
      { path: '', component: TemplateListComponent, data: { permission: 'TEMPLATE_VIEW' } },
      { path: 'create', component: TemplateUploadComponent, data: { permission: 'TEMPLATE_CREATE_OR_UPDATE' } },
      { path: 'edit/:id', component: TemplateUploadComponent, data: { permission: 'TEMPLATE_CREATE_OR_UPDATE' } },
      { path: 'annotate/:id', component: TemplateAnnotationComponent, data: { permission: 'TEMPLATE_CREATE_OR_UPDATE' } },
      { path: 'annotate/:id/job/:jobId', component: TemplateAnnotationComponent, data: { permission: 'TEMPLATE_CREATE_OR_UPDATE' } }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TemplatesRoutingModule { }
