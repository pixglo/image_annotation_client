/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { AppService } from '../../../app.service';
import { TemplateService } from '../../template.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';

import { Template } from '../../model/template';

@Component({
    selector: 'app-template-list',
    templateUrl: './template-list.component.html',
    styleUrls: ['./template-list.component.scss']
})
export class TemplateListComponent implements OnInit {

    loading = true;
    content = [];
    page = PageMetadata.empty(20, 'createdAt');

    // Search
    searchQuery: string;
    searchTextChanged = new Subject<string>();
    searchSubscription;
    isArchived = false;
    temaplateConfiguration: string[];

    constructor(private appService: AppService,
        private templateService: TemplateService,
        private router: Router,
        private route: ActivatedRoute) {
        this.appService.setHeader('Content Management', ['Template Library', 'Templates']);
    }

    async ngOnInit() {

        this.searchSubscription = this.searchTextChanged
            .pipe(debounceTime(500))
            .pipe(distinctUntilChanged())
            .pipe(mergeMap(search => this.search(this.page.page)))
            .subscribe(() => { });
        this.search();
    }

    async search(page = 1) {
        this.loading = true;
        try {
            const pageResponse = await this.templateService.search(this.searchQuery, page, this.page.size, this.page.sort, this.isArchived,
                this.temaplateConfiguration);
            // this.content = pageResponse.content;
            this.page = pageResponse.metadata;
        } finally {
            this.loading = false;
        }
    }

    triggerSearch($event) {
        // Called by the search bar
        this.searchTextChanged.next($event.target.value);
    }

    edit(template: Template) {
        this.router.navigate(['edit', template.id], { relativeTo: this.route });
    }

    annotate(template: Template) {
        this.router.navigate(['annotate', template.id], { relativeTo: this.route });
    }

    async delete(template: Template) {
        await this.templateService.del(template);
        console.log('Template deleted : ' + template.id);
        console.log('Deleted, Searching');
        this.search();
    }
    async archive(template: Template) {
        await this.templateService.archive(template, Object.assign({}, { 'archived': true }));
        console.log('Template archived successfully. ');
        this.search();
    }

    paginate(page) {
        console.log(page);
        this.search(page);
    }

    toggleArchive() {
        this.isArchived ? this.isArchived = false : this.isArchived = true;
        this.search();
    }

    isTemplateType(template: Template): boolean {
        return template.types.includes('TEMPLATE');
    }
}
