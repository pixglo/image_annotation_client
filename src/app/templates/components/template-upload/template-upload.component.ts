/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { Template } from '../../model/template';
import { TemplateService } from '../../template.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../../shared/notification.service';
import { DropzoneDirective, DropzoneComponent, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { MediaService } from '../../../media/media.service';

@Component({
  selector: 'app-template-upload',
  templateUrl: './template-upload.component.html',
  styleUrls: ['./template-upload.component.scss']
})
export class TemplateUploadComponent implements OnInit {
  templateTypes = [];
  templateForm: FormGroup;
  isEditMode = false;
  template: Template = undefined;
  private templateId: string = undefined;
  private apiBase = '/v1/templates';
  progress = 0;
  isFileAdded = false;
  isEnable = true;
  public config: DropzoneConfigInterface;
  private readonly moduleName = 'Content Management';
  private readonly screenName = 'Template Library';
  private readonly templateListUrl = '/templates';

  @ViewChild(DropzoneDirective) directiveRef: DropzoneDirective;
  @ViewChild(DropzoneComponent) componentRef: DropzoneComponent;

  constructor(private appService: AppService,
    private templateService: TemplateService,
    private mediaService: MediaService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) {

    this.templateForm = this.buildForm();
  }

  async ngOnInit() {
    this.config = {
      url: `${this.apiBase}`,
      parallelUploads: 1,
      uploadMultiple: false,
      autoProcessQueue: false,
      maxFiles: 1,
      acceptedFiles: '.png,.jpg,.jpeg,.bmp',
      maxFilesize: this.mediaService.getMaxFileSizeDefaultMB(),
      filesizeBase: 1024,
      timeout: 60000 * 6,
      thumbnailWidth: 350,
      thumbnailHeight: 250,
      addRemoveLinks: false,
    };

    this.templateTypes = await this.templateService.getTypes();
    console.log(this.templateTypes);
    this.route.params.subscribe(params => {
      if (params.id !== undefined) {
        const selectedId = params.id;
        this.activateEditMode(selectedId);
      } else {
        Object.assign(this.config, { previewTemplate: document.getElementById('asset-preview-template').innerHTML });
        this.activateCreateMode();
      }
    });
  }

  private activateCreateMode(): void {
    this.appService.setHeader(this.moduleName, [this.moduleName, this.screenName, 'Upload Template']);
  }

  private async activateEditMode(id: any) {
    this.isEditMode = true;
    this.appService.setHeader(this.moduleName, [this.moduleName, this.screenName, 'Edit Template']);

    // Load Template
    this.template = <Template>await this.templateService.findById(id);
    this.templateId = this.template.id;
    console.log('Loading template', this.template);

    // Load the uploaded image in the ngx-dropzone plugin.
    console.log('Template Id for mock file : ' + this.template.id);

    this.templateForm.patchValue(this.template);
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      name: ['', Validators.required],
      types: [[], Validators.required],
      actionUrl: [''],
      tags: [[]]
    });
  }

  async save() {
    if (this.templateForm.invalid) {
      return;
    }

    this.templateForm.value.tags = this.templateForm.value.tags.map(tag => {
      return tag.value || tag;
    });

    if (this.isEditMode) {
      await this.update();
    } else {
      await this.create();
    }
  }

  async create() {
    this.isEnable = false;
    const response = await this.templateService.create(this.templateForm.value);
    console.log(response);
    this.templateId = response['id'];
    console.log('Template created with id :' + this.templateId);
    const dropzone = this.directiveRef.dropzone();
    dropzone.options.url = `${this.apiBase}/${this.templateId}/content`;
    dropzone.processQueue();
  }

  async update() {
    const response = await this.templateService.update(Object.assign({ id: this.template.id }, this.templateForm.value));
    console.log(response);
    this.router.navigate([this.templateListUrl]);
  }

  async cancel() {
    if (this.templateForm.dirty) {
      await this.notificationService.showChangesConfirmation();
    }
    this.router.navigate([this.templateListUrl]);
  }

  async del() {
    await this.templateService.del(this.template);
    this.router.navigate([this.templateListUrl]);
  }

  async uploadSuccess(event: any) {
    console.log('Template upload success');
    this.isEnable = false;
    this.isFileAdded = false;
    this.notificationService.showSuccess('template.create.success');
    this.router.navigate([this.templateListUrl]);
  }

  uploadProgress(event: any) {
    const progress = event[1];
    this.progress = progress;
  }

  async onUploadError(e) {
    console.log(e);
    setTimeout(() => {
      this.progress = 0;
    }, 1000);
  }

  onFileAdded(e) {
    this.isFileAdded = true;
  }

  removeFile(e) {
    this.isFileAdded = false;
  }

  onTypeChange(types) {
    if (!types.length || types.find(type => type.value === 'OVERLAY') === undefined) {
      this.templateForm.controls.actionUrl.setValue('');
    }
  }

}
