/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { TemplateService } from '../../template.service';
import { NotificationService } from '../../../shared/notification.service';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HostListener } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs-compat/add/observable/fromEvent';
import 'rxjs-compat/add/operator/map';
import 'rxjs-compat/add/operator/debounceTime';
import 'rxjs-compat/add/operator/distinctUntilChanged';
import { Template } from '../../model/template';

@Component({
  selector: 'app-template-annotation',
  templateUrl: './template-annotation.component.html',
  styleUrls: ['./template-annotation.component.scss']
})
export class TemplateAnnotationComponent implements OnInit, AfterViewInit {
  @ViewChild('canvas') canvasRef: ElementRef;
  @ViewChild('canvasContainer') canvasContainer: ElementRef;

  public templateId;
  public selectedTemplate: Template;
  private canvas: any;
  private context: any;
  private imageValue: HTMLImageElement;
  public maxZoom = 1.5;
  public minZoom = 0;
  public zoomLevel = 0;
  public zoomRatio = 1;
  public top: number;
  public bottom: number;
  public left: number;
  public right: number;
  public canvasWidth = 0;
  public canvasHeight = 0;
  public canvasViewWidth = 0;
  public canvasViewHeight = 0;
  public centerX = 0;
  public centerY = 0;
  public correspondingPoints: Array<any> = [];
  public screenHeight = 0;
  public screenWidth = 0;
  public pointsWrapperContainer = 0;
  public templateHeight = 0;
  public templateWidth = 0;
  public isResizedImage = false;
  public dragPos: Point;
  public selectedTemplateAnnotated = false;
  public renderingJobId: any;
  readonly returningJobTab = 'templates';
  public showBeacon = false;
  public beaconX = 0;
  public beaconY = 0;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    this.screenHeight = this.screenHeight - 180;
    this.pointsWrapperContainer = window.innerHeight - 410;
  }

  constructor(private appService: AppService,
    private templateService: TemplateService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private changeDetector: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.appService.setHeader('Template Annotation', ['Template', ' Annotate']);
    this.getScreenSize();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.id !== undefined) {
        this.templateId = params.id;
        this.renderingJobId = params.jobId;
      } else {
        console.warn('Template Id Not Found.');
      }
    });
  }

  ngAfterViewInit() {
    setTimeout(() => this.initialize());
  }

  public async initialize() {
    const canvasContainerWidth = this.canvasContainer.nativeElement.offsetWidth;

    this.selectedTemplate = await this.templateService.findById(this.templateId);

    this.selectedTemplateAnnotated = this.selectedTemplate.annotated;
    this.canvas = this.canvasRef.nativeElement;

    if (this.canvas.getContext) {
      this.context = this.canvas.getContext('2d');
    }

    const image = new Image();

    if (this.selectedTemplate.width >= (canvasContainerWidth - 20)) {

      this.canvasViewWidth = Math.round(canvasContainerWidth - 20);
      this.canvasViewHeight = Math.round(((canvasContainerWidth - 20) * this.selectedTemplate.height) / this.selectedTemplate.width);
      image.src = `/v1/templates/${this.templateId}/resize?width=${this.canvasViewWidth}&&height=${this.canvasViewHeight}`;
      this.isResizedImage = true;

    } else {

      this.canvasViewWidth = canvasContainerWidth - 20;
      this.canvasViewHeight = ((canvasContainerWidth - 20) * this.selectedTemplate.height) / this.selectedTemplate.width;
      image.src = `/v1/templates/${this.templateId}/content`;
      this.isResizedImage = false;

    }

    image.onload = () => {
      this.context.canvas.width = this.canvasViewWidth;
      this.context.canvas.height = image.height + 100;
      this.imageValue = image;
      this.templateHeight = image.height;
      this.templateWidth = image.width;

      this.loadImage(image);
    };

    Observable.fromEvent(this.canvasRef.nativeElement, 'mousewheel')
      .map((evt: any) => evt.target.value)
      .debounceTime(1000)
      .subscribe(() => this.populateCoordinatesInVisibleArea());
  }

  private loadImage(image: HTMLImageElement) {
    const self = this;
    if (!this.canvas || !image) {
      return;
    }

    this.canvasWidth = image.width;
    this.canvasHeight = image.height;
    this.zoomLevel = 0;
    this.minZoom = 0;

    if (!this.centerX && !this.centerY) {
      this.setCenterX(this.canvasWidth / 2);
      this.setCenterY(this.canvasHeight / 2);
    }
    if (this.selectedTemplate.annotated) {
      this.correspondingPoints = JSON.parse(this.selectedTemplate.annotatedString);

      if (this.isResizedImage) {

        const reCalculatedPoints = this.correspondingPoints.map(function (point) {
          const x = Math.round((point.x / self.selectedTemplate.width) * self.canvasViewWidth);
          const y = Math.round((point.y / self.selectedTemplate.height) * self.canvasViewHeight);
          return { x: x, y: y };

        });
        self.correspondingPoints = [];
        self.correspondingPoints = reCalculatedPoints;
      }

      this.reDraw();
    } else {
      this.draw({ x: this.centerX, y: this.centerY }, 1);
    }
  }

  private draw(center: Point, zoomRatio: number) {
    if (this.context) {
      this.context.clearRect(0, 0, this.canvasViewWidth, this.context.canvas.height);
      this.context.drawImage(
        this.imageValue, 0, 0,
        this.imageValue.width, this.imageValue.height,
        this.imageValue.width / 2 - center.x * zoomRatio, this.imageValue.height / 2 - center.y * zoomRatio,
        this.imageValue.width * zoomRatio, this.imageValue.height * zoomRatio);
    }
  }

  /**
   * When click on a point on the canvas, park that point
   * as an annotated point.
   *
   * @param event
   */
  public annotate(event) {
    event.preventDefault();

    if (event.ctrlKey) {
      const rect = this.canvas.getBoundingClientRect();
      const x = event.clientX - rect.left;
      const y = event.clientY - rect.top;

      if (this.zoomLevel === 0) {
        // Mark the points
        const result = this.correspondingPoints.find(function (point) {
          return point.x === x && point.y === y;
        });

        if (!result) {
          this.context.fillStyle = '#7CFC00';
          this.context.beginPath();
          this.context.arc(x, y, 1.5, 0, Math.PI * 2, true);
          this.context.fill();

          const xCoorrdinate = Math.round(x + this.left);
          const yCoorrdinate = Math.round(y + this.top);

          this.correspondingPoints.push({ x: xCoorrdinate, y: yCoorrdinate });
        }
      } else if (this.zoomLevel > 0) {

        const realX = Math.round(this.left + (x / this.zoomRatio));
        const realY = Math.round(this.top + (y / this.zoomRatio));

        const result = this.correspondingPoints.find(function (point) {
          return point.x === realX && point.y === realY;
        });

        if (!result) {
          this.context.fillStyle = '#7CFC00';
          this.context.beginPath();
          this.context.arc(x, y, 1.5, 0, Math.PI * 2, true);
          this.context.fill();
          this.correspondingPoints.push({ x: realX, y: realY });
        }
      }
    }
  }

  private drawMarkers(x, y) {
    const result = this.correspondingPoints.find(function (point) {
      return point.x === x && point.y === y;
    });
    if (!result) {
      this.context.fillStyle = '#7CFC00';
      this.context.beginPath();
      this.context.arc(x, y, 1.5, 0, Math.PI * 2, true);
      this.context.fill();
      this.correspondingPoints.push({ x: x, y: y });
    }
  }

  /**
   *
   * @param index
   */
  selectCoordinate(index) {
    this.showBeacon = true;
    this.rescale(1, this.zoomRatio);
    const self = this;
    this.correspondingPoints.forEach(function (point, i) {

      if (point.x >= self.left && point.x <= self.right && point.y >= self.top && point.y <= self.bottom) {
        const newX = (point.x - self.left) * self.zoomRatio;
        const newY = (point.y - self.top) * self.zoomRatio;
        if (index === i) {
          self.beaconX = newX + 8;
          self.beaconY = newY + 8;
          self.context.fillStyle = '#fe0034';
          self.context.beginPath();
          self.context.arc(newX, newY, 1.5, 0, Math.PI * 2, true);
          self.context.fill();
        } else {
          self.context.fillStyle = '#7CFC00';
          self.context.beginPath();
          self.context.arc(newX, newY, 1.5, 0, Math.PI * 2, true);
          self.context.fill();
        }
      }
    });
  }

  unselectCoordinate(index) {
    this.showBeacon = false;
    this.beaconX = 0;
    this.beaconY = 0;
    this.rescale(1, this.zoomRatio);
    const self = this;
    this.correspondingPoints.forEach(function (point, i) {

      if (point.x >= self.left && point.x <= self.right && point.y >= self.top && point.y <= self.bottom) {
        const newX = (point.x - self.left) * self.zoomRatio;
        const newY = (point.y - self.top) * self.zoomRatio;
        self.context.fillStyle = '#7CFC00';
        self.context.beginPath();
        self.context.arc(newX, newY, 1.5, 0, Math.PI * 2, true);
        self.context.fill();
      }
    });
  }

  removeMarker(index) {
    this.correspondingPoints.splice(index, 1);

    if (this.zoomLevel === 0) {
      // Redraw the canvas after removing the marker point.
      this.reDraw();
    } else {
      this.rescale(1, this.zoomRatio);
    }
  }

  /**
   * When the point is deleted, remove the corresponding point and
   * redraw the image. Then plot the points with the existing points.
   */
  public reDraw() {
    this.draw({ x: this.centerX, y: this.centerY }, 1);

    if (this.top !== 0 || this.left !== 0) {
      const self = this;
      this.correspondingPoints.forEach(function (point) {
        self.context.fillStyle = '#7CFC00';
        self.context.beginPath();
        self.context.arc(point.x - self.left, point.y - self.top, 1.5, 0, Math.PI * 2, true);
        self.context.fill();
      });
    } else {
      const self = this;
      this.correspondingPoints.forEach(function (point) {
        self.context.fillStyle = '#7CFC00';
        self.context.beginPath();
        self.context.arc(point.x, point.y, 1.5, 0, Math.PI * 2, true);
        self.context.fill();
      });
    }
  }

  private clampZoomLevel(zoomLevel: number): number {
    let clamped = zoomLevel;

    if (this.minZoom !== undefined) {
      clamped = Math.max(this.minZoom, clamped);
    }

    if (this.maxZoom !== undefined) {
      clamped = Math.min(this.maxZoom, clamped);
    }

    return clamped;
  }

  private clampCenter(val: number, length: number): number {
    return Math.min(length, Math.max(0, val));
  }

  /**
   * Zoom canvas on the mouse wheel event.
   * @param event
   */
  public zoomInAndOut(event: WheelEvent) {
    event.preventDefault();

    const previousZoomLevel = this.zoomLevel;
    const previousZoomRatio = this.zoomRatio;

    const newZoomLevel = this.clampZoomLevel(previousZoomLevel + event.deltaY * 0.05);
    const newZoomRatio = Math.pow(2, newZoomLevel);

    this.rescale(previousZoomRatio, newZoomRatio, { x: event.offsetX, y: event.offsetY });
    this.zoomLevel = newZoomLevel;
    this.zoomRatio = newZoomRatio;
  }

  /**
   * This is called initially to set the centerX
   * Also called when zoom to set the new center
   *
   * @param val
   */
  private setCenterX(val?: number) {
    if (!this.imageValue) {
      return;
    }
    val = this.clampCenter(val, this.imageValue.width);
    this.centerX = val;

    this.left = val - this.imageValue.width / (2 * this.zoomRatio);
    this.right = val + this.imageValue.width / (2 * this.zoomRatio);
  }

  /**
   * This is called initially to set the centerY
   * Also called when zoom to set the new center
   *
   * @param val
   */
  private setCenterY(val?: number) {
    if (!this.imageValue) {
      return;
    }

    val = this.clampCenter(val, this.imageValue.height);
    this.centerY = val;

    this.top = val - this.imageValue.height / (2 * this.zoomRatio);
    this.bottom = val + this.imageValue.height / (2 * this.zoomRatio);
  }

  private rescale(previousZoomRatio: number, newZoomRatio: number, center?: Point) {
    if (previousZoomRatio === newZoomRatio || previousZoomRatio === undefined || newZoomRatio === undefined) {
      return;
    }

    if (this.canvas == null) {
      return;
    }

    if (center == null) {
      center = { x: this.canvas.scrollWidth / 2, y: this.canvas.scrollHeight / 2 };
    }

    const yRatio = center.y / this.canvas.scrollHeight;
    const xRatio = center.x / this.canvas.scrollWidth;

    const prevWidth = this.right - this.left;
    const prevHeight = this.bottom - this.top;

    const scale = previousZoomRatio / newZoomRatio;

    const newWidth = scale * prevWidth;
    const newHeight = scale * prevHeight;

    this.left = this.left + (prevWidth - newWidth) * xRatio;
    this.right -= (prevWidth - newWidth) * (1 - xRatio);

    this.top = this.top + (prevHeight - newHeight) * yRatio;
    this.bottom -= (prevHeight - newHeight) * (1 - yRatio);

    this.centerX = (this.left + this.right) / 2;
    this.centerY = (this.top + this.bottom) / 2;

    this.draw({ x: this.centerX, y: this.centerY }, newZoomRatio);
  }

  /**
   * Note : When zoom-in zoom-out for multiple times, there was and mis-alignment
   * in the image. To avoid this and to re render the original image in the canvas
   * this method is used. Remove in the final edition.
   */
  reLoadImage() {
    this.zoomLevel = 0;
    this.zoomRatio = 1;

    this.setCenterX(this.canvasWidth / 2);
    this.setCenterY(this.canvasHeight / 2);

    this.draw({ x: this.canvasWidth / 2, y: this.canvasHeight / 2 }, 1);

    const self = this;
    this.correspondingPoints.forEach(function (point) {
      self.context.fillStyle = '#7CFC00';
      self.context.beginPath();
      self.context.arc(point.x, point.y, 1.5, 0, Math.PI * 2, true);
      self.context.fill();
    });
  }

  /**
   * Zoom-in button
   */
  zoomIn() {
    if (this.zoomLevel + 0.5 <= this.maxZoom) {
      this.zoomLevel = this.zoomLevel + 0.5;

      this.setCenterY(this.canvasHeight / 2);
      this.setCenterX(this.canvasWidth / 2);

      const previousZoomRatio = this.zoomRatio;
      this.zoomRatio = Math.pow(2, this.zoomLevel);
      this.rescale(previousZoomRatio, this.zoomRatio, { x: this.centerX, y: this.centerY });
      this.populateCoordinatesInVisibleArea();
    }
  }

  /**
   * Zoom-out button.
   */
  zoomOut() {
    if (this.zoomLevel - 0.5 >= this.minZoom) {
      this.zoomLevel = this.zoomLevel - 0.5;

      this.setCenterY(this.canvasHeight / 2);
      this.setCenterX(this.canvasWidth / 2);

      const previousZoomRatio = this.zoomRatio;
      this.zoomRatio = Math.pow(2, this.zoomLevel);
      this.rescale(previousZoomRatio, this.zoomRatio, { x: this.centerX, y: this.centerY });
      this.populateCoordinatesInVisibleArea();
    }
  }

  populateCoordinatesInVisibleArea() {
    const self = this;

    this.correspondingPoints.forEach(function (point) {

      if (point.x >= self.left && point.x <= self.right && point.y >= self.top && point.y <= self.bottom) {
        const newX = (point.x - self.left) * self.zoomRatio;
        const newY = (point.y - self.top) * self.zoomRatio;
        self.context.fillStyle = '#7CFC00';
        self.context.beginPath();
        self.context.arc(newX, newY, 1.5, 0, Math.PI * 2, true);
        self.context.fill();
      }
    });
  }

  getAnnotatedPoints() {
    let annotationArr = [];

    if (this.isResizedImage) {
      const self = this;
      annotationArr = this.correspondingPoints.map(function (point) {
        const x = Math.round((point.x / self.canvasViewWidth) * self.selectedTemplate.width);
        const y = Math.round((point.y / self.canvasViewHeight) * self.selectedTemplate.height);
        const originalPoint = { x: x, y: y };
        return originalPoint;
      });

      console.log(`Corresponding Points : ${this.correspondingPoints}`);
      console.log(`Re-Calculated Points : ${annotationArr}`);
    } else {
      annotationArr = this.correspondingPoints;
    }

    return annotationArr;
  }

  async save() {
    let template: Template;

    template = Object.assign(
      this.selectedTemplate,
      { 'annotated': true, 'annotatedString': JSON.stringify(this.getAnnotatedPoints()) }
    );

    await this.templateService.annotate(template);

    if (this.renderingJobId) {
      this.router.navigate(['/rendering-jobs/process', this.renderingJobId, this.returningJobTab]);
    } else {
      this.router.navigate(['/templates']);
    }
  }

  cancel() {
    if (this.renderingJobId) {
      this.router.navigate(['/rendering-jobs/process', this.renderingJobId, this.returningJobTab]);
    } else {
      this.router.navigate(['/templates']);
    }
  }

  mousedown(event: MouseEvent) {
    this.dragPos = { x: event.offsetX, y: event.offsetY };
  }

  mousemove(event: MouseEvent) {
    if (event.buttons > 0) {
      if (this.dragPos === null || this.dragPos === undefined) {
        this.dragPos = { x: event.offsetX, y: event.offsetY };
      }

      const dx = (event.offsetX - this.dragPos.x) / this.zoomRatio;
      const dy = (event.offsetY - this.dragPos.y) / this.zoomRatio;

      this.setCenterX(this.centerX - dx);
      this.setCenterY(this.centerY - dy);

      this.dragPos = { x: event.offsetX, y: event.offsetY };

      this.draw({ x: this.centerX, y: this.centerY }, this.zoomRatio);

      if (this.top !== 0 && this.right !== 0) {
        this.populateCoordinatesInVisibleArea();
      }
    }
  }

  async clearAllAnnotations() {
    const template = Object.assign(this.selectedTemplate, { 'annotated': false, 'annotatedString': null });
    await this.templateService.deleteAllAnnotations(template);
    this.correspondingPoints = [];
    this.reDraw();
  }

  exportAnnotations() {
    const dataStr = `data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(this.getAnnotatedPoints()))}`;
    const anchorNode = document.createElement('a');
    anchorNode.setAttribute('href', dataStr);
    anchorNode.setAttribute('download', `${this.selectedTemplate.id}_annotation.json`);
    document.body.appendChild(anchorNode); // required for firefox
    anchorNode.click();
    anchorNode.remove();
  }
}

interface Point {
  x: number;
  y: number;
}
