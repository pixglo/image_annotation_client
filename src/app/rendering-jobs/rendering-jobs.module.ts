/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RenderingJobListComponent } from './components/rendering-job-list/rendering-job-list.component';
import { RenderingJobProcessComponent } from './components/rendering-job-process/rendering-job-process.component';
import { RenderingJobsRoutingModule } from './rendering-jobs-routing.module';
import { SharedModule } from '../shared/shared.module';
import { LayoutModule } from '../layout/layout.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MessageService } from '../shared/message.service';
import { messages } from './rendering-job-messages';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { SourceMediaComponent } from './components/source-media/source-media.component';
import { TemplatesComponent } from './components/templates/templates.component';
import { RegionsComponent } from './components/regions/regions.component';
import { MediaScenesComponent } from './components/media-scenes/media-scenes.component';
import { DistributionsComponent } from './components/distributions/distributions.component';
import { SourceMediaSelectComponent } from './components/source-media-select/source-media-select.component';
import { RenderingJobCreateComponent } from './components/rendering-job-create/rendering-job-create.component';
import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';
import { TemplateSelectComponent } from './components/template-select/template-select.component';
import { SceneAnnotatorComponent } from './components/scene-annotator/scene-annotator.component';
import { ROICreateComponent } from './components/roi-create/roi-create.component';
// import { NgxPixgloEditorModule } from '@pixglo/video-editor';
import { MediaPreviewComponent } from './components/media-preview/media-preview.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxSpinnerModule } from 'ngx-spinner';
/*
@NgModule({
  declarations: [
    RenderingJobListComponent,
    RenderingJobProcessComponent,
    SourceMediaComponent,
    TemplatesComponent,
    RegionsComponent,
    MediaScenesComponent,
    DistributionsComponent,
    SourceMediaSelectComponent,
    RenderingJobCreateComponent,
    TemplateSelectComponent,
    SceneAnnotatorComponent,
    ROICreateComponent,
    MediaPreviewComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    LayoutModule,
    SharedModule,
    RenderingJobsRoutingModule,
    NgxDatatableModule,
    NgbModule,
    NgSelectModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    NgxPixgloEditorModule.forRoot({
      urlprefix: 'v1/annotations', BASE_URL: window.location.origin
    }),
     MDBBootstrapModule.forRoot(),
     NgxSpinnerModule
  ],
  entryComponents: [
    SourceMediaComponent,
    TemplatesComponent,
    RegionsComponent,
    MediaScenesComponent,
    DistributionsComponent,
    SourceMediaSelectComponent,
    RenderingJobCreateComponent,
    TemplateSelectComponent,
    ROICreateComponent,
    MediaPreviewComponent
  ]
})

export class RenderingJobsModule {
  constructor(
    private messageService: MessageService
  ) {
    this.messageService.register(messages);
  }
}*/

@NgModule({
    declarations: [
        RenderingJobListComponent,
        RenderingJobProcessComponent,
        SourceMediaComponent,
        TemplatesComponent,
        RegionsComponent,
        MediaScenesComponent,
        DistributionsComponent,
        SourceMediaSelectComponent,
        RenderingJobCreateComponent,
        TemplateSelectComponent,
        SceneAnnotatorComponent,
        ROICreateComponent,
        MediaPreviewComponent
    ],
    imports: [
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        LayoutModule,
        SharedModule,
        RenderingJobsRoutingModule,
        NgxDatatableModule,
        NgbModule,
        NgSelectModule,
        VgCoreModule,
        VgControlsModule,
        VgOverlayPlayModule,
        VgBufferingModule,
        MDBBootstrapModule.forRoot(),
        NgxSpinnerModule
    ],
    entryComponents: [
        SourceMediaComponent,
        TemplatesComponent,
        RegionsComponent,
        MediaScenesComponent,
        DistributionsComponent,
        SourceMediaSelectComponent,
        RenderingJobCreateComponent,
        TemplateSelectComponent,
        ROICreateComponent,
        MediaPreviewComponent
    ]
})

export class RenderingJobsModule {
    constructor(
        private messageService: MessageService
    ) {
        this.messageService.register(messages);
    }
}
