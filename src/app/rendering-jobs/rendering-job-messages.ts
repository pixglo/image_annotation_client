/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
  'job.create.success': 'Rendering job created successfully',
  'media.source.updated': 'Media source updated successfully',
  'regions.updated': 'Regions updated successfully',
  'template.configurations.updated': 'Template configurations updated successfully',
  'distribution.configurations.updated': 'Distribution configurations updated successfully',
  'annotation.file.upload.success': 'Occlusion Mask and Annotation Json uploaded successfully',
  'annotation.file.upload.failed': 'Occlusion Mask and Annotation Json upload failed',
  'job.delete.confirm': 'Are you sure to delete "${name}" ?',
  'job.delete.confirm.title': 'Delete Rendering Job',
  'job.delete.success': 'Rendering Job was deleted successfully.',
  'job.delete.success.title': 'Rendering Job Deleted',
};
