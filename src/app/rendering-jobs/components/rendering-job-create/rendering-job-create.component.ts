/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppService } from '../../../app.service';
import { RenderingJobService } from '../../rendering-job.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RenderingJob } from '../../../rendering-jobs/model/rendering-job';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-rendering-job-create',
  templateUrl: './rendering-job-create.component.html',
  styleUrls: [
    './rendering-job-create.component.scss'
  ]
})
export class RenderingJobCreateComponent implements OnInit {
  @Output() renderingJobCreateEvent = new EventEmitter<RenderingJob>();
  renderingJobForm: FormGroup;

  constructor(
    private appService: AppService,
    private renderingJobService: RenderingJobService,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {
    this.renderingJobForm = this.buildForm();
  }

  ngOnInit() { }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

  async createRenderingJob(e: any) {
    if (!this.renderingJobForm.value.name) {
      e.preventDefault();
      return;
    }
    const job = await this.renderingJobService.create(this.renderingJobForm.value);
    this.renderingJobCreateEvent.emit(job);
    this.activeModal.close();
  }

  cancelRenderingJobCreation() {
    this.activeModal.close();
  }
}
