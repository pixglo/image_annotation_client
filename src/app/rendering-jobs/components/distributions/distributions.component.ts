/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';
import { RenderingJob } from '../../model/rendering-job';
import { FtpConfiguration } from '../../model/FtpConfiguration';
import { RenderingJobService } from '../../rendering-job.service';

@Component({
  selector: 'app-distributions',
  templateUrl: './distributions.component.html',
  styleUrls: [
    './distributions.component.scss'
  ]
})
export class DistributionsComponent implements OnInit {
  @Input() renderingJob: RenderingJob;
  isFtpEnabled = false;
  ftpConfiguration = new FtpConfiguration();
  isFtpConnectionVerified = false;
  isFtpConnectionSuccess = false;

  constructor(
    private appService: AppService,
    private router: Router,
    private renderingJobService: RenderingJobService,
  ) { }

  async ngOnInit() {
    this.renderingJob = await this.renderingJobService.findById(this.renderingJob.id);
    this.populateDistributionConfigurations(this.renderingJob.distributionConfigurations);
  }

  populateDistributionConfigurations(distributionConfigs: Array<any>) {
    distributionConfigs.forEach(config => {
      // Add new switch cases, if new configuration types are to be introduced
      switch (config.distributionType) {
        case 'FTP':
          this.isFtpEnabled = true;
          this.ftpConfiguration = config;
          break;
        default: console.log('Unknown Type Found');
      }
    });
  }

  onFtpCheckChange(e) {
    this.isFtpEnabled = e.target.checked;
  }

  async verifyFtpConnection() {
    this.isFtpConnectionVerified = false;

    try {
      const res = await this.renderingJobService.verifyFtpConnection(this.ftpConfiguration);
      this.isFtpConnectionSuccess = true;
    } catch (err) {
      this.isFtpConnectionSuccess = false;
      console.log(err);
    }

    this.isFtpConnectionVerified = true;
  }

  async saveDistributionConfigs() {
    await this.renderingJobService.updateDistributionconfigurations(this.renderingJob.id, this.ftpConfiguration);
    console.log('Distribution configs updated');
  }
}
