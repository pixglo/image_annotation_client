/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AppService } from '../../../app.service';
import { RenderingJobService } from '../../rendering-job.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RenderingJob } from '../../../rendering-jobs/model/rendering-job';
import { TemplateService } from '../../../templates/template.service';
import { TenantService } from '../../../tenants/tenant.service';

@Component({
  selector: 'app-roi-create',
  templateUrl: './roi-create.component.html',
  styleUrls: [
    './roi-create.component.scss'
  ]
})
export class ROICreateComponent implements OnInit {
  @Output() ROICreateEvent = new EventEmitter<any>();
  @Input() renderingJob: RenderingJob;
  regions = [];
  templates = [];
  overlays = [];
  selectedROIType: any;
  selectedTemplate: any;
  selectedOverlay: any;
  selectedOverlayId: any;

  constructor(
    private appService: AppService,
    private renderingJobService: RenderingJobService,
    public activeModal: NgbActiveModal,
    private templateService: TemplateService,
    private tenantService: TenantService,
  ) { }

  async ngOnInit() {
    const templateIds = this.renderingJob.templateConfigurations.map(template => template.sourceTemplateId);

    if (templateIds.length) {
      try {
        const [regions, templates] = await Promise.all([
          this.tenantService.getRegionsByTenant(JSON.parse(localStorage.getItem('app-auth-user')).tenantId),
          this.templateService.findAnnotatedTemplatesByIds(templateIds.join(','))
        ]);

        this.regions = regions;
        this.templates = templates;
      } catch (err) {
        console.log(err);
      }
    }
  }

  onROITypeChange(e: any) {
    this.selectedROIType = e.target.value;
  }

  async onTemplateChange(e: any) {
    this.selectedTemplate = e;
    this.overlays = [];
    this.selectedOverlay = null;
    this.selectedOverlayId = null;

    if (!e) {
      return;
    }

    const overlayArr = this.renderingJob.templateConfigurations
      .find(template => template.sourceTemplateId === e.id).regionOverlays
      .filter(regionOverlay => regionOverlay.overlayTemplateId);

    if (overlayArr.length) {
      try {
        const res = await this.templateService.findTemplatesByIds(overlayArr.map(overlay => overlay.overlayTemplateId).join(','));

        this.overlays = overlayArr.map(overlay => {
          const region = this.regions.find((item) => item.id === overlay.regionId).name;
          const regionOverlayName = `${res.find((item) => item.id === overlay.overlayTemplateId).name} (${region})`;

          return {
            id: overlay.overlayTemplateId,
            name: regionOverlayName
          };
        });
      } catch (err) {
        console.log(err);
      }
    }
  }

  onOverlayChange(e: any) {
    this.selectedOverlay = e;
  }

  getROIConfigration() {
    return {
      type: this.selectedROIType,
      template: this.selectedTemplate,
      overlay: this.selectedOverlay
    };
  }

  addROI() {
    this.ROICreateEvent.emit(this.getROIConfigration());
    this.closeROIConfigurationModel();
  }

  closeROIConfigurationModel() {
    this.activeModal.close();
  }
}
