/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RenderingJob } from '../../model/rendering-job';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-scene-annotator',
  templateUrl: './scene-annotator.component.html',
  styleUrls: [
    './scene-annotator.component.scss'
  ]
})
export class SceneAnnotatorComponent implements OnInit, OnDestroy {
  renderingJob = new RenderingJob();
  sceneId: string;
  readonly returningJobTab = 'scenes';
  paramSubscription: Subscription;

  constructor(
    private appService: AppService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.appService.setHeader('Scene Annotator', ['Rendering Jobs', 'Scenes', 'Scene Annotation']);
  }

  ngOnInit() {
    this.paramSubscription = this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.renderingJob.id = params['id'];
        this.sceneId = params['sceneId'];
      }
    });
  }

  viewScenes() {
    this.router.navigate(['/rendering-jobs/process', this.renderingJob.id, this.returningJobTab]);
  }

  ngOnDestroy(): any {
    this.paramSubscription.unsubscribe();
  }
}
