/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../../app.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RenderingJob } from '../../model/rendering-job';
import { MediaService } from '../../../media/media.service';
import { Media } from '../../../media/model/media';
import { RenderingJobService } from '../../rendering-job.service';
import { VgAPI } from 'videogular2/core';
import { Template } from '../../../templates/model/template';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-media-preview',
  templateUrl: './media-preview.component.html',
  styleUrls: [
    './media-preview.component.scss'
  ]
})
export class MediaPreviewComponent implements OnInit {
  @Input() renderingJogInput: RenderingJob;
  @Input() regionId: string;

  mediaFile: Media;
  contentSource: string;
  isPreviewSupported = true;
  api: VgAPI;
  templateList: Template[] = [];

  constructor(
    private appService: AppService,
    private mediaService: MediaService,
    private renderingJobService: RenderingJobService,
    public activeModal: NgbActiveModal,
    private spinner: NgxSpinnerService
  ) {
    this.appService.setHeader('Media', ['Content Management', 'Media']);
  }

  async ngOnInit() {
    this.mediaFile = await this.mediaService.findById(this.renderingJogInput.mediaId);
    this.isPreviewSupported = this.mediaFile.mediaType === 'video/mp4';
    this.contentSource = this.renderingJobService.previewRenderedContentByRegionId(this.renderingJogInput.id, this.regionId);
  }

  onPlayerReady(api: VgAPI) {
    this.api = api;
  }

  getFrameNumberByPlaybackTime(playbackTimeObj) {
    return ((Number(this.mediaFile.videoMetadata.nb_frames) / playbackTimeObj.total) * playbackTimeObj.current).toFixed();
  }

  getTranslatedClickCoordinates(playerWidth, playerHeight, clickedPointX, clickedPointY) {
    return {
      pointX: (clickedPointX / playerWidth) * Number(this.mediaFile.videoMetadata.width),
      pointY: (clickedPointY / playerHeight) * Number(this.mediaFile.videoMetadata.height)
    };
  }

  async onVideoPlayerClick(e) {
    const playerApi = this.api;
    const currentFrame = this.getFrameNumberByPlaybackTime(playerApi.getDefaultMedia().time);
    this.spinner.show();

    const translatedClickCoordinatesObj = this.getTranslatedClickCoordinates(
      e.currentTarget.offsetWidth,
      e.currentTarget.offsetHeight,
      e.layerX,
      e.layerY
    );

    try {
      const template = await this.renderingJobService.getOverlayByCoordinatesValues(
        this.renderingJogInput.id,
        this.regionId,
        currentFrame,
        translatedClickCoordinatesObj.pointX,
        translatedClickCoordinatesObj.pointY
      );

      if (template) {
        this.templateList.push(template);
      }
    } catch (err) {
      console.log(err);
    } finally {
      this.spinner.hide();
    }
  }
}
