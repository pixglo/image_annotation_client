/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AppService } from '../../../app.service';
import { RenderingJobService } from '../../../rendering-jobs/rendering-job.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { RenderingJob } from '../../model/rendering-job';
import { Template } from '../../../templates/model/template';
import { TemplateService } from '../../../templates/template.service';
import { RegionOverlay } from '../../model/region-overlay';


@Component({
  selector: 'app-template-select',
  templateUrl: './template-select.component.html',
  styleUrls: [
    './template-select.component.scss'
  ]
})
export class TemplateSelectComponent implements OnInit {
  @Input() renderingJob: RenderingJob;
  @Input() templateType: string;
  @Output() templateSelectEvent = new EventEmitter<Template>();
  @Output() overlaySelectEvent = new EventEmitter<Template>();

  loading = true;
  content = [];
  page = PageMetadata.empty(10);
  searchQuery: string;
  searchTextChanged = new Subject<string>();
  searchSubscription;
  selectedTemplate: Template;

  constructor(
    private appService: AppService,
    private templateService: TemplateService,
    private renderingJobService: RenderingJobService,
    public activeModal: NgbActiveModal
  ) {
  }

  ngOnInit() {
    // Debounce Search
    this.searchSubscription = this.searchTextChanged
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .pipe(mergeMap(search => this.search(this.page.page)))
      .subscribe(() => {
      });
    this.search();
  }

  async search(page = 1) {
    this.loading = true;
    try {
      const pageResponse = await this.templateService.search(
        this.searchQuery, page, this.page.size, undefined, undefined, [this.templateType],
        (this.templateType === 'TEMPLATE' ? (this.renderingJob.mediaId || null) : null),
        (this.renderingJob.templateConfigurations === null ? undefined : this.renderingJob.templateConfigurations)
      );

      this.content = pageResponse.content;
      this.page = pageResponse.metadata;
    } catch (err) {
      console.log(err);
    } finally {
      this.loading = false;
    }
  }

  loadPage(evt) {
    this.search(evt.offset + 1);
  }

  triggerSearch($event) {
    this.searchTextChanged.next($event.target.value);
  }

  onPageChange(page) {
    this.search(page);
  }

  onTemplateSelect(template: Template) {
    this.selectedTemplate = this.selectedTemplate === template.id ? null : template;
  }

  async applySelection() {
    if (!this.selectedTemplate) {
      console.log('No media file is selected');
      return;
    }

    switch (this.templateType) {
      case 'TEMPLATE': this.templateSelectEvent.emit(this.selectedTemplate);
        break;
      case 'OVERLAY': this.overlaySelectEvent.emit(this.selectedTemplate);
        break;
      default: console.error(`Unknown Template Type: ${this.templateType}`);
    }

    this.activeModal.close();
  }

  cancelSelection() {
    this.activeModal.close();
  }
}
