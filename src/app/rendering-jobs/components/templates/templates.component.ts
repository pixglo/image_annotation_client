/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RenderingJob } from '../../model/rendering-job';
import { RenderingJobService } from '../../rendering-job.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Template } from '../../../templates/model/template';
import { TemplateSelectComponent } from '../template-select/template-select.component';
import { TemplateConfiguration } from '../../model/template-configuration';
import { TenantService } from '../../../tenants/tenant.service';
import { RegionOverlay } from '../../model/region-overlay';
import { Router } from '@angular/router';
import { TemplateService } from '../../../templates/template.service';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: [
    './templates.component.scss'
  ]
})
export class TemplatesComponent implements OnInit {
  @Input() renderingJob: RenderingJob;
  @Output() updateTabStatus = new EventEmitter<RenderingJob>();
  content = [];
  readonly typeTemplate = 'TEMPLATE';
  readonly typeOverlay = 'OVERLAY';
  readonly modalSize = 'lg';
  readonly modalWindowClass = 'modal-fill-in modal-lg';
  readonly defaultOverlayId = 'DEFAULT';
  readonly defaultOverlayName = 'Default';
  templateConfigurations: Array<TemplateConfiguration> = [];
  regions = [];
  isLoadingContent = true;

  constructor(
    private tenantService: TenantService,
    private templateService: TemplateService,
    private renderingJobService: RenderingJobService,
    private modalService: NgbModal,
    private router: Router,
  ) { }

  async ngOnInit() {
    this.renderingJob = await this.renderingJobService.findById(this.renderingJob.id);

    if (this.renderingJob.regions && this.renderingJob.regions.length) {
      let defaultOverlayIndex = this.renderingJob.regions.findIndex(region => region.id === this.defaultOverlayId);

      // Default option is not required when regions are present, therefore remove from array
      if (this.renderingJob.regions.length > 1 && defaultOverlayIndex > -1) {
        this.renderingJob.regions.splice(defaultOverlayIndex, 1);
        defaultOverlayIndex = -1;
      }

      if (defaultOverlayIndex === -1) {
        this.regions = await this.tenantService.getRegionsByIdsAndTenant(
          this.renderingJob.regions,
          JSON.parse(localStorage.getItem('app-auth-user')).tenantId
        );
      }
    } else {
      // Add default option if no regions are present
      this.regions = [{ id: this.defaultOverlayId, name: this.defaultOverlayName }];
    }

    if (this.renderingJob.templateConfigurations) {
      this.templateConfigurations = this.renderingJob.templateConfigurations;

      this.templateConfigurations.forEach((templateConfiguration, i) => {
        templateConfiguration.regionOverlays = this.regions.map(region => {
          const regionOverLay = new RegionOverlay();
          const regionIndex = templateConfiguration.regionOverlays.findIndex(overlay => overlay.regionId === region.id);
          regionOverLay.regionId = region.id;

          if (regionIndex > -1) {
            regionOverLay.overlayTemplateId = templateConfiguration.regionOverlays[regionIndex].overlayTemplateId;
            regionOverLay.overlayTemplateFileName = templateConfiguration.regionOverlays[regionIndex].overlayTemplateFileName;
          }

          return regionOverLay;
        });
      });
    }

    this.isLoadingContent = false;
  }

  selectTemplate(templateConfigurationIndex = null) {
    const modalRef = this.modalService.open(TemplateSelectComponent, { size: this.modalSize, windowClass: this.modalWindowClass });

    modalRef.componentInstance.renderingJob = this.renderingJob;
    modalRef.componentInstance.templateType = this.typeTemplate;

    modalRef.componentInstance.templateSelectEvent.subscribe((template: Template) => {
      if (templateConfigurationIndex !== null) {
        this.templateConfigurations[templateConfigurationIndex].sourceTemplateId = template.id;
        return;
      }

      const templateConfiguration = new TemplateConfiguration();
      templateConfiguration.sourceTemplateId = template.id;
      templateConfiguration.sourceTemplateFileName = template.fileName;

      templateConfiguration.regionOverlays = this.regions.map((region) => {
        const regionOverlay = new RegionOverlay();
        regionOverlay.regionId = region.id;
        return regionOverlay;
      });

      this.templateConfigurations.push(templateConfiguration);
    });
  }

  selectOverlay(templateConfigurationIndex: number, regionOverlayIndex: number) {
    const modalRef = this.modalService.open(TemplateSelectComponent, { size: this.modalSize, windowClass: this.modalWindowClass });

    modalRef.componentInstance.renderingJob = this.renderingJob;
    modalRef.componentInstance.templateType = this.typeOverlay;

    modalRef.componentInstance.overlaySelectEvent.subscribe((template: Template) => {
      this.templateConfigurations[templateConfigurationIndex].regionOverlays[regionOverlayIndex].overlayTemplateId = template.id;
      this.templateConfigurations[templateConfigurationIndex].regionOverlays[regionOverlayIndex].overlayTemplateFileName = template.fileName;
    });
  }

  removeTemplate(templateConfigurationIndex: number) {
    this.templateConfigurations.splice(templateConfigurationIndex, 1);

    if (!this.templateConfigurations.length) {
      this.saveConfiguration();
    }
  }

  removeOverlay(templateConfigurationIndex: number, regionOverlayIndex: number) {
    this.templateConfigurations[templateConfigurationIndex].regionOverlays[regionOverlayIndex].overlayTemplateId = null;
    this.templateConfigurations[templateConfigurationIndex].regionOverlays[regionOverlayIndex].overlayTemplateFileName = null;
  }

  async saveConfiguration() {
    this.renderingJob.templateConfigurations = this.templateConfigurations;
    await this.renderingJobService.updateTemplateConfigutations(this.renderingJob);
    console.log('Template configurations updated');
    this.updateTabStatus.emit(this.renderingJob);
  }

  async renderSourceVideo() {
    await this.saveConfiguration();
    await this.renderingJobService.renderSourceVideo(this.renderingJob.id);
    this.router.navigate(['/rendering-jobs']);
  }
}
