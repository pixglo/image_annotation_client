/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { MediaService } from '../../../media/media.service';
import { RenderingJob } from '../../model/rendering-job';
import { VgAPI } from 'videogular2/core';
import { Scene } from '../../../media/model/scene';
import { ActivatedRoute, Router } from '@angular/router';
import { RenderingJobService } from '../../rendering-job.service';

@Component({
  selector: 'app-media-scenes',
  templateUrl: './media-scenes.component.html',
  styleUrls: [
    './media-scenes.component.scss'
  ]
})
export class MediaScenesComponent implements OnInit {
  @Input() renderingJob: RenderingJob;

  scenes: Array<Scene>;
  sceneIndex: number;
  isPreviewSupported = false;
  currentIndex = 0;
  currentItem: any;
  api: VgAPI;
  isMediaConfigured = false;
  isLoadingContent = true;

  constructor(
    private mediaService: MediaService,
    private route: ActivatedRoute,
    private router: Router,
    private renderingJobService: RenderingJobService
  ) { }

  async ngOnInit() {
    if (this.renderingJob && this.renderingJob.id) {
      this.renderingJob = await this.renderingJobService.findById(this.renderingJob.id);

      if (this.renderingJob.mediaId) {
        this.isMediaConfigured = true;

        try {
          this.scenes = <Array<Scene>>await this.mediaService.findScenesByMediaId(this.renderingJob.mediaId);
          this.setCurrentlyPreviewingScene(this.scenes[this.currentIndex]);
        } catch (err) {
          console.log(err);
        }
      }
    }

    this.isLoadingContent = false;
  }

  setCurrentlyPreviewingScene(scene: Scene) {
    this.currentItem = scene;
    this.currentItem.src = this.mediaService.getSceneContentSource(scene.id);
  }

  onPlayerReady(api: VgAPI) {
    this.api = api;
    this.api.getDefaultMedia().subscriptions.loadedMetadata.subscribe(this.playVideo.bind(this));
    this.api.getDefaultMedia().subscriptions.ended.subscribe(this.nextVideo.bind(this));
  }

  nextVideo() {
    this.currentIndex++;
    if (this.currentIndex === this.scenes.length) {
      this.currentIndex = 0;
    }
    this.setCurrentlyPreviewingScene(this.scenes[this.currentIndex]);
  }

  playVideo() {
    this.api.play();
  }

  onClickPlaylistItem(index: number) {
    this.currentIndex = index;
    this.setCurrentlyPreviewingScene(this.scenes[index]);
  }

  annotateScene(sceneIndex: any) {
    this.router.navigate(['/rendering-jobs/process/', this.renderingJob.id, 'annotate', 'scene', sceneIndex]);
  }

}
