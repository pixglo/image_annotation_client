/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';
import { MediaService } from '../../../media/media.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SourceMediaSelectComponent } from '../../components/source-media-select/source-media-select.component';
import { Media } from '../../../media/model/media';
import { RenderingJob } from '../../model/rendering-job';
import { RenderingJobService } from '../../rendering-job.service';

@Component({
  selector: 'app-source-media',
  templateUrl: './source-media.component.html',
  styleUrls: [
    './source-media.component.scss'
  ]
})
export class SourceMediaComponent implements OnInit {
  @Input() renderingJob: RenderingJob;
  @Output() updateTabStatus = new EventEmitter<RenderingJob>();
  selectedMedia: Media;
  contentSource = '';
  isPreviewSupported = false;
  isLoadingContent = true;

  constructor(
    private appService: AppService,
    private mediaService: MediaService,
    private renderingJobService: RenderingJobService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  async ngOnInit() {
    this.renderingJob = await this.renderingJobService.findById(this.renderingJob.id);

    if (this.renderingJob.mediaId) {
      const media = await this.mediaService.findById(this.renderingJob.mediaId);
      this.setMediaContent(media);
    }

    this.isLoadingContent = false;
  }

  selectMedia() {
    const modalRef = this.modalService.open(SourceMediaSelectComponent, { size: 'lg', windowClass: 'modal-fill-in modal-lg' });

    modalRef.componentInstance.renderingJob = this.renderingJob;

    modalRef.componentInstance.mediaSelectEvent.subscribe((media: Media) => {
      this.setMediaContent(media);
      this.updateTabStatus.emit(this.renderingJob);
    });
  }

  setMediaContent(media: Media) {
    this.selectedMedia = media;
    this.isPreviewSupported = media.mediaType === 'video/mp4';
    this.contentSource = this.mediaService.getContentSource(media.id);
  }

  async clearSelectedMedia() {
    this.selectedMedia = null;
    this.renderingJob.mediaId = null;
    await this.renderingJobService.updateMedia(this.renderingJob);
    console.log('Media source cleared');
    this.updateTabStatus.emit(this.renderingJob);
  }
}
