/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RenderingJobService } from '../../rendering-job.service';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RenderingJobCreateComponent } from '../rendering-job-create/rendering-job-create.component';
import { RenderingJob } from '../../model/rendering-job';
import { TenantService } from '../../../tenants/tenant.service';
import { MediaPreviewComponent } from '../media-preview/media-preview.component';

@Component({
  selector: 'app-rendering-job-list',
  templateUrl: './rendering-job-list.component.html',
  styleUrls: [
    './rendering-job-list.component.scss'
  ]
})
export class RenderingJobListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  content = [];
  tenantRegions = [];
  page = PageMetadata.empty(10);
  searchQuery: string;
  searchTextChanged = new Subject<string>();
  searchSubscription;
  isLoadingContent = false;

  constructor(
    private appService: AppService,
    private renderingJobService: RenderingJobService,
    private tenantService: TenantService,
    private router: Router,
    private modalService: NgbModal,
    private route: ActivatedRoute
  ) {
    this.appService.setHeader('Rendering Jobs', ['Rendering Job Management', 'Jobs']);
  }

  async ngOnInit() {
    // Fetech tenant regions to map with rendering job region IDs
    this.tenantRegions = await this.tenantService.getRegionsByTenant(JSON.parse(localStorage.getItem('app-auth-user')).tenantId);

    // Debounce Search
    this.searchSubscription = this.searchTextChanged
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .pipe(mergeMap(search => this.search(this.page.page)))
      .subscribe(() => {
      });
    this.search();
  }

  async search(page = 1) {
    this.isLoadingContent = false;
    try {
      const pageResponse = await this.renderingJobService.search(this.searchQuery, page, this.page.size);

      this.content = pageResponse.content.map(renderingJob => {
        renderingJob.regions = renderingJob.regions ? this.getRegionsWithRenderStatus(renderingJob) : [];
        return renderingJob;
      });

      this.page = pageResponse.metadata;
    } catch (err) {
      console.log(err);
    } finally {
      this.isLoadingContent = false;
    }
  }

  getRegionsWithRenderStatus(renderingJob: RenderingJob) {
    return this.tenantRegions.filter(
      function (region) {
        return this.indexOf(region.id) >= 0;
      }, renderingJob.regions
    ).map(jobRegion => {
      console.log(renderingJob);
      return Object.assign({ renderingStatus: renderingJob.regionRenderingStatusMap[jobRegion.id] || null }, jobRegion);
    });
  }

  loadPage(e) {
    this.search(e.offset + 1);
  }

  triggerSearch(e) {
    this.searchTextChanged.next(e.target.value);
  }

  createRenderingJob() {
    const modalRef = this.modalService.open(RenderingJobCreateComponent, { size: 'lg', windowClass: 'modal-fill-in' });

    modalRef.componentInstance.renderingJobCreateEvent.subscribe((renderingJob: RenderingJob) => {
      this.processRenderingJob(renderingJob);
    });
  }

  processRenderingJob(renderingJob: RenderingJob) {
    this.router.navigate(['process', renderingJob.id], { relativeTo: this.route });
  }

  async deleteRenderingJob(renderingJob: RenderingJob) {
    await this.renderingJobService.delete(renderingJob);
    this.search();
  }

  toggleExpandRow(row) {
    if (!row.regions.length) {
      return;
    }
    this.table.rowDetail.toggleExpandRow(row);
  }

  previewRenderedContent(renderingJob: RenderingJob, regionId: string) {
    const modalRef = this.modalService.open(MediaPreviewComponent, { size: 'lg', windowClass: 'modal-fill-in modal-lg' });
    modalRef.componentInstance.renderingJogInput = renderingJob;
    modalRef.componentInstance.regionId = regionId;
  }

  // Download rendered video content for a given region ID
  downloadRenderedVideo(renderingJob: RenderingJob, regionId: string) {
    const url = this.renderingJobService.downloadRenderedContentByRegionId(renderingJob.id, regionId);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.href = url;
    a.download = renderingJob.mediaFileName;
    a.click();
    document.body.removeChild(a);
  }
}
