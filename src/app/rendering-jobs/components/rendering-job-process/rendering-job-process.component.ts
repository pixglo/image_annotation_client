/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RenderingJobService } from '../../rendering-job.service';
import { RenderingJob } from '../../model/rendering-job';

@Component({
  selector: 'app-rendering-job-process',
  templateUrl: './rendering-job-process.component.html',
  styleUrls: [
    './rendering-job-process.component.scss'
  ]
})
export class RenderingJobProcessComponent implements OnInit {
  activeTab = 'media';
  renderingJob = new RenderingJob();

  constructor(
    private appService: AppService,
    private renderingJobService: RenderingJobService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.appService.setHeader('Rendering Jobs', ['Media Processing', 'Rendering Jobs']);

    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.renderingJob.id = params['id'];

        if (params['tab'] !== undefined) {
          this.activeTab = params['tab'];
        }
      }
    });
  }

  async ngOnInit() {
    this.renderingJob = await this.renderingJobService.findById(this.renderingJob.id);
  }

  onTabChange(tab: string) {
    this.activeTab = tab;
  }

  onTabUpdated(renderingJob: RenderingJob) {
    this.renderingJob = renderingJob;
  }

  sceneAnnotateComplete() {
    this.renderingJob.isScenesAnnotated = true;
    this.renderingJobService.updateAnnotationStatus(this.renderingJob).subscribe(res => {
      this.renderingJob = res;
    });
  }

}
