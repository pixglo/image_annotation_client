/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AppService } from '../../../app.service';
import { MediaService } from '../../../media/media.service';
import { RenderingJobService } from '../../../rendering-jobs/rendering-job.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { Media } from '../../../media/model/media';
import { RenderingJob } from '../../model/rendering-job';


@Component({
  selector: 'app-source-media-select',
  templateUrl: './source-media-select.component.html',
  styleUrls: [
    './source-media-select.component.scss'
  ]
})
export class SourceMediaSelectComponent implements OnInit {
  @Input() renderingJob: RenderingJob;
  @Output() mediaSelectEvent = new EventEmitter<Media>();

  loading = true;
  content = [];
  page = PageMetadata.empty(10);
  searchQuery: string;
  searchTextChanged = new Subject<string>();
  searchSubscription;
  selectedSourceMedia: Media;

  constructor(
    private appService: AppService,
    private mediaService: MediaService,
    private renderingJobService: RenderingJobService,
    public activeModal: NgbActiveModal
  ) {
  }

  ngOnInit() {
    // Debounce Search
    this.searchSubscription = this.searchTextChanged
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .pipe(mergeMap(search => this.search(this.page.page)))
      .subscribe(() => {
      });
    this.search();
  }

  async search(page = 1) {
    this.loading = true;
    try {
      const pageResponse = await this.mediaService.search(this.searchQuery, page, this.page.size, null, true, true);
      this.content = pageResponse.content;
      this.page = pageResponse.metadata;
    } catch (err) {
      console.log(err);
    } finally {
      this.loading = false;
    }
  }

  loadPage(evt) {
    this.search(evt.offset + 1);
  }

  triggerSearch($event) {
    this.searchTextChanged.next($event.target.value);
  }

  onPageChange(page) {
    this.search(page);
  }

  onMediaSelect(media: Media) {
    this.selectedSourceMedia = this.selectedSourceMedia === media.id ? null : media;
  }

  async applySelection() {
    if (!this.selectedSourceMedia) {
      console.log('No media file is selected');
      return;
    }

    this.renderingJob.mediaId = this.selectedSourceMedia.id;
    await this.renderingJobService.updateMedia(this.renderingJob);

    this.mediaSelectEvent.emit(this.selectedSourceMedia);
    this.activeModal.close();
  }

  cancelSelection() {
    this.activeModal.close();
  }
}
