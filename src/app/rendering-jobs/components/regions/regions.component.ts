/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';
import { TenantService } from '../../../tenants/tenant.service';
import { RenderingJob } from '../../model/rendering-job';
import { RenderingJobService } from '../../rendering-job.service';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: [
    './regions.component.scss'
  ]
})
export class RegionsComponent implements OnInit {
  @Input() renderingJob: RenderingJob;
  @Output() updateTabStatus = new EventEmitter<RenderingJob>();
  regions = [];
  isLoadingContent = true;

  constructor(
    private appService: AppService,
    private tenantService: TenantService,
    private router: Router,
    private renderingJobService: RenderingJobService
  ) { }

  async ngOnInit() {
    const [renderingJob, regions] = await Promise.all([
      this.renderingJobService.findById(this.renderingJob.id),
      this.tenantService.getRegionsByTenant(JSON.parse(localStorage.getItem('app-auth-user')).tenantId)
    ]);

    this.regions = regions;
    this.renderingJob = renderingJob;

    if (!this.renderingJob.regions) {
      this.renderingJob.regions = [];
    }

    this.isLoadingContent = false;
  }

  onRegionCheckChange(e, regionId: string) {
    const existingRegionIndex = this.renderingJob.regions.indexOf(regionId);

    if (e.target.checked && existingRegionIndex < 0) {
      this.renderingJob.regions.push(regionId);
    } else {
      this.renderingJob.regions.splice(existingRegionIndex, 1);
    }
  }

  async saveRegions() {
    await this.renderingJobService.updateRegions(this.renderingJob);
    console.log('Regions updated succesfully');
    this.updateTabStatus.emit(this.renderingJob);
  }
}
