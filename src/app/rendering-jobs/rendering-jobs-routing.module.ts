/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Layout2Component } from '../layout/layout-2/layout-2.component';
import { RenderingJobListComponent } from './components/rendering-job-list/rendering-job-list.component';
import { RenderingJobProcessComponent } from './components/rendering-job-process/rendering-job-process.component';
import { SceneAnnotatorComponent } from './components/scene-annotator/scene-annotator.component';

const routes: Routes = [
  {
    path: '',
    component: Layout2Component,
    children: [
      { path: '', component: RenderingJobListComponent },
      { path: 'process/:id', component: RenderingJobProcessComponent },
      { path: 'process/:id/:tab', component: RenderingJobProcessComponent },
      { path: 'process/:id/annotate/scene/:sceneId', component: SceneAnnotatorComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class RenderingJobsRoutingModule { }
