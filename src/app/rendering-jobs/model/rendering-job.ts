/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { TemplateConfiguration } from './template-configuration';
import { Media } from '../../media/model/media';

export class RenderingJob {

  static from(obj): RenderingJob {
    const renderingJob = new RenderingJob();
    Object.assign(renderingJob, obj);
    return renderingJob;
  }

  constructor(
    public id?: string,
    public name?: string,
    public tenantId?: string,
    public mediaId?: string,
    public mediaFileName?: string,
    public regions?: Array<any>,
    public templateConfigurations?: Array<TemplateConfiguration>,
    public isScenesAnnotated?: boolean,
    public distributionConfigurations?: Array<any>,
    public mediaFile?: Media,
    public regionRenderingStatusMap?: any
  ) { }

}
