/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RenderingJob } from './model/rendering-job';
import { Subscription } from 'rxjs';
import { AppService } from '../app.service';
import { NotificationService } from '../shared/notification.service';
import { Page } from '../shared/model/paging/page';
import { FtpConfiguration } from './model/FtpConfiguration';
import { AppConfig } from '../shared/model/config/app-config';
import { Template } from '../templates/model/template';

@Injectable({
  providedIn: 'root'
})
export class RenderingJobService {
  private readonly renderingJobsApi = '/v1/rendering-jobs';
  appConfigSubscription: Subscription;
  config = new AppConfig();

  constructor(
    private appService: AppService,
    private httpClient: HttpClient,
    private notificationService: NotificationService
  ) {
    this.appConfigSubscription = this.appService.appConfig$.subscribe((config) => {
      this.config = config;
    });
  }

  async search(query?: string, page?: number, size?: number, sort?: string): Promise<Page<RenderingJob>> {
    let params = new HttpParams();
    if (query) { params = params.set('query', query); }
    if (page) { params = params.set('page', page.toString()); }
    if (size) { params = params.set('size', size.toString()); }
    if (sort) { params = params.set('sort', sort); }

    console.log('Searching for rendering jobs with params', params.toString());
    const response = await this.httpClient.get(this.renderingJobsApi, { params: params }).toPromise();

    return Page.build(response, RenderingJob.from);
  }

  async create(renderingJob: RenderingJob) {
    const res = await this.httpClient.post(`${this.renderingJobsApi}`, renderingJob).toPromise();
    this.notificationService.showSuccess('job.create.success');
    return res;
  }

  async findById(renderingJobId: string) {
    return this.httpClient.get(`${this.renderingJobsApi}/${renderingJobId}`).toPromise();
  }

  async updateMedia(renderingJob: RenderingJob) {
    const res = await this.httpClient.put(`${this.renderingJobsApi}/${renderingJob.id}/media`, renderingJob).toPromise();
    this.notificationService.showSuccess('media.source.updated');
    return res;
  }

  async updateRegions(renderingJob: RenderingJob) {
    const res = this.httpClient.put(`${this.renderingJobsApi}/${renderingJob.id}/regions`, renderingJob).toPromise();
    this.notificationService.showSuccess('regions.updated');
    return res;
  }

  async updateTemplateConfigutations(renderingJob: RenderingJob) {
    const res = await this.httpClient.put(`${this.renderingJobsApi}/${renderingJob.id}/templates`, renderingJob).toPromise();
    this.notificationService.showSuccess('template.configurations.updated');
    return res;
  }

  async updateDistributionconfigurations(renderingJobId: string, ftpConfiguration: FtpConfiguration) {
    const res = await this.httpClient.put(`${this.renderingJobsApi}/${renderingJobId}/distributions`, ftpConfiguration).toPromise();
    this.notificationService.showSuccess('distribution.configurations.updated');
    return res;
  }

  verifyFtpConnection(ftpConfiguration: FtpConfiguration) {
    return this.httpClient.post(`${this.renderingJobsApi}/verify-ftp-connection`, ftpConfiguration).toPromise();
  }

  updateAnnotationStatus(renderingJob: RenderingJob) {
    return this.httpClient.put(`${this.renderingJobsApi}/annotation-status`, renderingJob);
  }

  async delete(renderingJob: RenderingJob) {
    const message = 'job.delete.confirm';
    const messageValues = { name: `${renderingJob.name}` };
    await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
      await this.httpClient.delete(`${this.renderingJobsApi}/${renderingJob.id}`).toPromise();
      this.notificationService.showSuccess('job.delete.success', messageValues);
    });
  }

  async renderSourceVideo(renderingJobId: string) {
    return this.httpClient.post(`${this.renderingJobsApi}/${renderingJobId}/render`, null).toPromise();
  }

  previewRenderedContentByRegionId(renderingJobId: string, regionId: string): string {
    return `${this.config.baseUrl}${this.renderingJobsApi}/${renderingJobId}/regions/${regionId}/preview-content`;
  }

  downloadRenderedContentByRegionId(renderingJobId: string, regionId: string): string {
    return `${this.config.baseUrl}${this.renderingJobsApi}/${renderingJobId}/regions/${regionId}/download-content`;
  }

  async getOverlayByCoordinatesValues(id: string, regionId: string, currentFrame: string, pointX: Number, pointY: Number)
    : Promise<Template> {
    let params = new HttpParams();
    if (currentFrame) { params = params.set('currentFrame', currentFrame); }
    if (pointX) { params = params.set('pointX', pointX.toString()); }
    if (pointY) { params = params.set('pointY', pointY.toString()); }
    return <Template>await this.httpClient.get(
      `${this.renderingJobsApi}/${id}/regions/${regionId}/action-response`, { params: params }
    ).toPromise();
  }
}
