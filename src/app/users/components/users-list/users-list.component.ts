/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../model/user';
import { UserService } from '../../user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { Subject } from 'rxjs';
import { AppService } from '../../../app.service';
import { NotificationService } from '../../../shared/notification.service';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { AuthenticationService } from '../../../auth/authentication.service';
import { RoleService } from '../../../roles/role.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  currentUserId;
  loading = true;
  content = [];
  page = PageMetadata.empty(10, 'firstName');
  isSuperAdmin = true;
  roles = [];

  // Search
  searchQuery: string;
  searchTextChanged = new Subject<string>();
  searchSubscription;

  constructor(private appService: AppService,
    private userService: UserService,
    private roleService: RoleService,
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute) {
    this.appService.setHeader('Users', ['Access Control', 'Users']);
    this.currentUserId = this.authService.getUser().userId;
    this.isSuperAdmin = this.authService.getUser().tenantId == null ? true : false;
  }

  async ngOnInit() {
    // Fetch roles (used to map with user role IDs)
    this.roles = await this.roleService.findRoles();

    // Debounce Search
    this.searchSubscription = this.searchTextChanged
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .pipe(mergeMap(search => this.search(this.page.page)))
      .subscribe(() => {
      });
    this.search();
  }

  async search(page = 1) {
    this.loading = true;
    try {
      const pageResponse = await this.userService.search(this.searchQuery, page, this.page.size, this.page.sort);

      this.content = pageResponse.content.map(user => {
        return Object.assign({ roleNames: this.getUserRoleNames(user) }, user);
      });

      this.page = pageResponse.metadata;
      console.log('Search updated');
    } catch (err) {
      console.log(err);
    } finally {
      this.loading = false;
    }
  }

  getUserRoleNames(user) {
    return this.roles.filter(
      function (role) {
        return this.indexOf(role.value) >= 0;
      }, user.roleIds
    ).map(role => role.label).join(', ');
  }

  async del(user: User) {
    await this.userService.del(user);
    console.log('Deleted, Searching');
    this.search();
  }

  async reactivate(user: User) {
    await this.userService.reactivate(user);
    console.log('Reactivated, Searching');
    this.search();
  }

  async deactivate(user: User) {
    await this.userService.deactivate(user);
    console.log('Deactivated, Searching');
    this.search();
  }

  edit(user: User) {
    this.router.navigate(['edit', user.id], { relativeTo: this.route });
  }

  triggerActivationMail(user: User) {
    this.userService.triggerActivationMail(user);
  }

  triggerPasswordResetMail(user: User) {
    this.userService.triggerPasswordResetMail(user);
  }

  triggerSearch($event) {
    // Called by the search bar
    this.searchTextChanged.next($event.target.value);
  }
}
