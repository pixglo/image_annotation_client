/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';
import { UserService } from '../../user.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../../../app.service';
import { AuthenticationService } from '../../../auth/authentication.service';
import { AuthenticatedUser } from '../../../auth/model/authenticated-user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  authenticatedUser: AuthenticatedUser;
  userForm: FormGroup;
  selectedUser: User;
  systemAdminRoleSelected = false;
  isAuthUserSearched = true;

  constructor(
    private appService: AppService,
    private userService: UserService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private location: Location
  ) {
    this.userForm = this.buildForm();
  }

  async ngOnInit() {
    this.authenticatedUser = this.authService.getUser();

    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        const selectedId = params['id'];

        if (this.authenticatedUser.userId !== selectedId) {
          this.isAuthUserSearched = false;
          return;
        }

        this.activateReadOnlyMode(selectedId);
      }
    });
  }

  private async activateReadOnlyMode(id: any) {
    this.appService.setHeader('User Profile', ['User', 'Profile']);

    this.selectedUser = <User>await this.userService.findProfileById(id);
    this.selectedUser.roleNames = this.selectedUser.roles.map(role => role.name).join(', ');
    this.userForm.patchValue(this.selectedUser);

    // Disable entire form
    this.userForm.disable();
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      tenantName: ['', Validators.required],
      roleNames: ['', Validators.required],
    });
  }

  backNavigate() {
    this.location.back();
  }
}
