/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';
import { UserService } from '../../user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../../../app.service';
import { TenantService } from '../../../tenants/tenant.service';
import { NotificationService } from '../../../shared/notification.service';
import { RoleService } from '../../../roles/role.service';

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.scss']
})
export class UsersCreateComponent implements OnInit {

  userForm: FormGroup;
  isEditMode = true;
  tenants = [];
  roles = [];
  selectedUser: User;
  systemAdminRoleSelected = false;
  disableTenantSelection = false;

  constructor(
    private appService: AppService,
    private userService: UserService,
    private tenantService: TenantService,
    private roleService: RoleService,
    private route: ActivatedRoute,
    private router: Router,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder
  ) {
    this.userForm = this.buildForm();
  }

  async ngOnInit() {

    const [tenants, roles] = await Promise.all([
      this.tenantService.findTenantList(),
      this.roleService.findRoles()
    ]);

    this.tenants = tenants;
    this.roles = roles;

    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        const selectedId = params['id'];
        this.activateEditMode(selectedId);
      } else {
        this.activateCreateMode();

        const authUser = JSON.parse(localStorage.getItem('app-auth-user'));
        if (authUser.tenantId !== null) {
          this.userForm.controls['tenantId'].patchValue(authUser.tenantId);
          this.disableTenantSelection = true;
        }

      }
    });
  }

  private activateCreateMode(): void {
    this.isEditMode = false;
    this.appService.setHeader('Users', ['Access Control', 'Users', 'Create User']);
  }

  private async activateEditMode(id: any) {
    this.isEditMode = true;
    this.appService.setHeader('Users', ['Access Control', 'Users', 'Edit User']);

    // Load Role
    this.selectedUser = <User>await this.userService.findById(id);
    console.log('Loading user', this.selectedUser);

    if (this.selectedUser.roleIds.length > 0) {

      const systemAdministratorRoleId = this.getSystemAdministratorRoleId();

      if (this.selectedUser.roleIds.includes(systemAdministratorRoleId)) {
        this.systemAdminRoleSelected = true;
        this.userForm.controls['tenantId'].clearValidators();
        this.userForm.get('tenantId').updateValueAndValidity();
      }
    }

    this.userForm.patchValue(this.selectedUser);

    // Disable non-editable fields
    this.userForm.controls['tenantId'].disable();

  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      tenantId: [undefined, Validators.required],
      roleIds: [[], Validators.required],
    });
  }

  async save() {
    if (this.userForm.invalid) {
      return;
    }

    // Remove empty tenantId property from user form object
    if (this.userForm.value.tenantId === '') {
      delete this.userForm.value.tenantId;
    }

    if (this.isEditMode) {
      await this.update();
    } else {
      await this.create();
    }
  }

  private async create() {
    await this.userService.create(this.userForm.value);
    this.router.navigate(['users']);
  }

  private async update() {
    await this.userService.update(Object.assign({ id: this.selectedUser['id'] }, this.userForm.value));
    this.router.navigate(['users']);
  }

  async del() {
    await this.userService.del(this.selectedUser);
    this.router.navigate(['users']);
  }

  async cancel() {
    if (this.userForm.dirty) {
      await this.notificationService.showChangesConfirmation();
    }
    this.router.navigate(['/users']);
  }

  roleChange(e) {
    if (this.userForm.get('roleIds').value.length === 0) {
      this.systemAdminRoleSelected = false;
      this.userForm.controls['tenantId'].setValidators([Validators.required]);
      this.userForm.get('tenantId').updateValueAndValidity();
      return;
    }

    const systemAdministratorRoleId = this.getSystemAdministratorRoleId();

    console.log(systemAdministratorRoleId);

    if (this.userForm.get('roleIds').value.length > 0) {
      const roles = this.userForm.get('roleIds').value;

      if (roles.includes(systemAdministratorRoleId)) {
        this.userForm.controls['roleIds'].patchValue([systemAdministratorRoleId]);
        this.systemAdminRoleSelected = true;
        this.userForm.controls['tenantId'].patchValue('');
        this.userForm.controls['tenantId'].clearValidators();
        this.userForm.get('tenantId').updateValueAndValidity();
      }
    }
  }

  private getSystemAdministratorRoleId(): string {
    let systemAdministratorRoleId = undefined;
    const filteredRoleList = this.roles.filter(role => role.label === 'System Administrator');

    if (filteredRoleList.length) {
      systemAdministratorRoleId = filteredRoleList[0].value;
    }
    console.log(systemAdministratorRoleId);
    return systemAdministratorRoleId;
  }
}
