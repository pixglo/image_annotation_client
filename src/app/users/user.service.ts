/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {User} from './model/user';
import {Role} from '../roles/model/role';
import {Page} from '../shared/model/paging/page';
import {NotificationService} from '../shared/notification.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiBase = '/v1/users';

  constructor(private httpClient: HttpClient, private notificationService: NotificationService) { }

  async create(user: User) {
    await this.httpClient.post(`${this.apiBase}`, user).toPromise();
    console.log('Created user', user);
    this.notificationService.showSuccess('user.create.success');
  }

  async update(user: User) {
    await this.httpClient.put(`${this.apiBase}/${user.id}`, user).toPromise();
    console.log('Updated user', user);
    this.notificationService.showSuccess('user.update.success');
  }

  async del(user: User) {
    const message = 'user.delete.confirm';
    const messageValues = {name : `${user.firstName} ${user.lastName}`};
    await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
      console.log('Deleting', user);
      await this.httpClient.delete(`${this.apiBase}/${user.id}`).toPromise();
      console.log('Deleted', user);
      this.notificationService.showSuccess('user.delete.success', messageValues);
    });
  }

  async findById(userId: string) {
    if (! userId) {
      throw new Error('User ID is required for findById');
    }
    return this.httpClient.get(`${this.apiBase}/${userId}`).toPromise();
  }

  async findProfileById(userId: string) {
    if (!userId) {
      throw new Error('User ID is required for findById');
    }

    return this.httpClient.get(`${this.apiBase}/${userId}/profile`).toPromise();
  }

  async search(query?: string, page?: number, size?: number, sort?: string): Promise<Page<Role>> {

    let params = new HttpParams();
    if (query) { params = params.set('query', query); }
    if (page) { params = params.set('page', page.toString()); }
    if (size) { params = params.set('size', size.toString()); }
    if (sort) { params = params.set('sort', sort); }

    console.log('Searching for roles with params', params.toString());
    const response = await this.httpClient.get(`${this.apiBase}`, { params: params}).toPromise();
    return Page.build(response, Role.from);
  }

  async reactivate(user: User) {
    const messageValues = {name : `${user.firstName} ${user.lastName}`};
    const payload = { status: 'ACTIVE' };
    await this.httpClient.put(`${this.apiBase}/${user.id}/status`, payload).toPromise();
    console.log('Reactivated user', user);
    this.notificationService.showSuccess('user.reactivate.success', messageValues);
  }

  async deactivate(user: User) {
    const message = 'user.deactivate.confirm';
    const messageValues = {name : `${user.firstName} ${user.lastName}`};
    await this.notificationService.showYesNoConfirmation(message, messageValues).then(async () => {
      const payload = { status: 'INACTIVE' };
      await this.httpClient.put(`${this.apiBase}/${user.id}/status`, payload).toPromise();
      console.log('Deactivated user', user);
      this.notificationService.showSuccess('user.deactivate.success', messageValues);
    });
  }

  async triggerActivationMail(user: User) {
    const message = 'user.activation.mail.confirm';
    const messageValues = {name : `${user.firstName} ${user.lastName} (${user.email})`};
    await this.notificationService.showYesNoConfirmation(message, messageValues).then(async () => {
      await this.httpClient.post(`${this.apiBase}/${user.id}/activation`, {}).toPromise();
      console.log('Triggered activation email for user', user);
      this.notificationService.showSuccess('user.activation.mail.success', messageValues);
    });
  }

  async triggerPasswordResetMail(user: User) {
    const message = 'user.password.reset.mail.confirm';
    const messageValues = {name : `${user.firstName} ${user.lastName} (${user.email})`};
    await this.notificationService.showYesNoConfirmation(message, messageValues).then(async () => {
      await this.httpClient.post(`${this.apiBase}/${user.id}/password-reset`, {}).toPromise();
      console.log('Triggered password reset email for user', user);
      this.notificationService.showSuccess('user.password.reset.mail.success', messageValues);
    });
  }

}
