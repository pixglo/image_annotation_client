/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
  'user.not.found' : 'User (${value}) does not exist.',
  // "user.in.use.by.users" : "Cannot delete this user as it is being used by ${userCount} users.",
  'user.system.user.cannot.delete' : 'System users cannot be deleted.',
  'user.name.already.exists' : 'User name "${value}" is in use. Please use a different name.',

  'user.create.success' : 'User was created successfully.',
  'user.create.success.title' : 'User Created',
  'user.update.success' : 'User was updated successfully.',
  'user.update.success.title' : 'User Updated',

  'user.delete.confirm' : 'Are you sure to delete "${name}" ?',
  'user.delete.confirm.title' : 'Delete User',
  'user.delete.success' : 'User "${name}" was deleted successfully.',
  'user.delete.success.title' : 'User Deleted',

  'user.reactivate.success' : 'User "${name}" was reactivated successfully.',
  'user.reactivate.success.title' : 'Re-Activated User',

  'user.deactivate.success' : 'User "${name}" was deactivated successfully.',
  'user.deactivate.success.title' : 'Deactivated User',

  'user.deactivate.confirm' : 'Are you sure to deactivate "${name}"?',
  'user.deactivate.confirm.title' : 'Deactivate User',

  'user.activation.mail.confirm' : 'Are you sure to send a new activation link email to "${name}"?',
  'user.activation.mail.confirm.title' : 'Activation Email',

  'user.activation.mail.success' : 'An email has been sent to ${name} with an activation link.',
  'user.activation.mail.success.title' : 'Activation Email Sent',

  'user.password.reset.mail.confirm' : 'Are you sure you want to send a new password reset link email to "${name}"?',
  'user.password.reset.mail.confirm.title' : 'Password Reset Email',

  'user.password.reset.mail.success': 'An email has been sent to ${name} with a password reset link.',
  'user.password.reset.mail.success.title': 'Password Reset Email Sent'
};
