/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersCreateComponent } from './components/users-create/users-create.component';
import { UsersRoutingModule } from './users-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MessageService } from '../shared/message.service';
import { messages } from './users-messages';
import { UserProfileComponent } from './components/user-profile/user-profile.component';

@NgModule({
  declarations: [UsersListComponent, UsersCreateComponent, UserProfileComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgSelectModule,
    UsersRoutingModule
  ]
})
export class UsersModule {
  constructor(
    private messageService: MessageService
  ) {
    this.messageService.register(messages);
  }
}

