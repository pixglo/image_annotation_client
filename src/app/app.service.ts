/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig } from './shared/model/config/app-config';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AppService {

    headerChangeSubject: Subject<string[]> = new Subject<string[]>();
    appConfig$: BehaviorSubject<AppConfig> = new BehaviorSubject<AppConfig>(new AppConfig());

    constructor(private titleService: Title,
        private httpClient: HttpClient) {
        this.httpClient.get('/v1/public/configuration')
            .subscribe(value => {
                this.appConfig$.next(<AppConfig>value);
            });
    }

    // Set page title
    set pageTitle(value: string) {
        this.titleService.setTitle(`${value} - Pixglo Studio`);
    }

    setHeader(module: string, headerValues: string[]) {
        console.log('Setting Page Header to ', headerValues);
        this.pageTitle = module;
        this.headerChangeSubject.next(headerValues);
        console.log('Header Change Subject Changed');
    }

    // Check for RTL layout
    get isRTL() {
        return document.documentElement.getAttribute('dir') === 'rtl' ||
            document.body.getAttribute('dir') === 'rtl';
    }

    // Check if IE10
    get isIE10() {
        return typeof document['documentMode'] === 'number' && document['documentMode'] === 10;
    }

    // Layout navbar color
    get layoutNavbarBg() {
        return 'navbar-theme';
    }

    // Layout sidenav color
    get layoutSidenavBg() {
        return 'sidenav-theme';
    }

    // Layout footer color
    get layoutFooterBg() {
        return 'footer-theme';
    }

    // Animate scrollTop
    scrollTop(to: number, duration: number, element = document.scrollingElement || document.documentElement) {
        if (element.scrollTop === to) {
            return;
        }
        const start = element.scrollTop;
        const change = to - start;
        const startDate = +new Date();

        // t = current time; b = start value; c = change in value; d = duration
        const easeInOutQuad = (t, b, c, d) => {
            t /= d / 2;
            if (t < 1) {
                return c / 2 * t * t + b;
            }
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        };

        const animateScroll = function () {
            const currentDate = +new Date();
            const currentTime = currentDate - startDate;
            element.scrollTop = parseInt(easeInOutQuad(currentTime, start, change, duration), 10);
            if (currentTime < duration) {
                requestAnimationFrame(animateScroll);
            } else {
                element.scrollTop = to;
            }
        };

        animateScroll();
    }

    // Filter:: Api
    getSearchFields(params: any = {}, type: string) {
        return this.httpClient.post('/api/' + type + '/search', params);
    }
}
