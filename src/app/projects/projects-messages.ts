/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
    'project.not.found': 'Project (${value}) does not exist.',
    'project.name.already.exists': 'Project name "${value}" is in use. Please use a different name.',

    'project.create.success': 'Project was created successfully.',
    'project.create.success.title': 'Project Created',
    'project.update.success': 'Project was updated successfully.',
    'project.update.success.title': 'Project Updated',

    'project.delete.confirm': 'Are you sure to delete "${name}" ?',
    'project.delete.confirm.title': 'Delete Project',
    'project.delete.success': 'Project "${name}" was deleted successfully.',
    'project.delete.success.title': 'Project Deleted',
    'project.file.invalid.title': 'Project Upload Image',
    'project.file.invalid': 'Invalid image extension. Valid extension: "${extension}"',
    'project.file.success.title': 'Project Upload Image',
    'project.file.success': 'Image uploaded successfully..',
    'project.file.save.title': 'Project Upload Image',
    'project.file.save': 'Image was saved successfully..',
    'file.delete.confirm.title': 'Delete Image',
    'file.delete.confirm': 'Are you sure to delete "${name}" ?',
    'file.delete.success': 'Image "${name}" was deleted successfully.',
    'file.delete.success.title': 'Image Deleted',
    'annotation.save.success': 'Image Annotation was saved successfully.',
    'annotation.save.success.title': 'Image Annotation',
    'project.bulk.invalid': 'Select record(s) to download that record annotated data.',
    'project.bulk.invalid.title': 'Bulk Download',
    'project.bulk.nodata': 'No data available to download. First create annotated data, after that you can download.',
    'project.bulk.nodata.title': 'Bulk Download'
};
