/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Page } from '../shared/model/paging/page';
import { NotificationService } from '../shared/notification.service';
import { Project } from './model/projects';
import { Subscription, Observable } from 'rxjs';
import { AppConfig } from '../shared/model/config/app-config';
import { Media } from './model/media';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {
    private readonly mediaApi = '/api/media-files';
    private readonly annotatedMediaApi = '/api/annotated-media-files';
    private apiBase = '/api/annotation-projects';
    private mediafile: Media;
    uploadMediaPath = '';
    allowImageMediaType = [];
    imageProcessStatus = {};
    defaultImageStatus = {};
    appConfigSubscription: Subscription;
    config = new AppConfig();

    constructor(private httpClient: HttpClient, private notificationService: NotificationService) {
        this.uploadMediaPath = this.config.uploadMediaPath;
        this.allowImageMediaType = this.config.allowImageMediaType;
        this.imageProcessStatus = this.config.imageProcessStats;
        this.defaultImageStatus = this.config.defaultImageStatus;
    }

    async create(project: Project) {
        await this.httpClient.post(`${this.apiBase}` + '/create', project).toPromise();
        console.log('Created project', project);
        this.notificationService.showSuccess('project.create.success');
    }

    async update(project: Project) {
        await this.httpClient.post(`${this.apiBase}/update/${project._id}`, project).toPromise();
        console.log('Updated project', project);
        this.notificationService.showSuccess('project.update.success');
    }

    async del(project: Project) {
        const message = 'project.delete.confirm';
        const messageValues = { name: `${project.name}` };
        await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
            console.log('Deleting', project);
            await this.httpClient.post(`${this.apiBase}/delete/${project._id}`, {}).toPromise();
            console.log('Deleted', project);
            this.notificationService.showSuccess('project.delete.success', messageValues);
        });
    }

    async findById(recordId: string) {
        if (!recordId) {
            throw new Error('Project ID is required for findById');
        }
        return this.httpClient.post(`${this.apiBase}/get/${recordId}`, {}).toPromise();
    }

    // tslint:disable-next-line:max-line-length
    async search(query?: string, page?: number, size?: number, sort?: string, isSearch?: boolean, objFilter?: Object): Promise<Page<Project>> {

        // let params = new HttpParams();
        const queryParam = {
            params: {
                query: '',
                page: '',
                size: '',
                sort: ''
            },
            filter: {}
        };
        if (query) { queryParam.params.query = query; } else { queryParam.params.query = undefined; }
        if (page) { queryParam.params.page = page.toString(); } else { queryParam.params.page = undefined; }
        if (size) { queryParam.params.size = size.toString(); } else { queryParam.params.size = undefined; }
        if (sort) { queryParam.params.sort = sort; } else { queryParam.params.sort = undefined; }
        if (isSearch) { queryParam.filter = objFilter; } else { queryParam.filter = undefined; }
        // if (isSearch) {  } else { queryParam.filter = undefined; }

        console.log('Searching for project with params', queryParam);
        const response = await this.httpClient.post(`${this.apiBase}` + '/list', queryParam).toPromise();
        return Page.build(response, Project.from);
    }

    async searchMedia(query?: string, page?: number, size?: number, sort?: string, projectId?: string): Promise<Page<Project>> {

        // let params = new HttpParams();
        const queryParam = {
            query: '',
            page: '',
            size: '',
            sort: '',
            projectId: ''
        };
        if (query) { queryParam.query = query; } else { queryParam.query = undefined; }
        if (page) { queryParam.page = page.toString(); } else { queryParam.page = undefined; }
        if (size) { queryParam.size = size.toString(); } else { queryParam.size = undefined; }
        if (sort) { queryParam.sort = sort; } else { queryParam.sort = undefined; }
        if (projectId) { queryParam.projectId = projectId; } else { queryParam.projectId = undefined; }

        console.log('Searching for image with params', queryParam);
        const response = await this.httpClient.post(`${this.mediaApi}` + '/list', { params: queryParam }).toPromise();
        return Page.build(response, Project.from);
    }

    async delMedia(project: Project) {
        const message = 'file.delete.confirm';
        const messageValues = { name: `${project.name}` };
        await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
            console.log('Deleting', project);
            await this.httpClient.post(`${this.mediaApi}/delete/${project._id}`, {}).toPromise();
            console.log('Deleted', project);
            this.notificationService.showSuccess('file.delete.success', messageValues);
        });
    }

    getMaxFileSizeMediaMB(): number {
        return this.config.maxFileSizeMediaMB;
    }

    getMediaFile() {
        return this.mediafile;
    }
    async processMediaContent(mediaId: string, uploadURL: string) {
        return this.httpClient.post(`${this.mediaApi}/${mediaId}`, { uploadURL }).toPromise();
    }

    async updateMediaFile(media: Media) {
        await this.httpClient.put(`${this.mediaApi}/${media.id}`, media).toPromise();
        this.notificationService.showSuccess('media.update.success');
    }

    async saveMediaFile(mediaFile: Media) {
        return this.httpClient.post(`${this.mediaApi}`, mediaFile).toPromise();
    }

    setMediaFile(mediaFile: Media) {
        this.mediafile = mediaFile;
    }

    updateMediaFileProject(params) {
        return this.httpClient.post(`${this.mediaApi}` + '/update/' + params.projectId, params);
    }

    createMediaFileProject(params) {
        return this.httpClient.post(`${this.mediaApi}` + '/create', params);
    }

    async searchAnnotatedMedia(query?: string, page?: number, size?: number, sort?: string, projectId?: string, selected_imageId?: string) {

        // let params = new HttpParams();
        const queryParam = {
            query: '',
            page: '',
            size: '',
            sort: '',
            projectId: '',
            selected_imageId: ''
        };
        if (query) { queryParam.query = query; } else { queryParam.query = undefined; }
        if (page) { queryParam.page = page.toString(); } else { queryParam.page = undefined; }
        if (size) { queryParam.size = size.toString(); } else { queryParam.size = undefined; }
        if (sort) { queryParam.sort = sort; } else { queryParam.sort = undefined; }
        if (projectId) { queryParam.projectId = projectId; } else { queryParam.projectId = undefined; }
        if (selected_imageId) { queryParam.selected_imageId = selected_imageId; } else { queryParam.selected_imageId = undefined; }

        const response = await this.httpClient.post(`${this.mediaApi}` + '/list', { params: queryParam }).toPromise();
        return Page.build(response, Project.from);
    }

    updateMediaAnnotation(params) {
        return this.httpClient.post(`${this.mediaApi}` + '/update/' + params.mediaId, params);
    }


    downloadJson(params) {
        return this.httpClient.post(`${this.mediaApi}` + '/export', { params: params }).subscribe((response) => {
            this.httpClient.get(`${this.uploadMediaPath + response['item']['fileName']}`, { responseType: 'blob' as 'json' })
                .subscribe(readFile => this.downLoadFile(readFile, 'application/json', response['item']['fileName']));
        });
    }

    /**
     * Method is use to download file.
     * @param data - Array Buffer data
     * @param type - type of the document.
     */
    downLoadFile(data: any, type: string, filename: string) {
        const blob = new Blob([data], { type: type });

        // IE doesn't allow using a blob object directly as link href
        // instead it is necessary to use msSaveOrOpenBlob
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob);
            return;
        }

        const url = window.URL.createObjectURL(blob);

        const link = document.createElement('a');
        link.href = url;
        link.download = filename;
        // This is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
            // For Firefox it is necessary to delay revoking the ObjectURL
            window.URL.revokeObjectURL(url);
            link.remove();
        }, 100);
    }

    bulkDownloadJson(params, checkField) {
        if (params[checkField]) {
            return this.httpClient.post(`${this.mediaApi}` + '/export', { params: params }).subscribe((response) => {
                if (response['count'] > 0) {
                    this.httpClient.get(`${this.uploadMediaPath + response['item']['fileName']}`, { responseType: 'arraybuffer' })
                        .subscribe(readFile => this.downLoadFile(readFile, 'application/zip', response['item']['fileName']));
                } else {
                    this.notificationService.showError('project.bulk.nodata');
                }
            });
        }
    }

    downloadFromAWS(params) {
        return this.httpClient.post(`${this.mediaApi}` + '/upload-cloud', params);
    }
}
