/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Layout2Component } from '../layout/layout-2/layout-2.component';
import { ProjectsCreateComponent } from './components/projects-create/projects-create.component';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectsMediaLibraryComponent } from './components/projects-media-library/projects-media-library.component';
import { ProjectsUploadComponent } from './components/projects-upload/projects-upload.component';
import { ProjectsImageAnnotationComponent } from './components/projects-image-annotation/projects-image-annotation.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

const routes: Routes = [
    {
        path: '',
        component: Layout2Component,
        children: [
            { path: '', component: ProjectsListComponent, data: { permission: 'PROJECTS_VIEW' } },
            { path: 'create', component: ProjectsCreateComponent, data: { permission: 'PROJECTS_CREATE_OR_UPDATE' } },
            { path: 'edit/:id', component: ProjectsCreateComponent, data: { permission: 'PROJECTS_CREATE_OR_UPDATE' } },
            { path: 'media-library/:id', component: ProjectsMediaLibraryComponent, data: { permission: 'PROJECTS_MEDIA_LIBRARY_VIEW' } },
            { path: 'media-library/upload/:id/:origin', component: ProjectsUploadComponent, data: { permission: 'PROJECTS_MEDIA_UPLOAD' } },
            {
                path: 'image-annotation/:id/:image_id', component: ProjectsImageAnnotationComponent,
                data: { permission: 'PROJECTS_IMAGE_ANNOTATION' }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SlickCarouselModule,
        ButtonsModule.forRoot(),
        FormsModule
    ],
    exports: [
        RouterModule,
        SlickCarouselModule,
        FormsModule,
        ButtonsModule
    ]
})
export class ProjectsRoutingModule {
}
