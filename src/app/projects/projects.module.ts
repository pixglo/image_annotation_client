/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MessageService } from '../shared/message.service';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectsCreateComponent } from './components/projects-create/projects-create.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import { messages } from './projects-messages';
import { ProjectsMediaLibraryComponent } from './components/projects-media-library/projects-media-library.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProjectsUploadComponent } from './components/projects-upload/projects-upload.component';
import { MediaPreviewComponent } from './components/media-preview/media-preview.component';
import { ProjectsImageAnnotationComponent } from './components/projects-image-annotation/projects-image-annotation.component';
import { ImageAnnotationModule } from 'image-annotation';
@NgModule({
    declarations: [
        ProjectsListComponent, ProjectsCreateComponent,
        ProjectsMediaLibraryComponent, ProjectsUploadComponent,
        MediaPreviewComponent, ProjectsImageAnnotationComponent
    ],
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        SharedModule,
        ReactiveFormsModule,
        NgxDatatableModule,
        NgSelectModule,
        ProjectsRoutingModule,
        NgbModule,
        ImageAnnotationModule
    ],
    entryComponents: [MediaPreviewComponent]
})
export class ProjectsModule {
    constructor(
        private messageService: MessageService
    ) {
        this.messageService.register(messages);
    }
}

