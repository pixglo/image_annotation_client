/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { Subject } from 'rxjs';
import { AppService } from '../../../app.service';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { ProjectService } from '../../projects.service';
import { Project } from '../../model/projects';
import { NotificationService } from '../../../shared/notification.service';

@Component({
    selector: 'app-projects-list',
    templateUrl: './projects-list.component.html',
    styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit, OnDestroy {
    // Default Variables
    @ViewChild(DatatableComponent) table: DatatableComponent;
    loading = true;
    content = [];
    page = PageMetadata.empty(10, 'createdAt');
    masterSelected = false;
    flagDownloadDisabled = true;
    selectedProjects = [];
    filterFields = [
        {
            label: 'Name', val: 'name', isFilterable: true, elementType: 'text'
        },
        {
            label: 'Created On', val: 'createdAt', isFilterable: true, elementType: 'dateRange'
        }
    ];

    // Search Variables
    searchQuery: string;
    searchTextChanged = new Subject<string>();
    searchSubscription;

    constructor(private appService: AppService,
        private pageService: ProjectService,
        private router: Router,
        private notificationService: NotificationService,
        private route: ActivatedRoute) {
        this.appService.setHeader('Annotation Project', ['Image Management', 'Annotation Project']);
    }

    async ngOnInit() {

        // Debounce Search
        this.searchSubscription = this.searchTextChanged
            .pipe(debounceTime(500))
            .pipe(distinctUntilChanged())
            .pipe(mergeMap(search => this.search(this.page.page)))
            .subscribe(() => {
            });
        this.search();
    }

    ngOnDestroy() {
        if (this.searchSubscription) {
            this.searchSubscription.unsubscribe();
        }
    }
    // Handler: On sort
    onSort($event){
        // Preserve paging during sorting
        this.table.offset = +this.page.page - 1;
    }

    async search(page = 1, isSearch = false, objFilter = {}) {
        this.loading = true;
        try {
            const pageResponse = await this.pageService.search(this.searchQuery, page, this.page.size, this.page.sort, isSearch, objFilter);
            this.content = pageResponse.content;
            this.page = pageResponse.metadata;
            console.log('Search updated');
        } catch (err) {
            console.log(err);
        } finally {
            this.loading = false;
        }
    }

    async del(project: Project) {
        await this.pageService.del(project);
        this.search();
    }

    edit(project: Project) {
        this.router.navigate(['edit', project._id], { relativeTo: this.route });
    }

    triggerSearch($event) {
        // Called by the search bar
        this.searchTextChanged.next($event.target.value);
    }

    onRecordSelected(action) {
        if (action === 'all') {
            for (let i = 0; i < this.content.length; i++) {
                this.content[i].isSelected = this.masterSelected;
            }
        } else {
            this.masterSelected = this.content.every(function (item: any) {
                return item.isSelected === true;
            });
        }
        let cntSelected = 0;
        // Change bulk download button visibility based on checkbox selection count
        this.content.forEach((item) => {
            if (item.isSelected) {
                cntSelected = cntSelected + 1;
            }
        });
        this.flagDownloadDisabled = (cntSelected === 0);

    }

    uploadMedia(project: Project) {
        this.router.navigate(['media-library', project._id], { relativeTo: this.route });
    }

    uploadDirectMedia(project: Project) {
        this.router.navigate(['projects/media-library/upload', project._id, 'projects']);
    }

    bulkdownload() {
        this.selectedProjects = [];
        this.content.forEach((items) => {
            if (items.isSelected) {
                this.selectedProjects.push(items._id);
            }
        });

        // Call bulk download json api
        if (this.selectedProjects.length > 0) {
            const qryParam = {
                projectId: this.selectedProjects
            };
            this.pageService.bulkDownloadJson(qryParam, 'projectId');
        } else { // No project selected message
            this.notificationService.showError('project.bulk.invalid');
        }
    }

    /*Function to receive response emitted by filter*/
    public receiveFilterResponse(data: any): void {
        this.search(this.page.page, true, data);
    }
}
