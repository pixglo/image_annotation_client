/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../../../app.service';
import { NotificationService } from '../../../shared/notification.service';
import { ValidateEmpty } from '../../../validators/empty.validator';
import { Project } from '../../model/projects';
import { ProjectService } from '../../projects.service';

@Component({
    selector: 'app-projects-create',
    templateUrl: './projects-create.component.html',
    styleUrls: ['./projects-create.component.scss']
})
export class ProjectsCreateComponent implements OnInit {

    pageForm: FormGroup;
    isEditMode = true;
    currentRecord: Project;
    pageEndpoint = 'projects';
    constructor(
        private appService: AppService,
        private pageService: ProjectService,
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService,
        private formBuilder: FormBuilder
    ) {
        this.pageForm = this.buildForm();
    }

    async ngOnInit() {
        // Note: We await here so that we have all the dependant data loaded before subscribing to route
        // which triggers the edit mode
        // Check route to figure out the mode
        this.route.params.subscribe(params => {
            const recordId = params['id'];
            if (recordId) {
                this.activateEditMode(recordId);
            } else {
                this.activateCreateMode();
            }
        });
    }

    private activateCreateMode(): void {
        this.isEditMode = false;
        this.appService.setHeader('Annotation Project', ['Image Management', 'Annotation Project', 'Create Project']);
    }

    private async activateEditMode(id: any) {
        this.isEditMode = true;
        this.appService.setHeader('Annotation Project', ['Image Management', 'Annotation Project', 'Edit Project']);

        // Load Selected Record
        const getRecordData = <Project>await this.pageService.findById(id);
        this.currentRecord = getRecordData['item'];
        this.pageForm.patchValue(this.currentRecord);

    }

    private buildForm(): FormGroup {
        return this.formBuilder.group({
            name: ['', [Validators.required, ValidateEmpty]],
        });
    }

    async save() {
        if (this.pageForm.invalid) {
            return;
        }

        if (this.isEditMode) {
            await this.update();
        } else {
            await this.create();
        }
    }

    private async create() {
        await this.pageService.create(this.pageForm.value);
        this.router.navigate([this.pageEndpoint]);
    }

    private async update() {
        await this.pageService.update(Object.assign({ _id: this.currentRecord['_id'] }, this.pageForm.value));
        this.router.navigate([this.pageEndpoint]);
    }

    async del() {
        await this.pageService.del(this.currentRecord);
        this.router.navigate([this.pageEndpoint]);
    }

    async cancel() {
        if (this.pageForm.dirty) {
            await this.notificationService.showChangesConfirmation();
        }
        this.router.navigate(['/' + this.pageEndpoint]);
    }
}
