/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { Subject } from 'rxjs';
import { AppService } from '../../../app.service';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { ProjectService } from '../../projects.service';
import { Project } from '../../model/projects';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MediaPreviewComponent } from '../media-preview/media-preview.component';

@Component({
    selector: 'app-projects-media-library',
    templateUrl: './projects-media-library.component.html',
    styleUrls: ['./projects-media-library.component.scss']
})
export class ProjectsMediaLibraryComponent implements OnInit, OnDestroy {
    // Default Variables
    @ViewChild(DatatableComponent) table: DatatableComponent;
    loading = true;
    content = [];
    queryParams = PageMetadata.empty(8, 'createdAt');
    masterSelected = false;
    recordId: any = '';
    page = 1;
    // Search Variables
    searchQuery: string;
    searchTextChanged = new Subject<string>();
    searchSubscription;

    constructor(private appService: AppService,
        private pageService: ProjectService,
        private router: Router,
        private route: ActivatedRoute,
        private modalService: NgbModal) {
        this.appService.setHeader('Annotation Project', ['Image Management', 'Annotation Project', 'Image Library']);
    }

    async ngOnInit() {
        // Note: We await here so that we have all the dependant data loaded before subscribing to route
        // which triggers the edit mode
        // Check route to figure out the mode
        this.route.params.subscribe(params => {
            this.recordId = params['id'];
        });
        // Debounce Search
        this.searchSubscription = this.searchTextChanged
            .pipe(debounceTime(500))
            .pipe(distinctUntilChanged())
            .pipe(mergeMap(search => this.search(this.queryParams.page)))
            .subscribe(() => {
            });
        this.search();
    }

    ngOnDestroy() {
        if (this.searchSubscription) {
            this.searchSubscription.unsubscribe();
        }
    }

    async search(page = 1) {
        this.loading = true;
        try {
            const pageResponse = await this.pageService.searchMedia(this.searchQuery, page,
                this.queryParams.size, this.queryParams.sort, this.recordId);
            this.content = pageResponse.content;
            this.queryParams = pageResponse.metadata;
            console.log('Search updated');
        } catch (err) {
            console.log(err);
        } finally {
            this.loading = false;
        }
    }

    async del(project: Project) {
        await this.pageService.delMedia(project);
        this.search();
    }

    edit(project: Project) {
        this.router.navigate(['edit', project._id], { relativeTo: this.route });
    }

    triggerSearch($event) {
        // Called by the search bar
        this.searchTextChanged.next($event.target.value);
    }

    onRecordSelected(action) {
        if (action === 'all') {
            for (let i = 0; i < this.content.length; i++) {
                this.content[i].isSelected = this.masterSelected;
            }
        } else {
            this.masterSelected = this.content.every(function (item: any) {
                return item.isSelected === true;
            });
        }

    }

    uploadMedia() {
        this.router.navigate(['projects/media-library/upload', this.recordId, 'media-library']);
    }

    onPageChange() {
        if (this.page !== this.queryParams.page) { this.search(this.queryParams.page); this.page = this.queryParams.page; }
    }

    previewMediaContent(media) {
        const modalRef = this.modalService.open(MediaPreviewComponent, { size: 'lg', windowClass: 'modal-fill-in modal-lg' });
        modalRef.componentInstance.mediaFileInput = media;
    }

    cancel() {
        this.router.navigate(['projects']);
    }

    imageStatusIsCompleted(imageStatus) {
        return imageStatus === 'COMPLETED';
    }

    imageStatusClass(imageStatus) {
        return (this.pageService.imageProcessStatus[imageStatus] && this.pageService.imageProcessStatus[imageStatus]['class'])
            ? this.pageService.imageProcessStatus[imageStatus]['class'] : 'primary';
    }

    openImageAnnotation(objData) {
        this.router.navigate(['projects/image-annotation', this.recordId, objData._id]);
    }

    async downloadJson(objItem, isbulk = false) {
        const qryParam = {
            mediaId: objItem._id
        };
        this.pageService.downloadJson(qryParam);
    }

    bulkdownload() {
        // Call bulk download json api
        const qryParam = {
            projectId: [this.recordId]
        };
        this.pageService.bulkDownloadJson(qryParam, 'projectId');
    }
}
