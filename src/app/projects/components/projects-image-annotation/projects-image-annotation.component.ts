/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '../../projects.service';
import { NotificationService } from '../../../shared/notification.service';
import { PageMetadata } from '../../../shared/model/paging/page-metadata';
import { SlickCarouselComponent } from 'ngx-slick-carousel';
import { TagService } from '../../../tags/tags.service';



// tslint:disable:max-line-length
@Component({
    selector: 'app-projects-image-annotation',
    templateUrl: './projects-image-annotation.component.html',
    styleUrls: ['./projects-image-annotation.component.scss']
})
export class ProjectsImageAnnotationComponent implements OnInit {
    // Default Variables
    loading = true;
    content = [];
    projectId = '';
    selected_imageId = '';
    selected_image = [];
    current_image = '';
    image_data = [];
    queryParams = PageMetadata.empty(10, 'createdAt');
    page = 1;
    is_package_ready = false;
    disableNext = false;
    disablePrev = false;
    // image slider parameter
    slider_image = { max_width: 96, max_height: 72 };

    // Library variables
    usedShapes = ['rect'];
    currentPage = 1;

    tags = [];

    slideConfig = { 'slidesToShow': 9, 'slidesToScroll': 1, infinite: false, focusOnSelect: true, 'arrows': false };
    @ViewChild('slickModal') slickModal: SlickCarouselComponent;

    constructor(private appService: AppService, private route: ActivatedRoute, private pageService: ProjectService,
        private notificationService: NotificationService, private router: Router, private tagService: TagService) {
        this.appService.setHeader('Annotation Project', ['Image Management', 'Annotation Project', 'Image Annotation']);
    }

    async ngOnInit() {
        // Setup tags dinamically
        this.tags = [];
        const objTags = await this.tagService.getList({});
        objTags.subscribe((allTags) => {
            allTags['list'].forEach(tags => {
                this.tags.push({ 'label': tags.name, 'value': tags._id });
            });
        });

        // Note: We await here so that we have all the dependant data loaded before subscribing to route
        this.route.params.subscribe(params => {
            this.projectId = params['id'];
            this.selected_imageId = params['image_id'];
            this.current_image = this.selected_imageId;
        });
        this.search();
    }

    async search(page = 1) {
        this.loading = true;
        try {
            const pageResponse = await this.pageService.searchAnnotatedMedia('', page,
                this.queryParams.size, this.queryParams.sort, this.projectId, this.selected_imageId);
            this.content = pageResponse.content;
            this.queryParams = pageResponse.metadata;
            this.generatePackageData();
            this.setupNextPrevButton();
            this.loading = false;
        } catch (err) {
            this.loading = false;
            console.log(err);
        } finally {
            this.loading = false;
        }
    }

    /*Function to receive response emitted by filter*/
    public receiveAnnotationResponse(data: any): void {
        const queryParam = {
            mediaId: this.current_image,
            regions: this.selected_image[0].regions,
            status: 'COMPLETED'
        };
        this.loading = true;
        const respUpdate = this.pageService.updateMediaAnnotation(queryParam);
        respUpdate.subscribe((response) => {
            this.loading = false;
            this.notificationService.showSuccess('annotation.save.success');
        }, (err) => {
            this.loading = false;
            this.notificationService.showGenericError();
        });

    }

    /*Funtion to generate image object for package*/
    public generatePackageData() {
        if (+this.queryParams.page === 1) {
            this.image_data = [];
        }

        let imgIndex = 0;
        this.content.forEach((items) => {
            const mediaPath = (items.img) ? this.pageService.uploadMediaPath + items.img['large'] : '';
            if (mediaPath !== '') {

                const current_image = document.createElement('img');
                const image_diamention = { height: 0, width: 0 };


                // Chrome
                if (document.addEventListener) {
                    current_image.addEventListener('load', (iLoad) => {
                        const targetHeight  = iLoad['path'] ? iLoad['path'][0].naturalHeight : iLoad.target['height'];
                        const targetWidth  = iLoad['path'] ? iLoad['path'][0].naturalWidth : iLoad.target['width'];
                        this.getImageRatio({ naturalHeight: targetHeight, naturalWidth: targetWidth }, image_diamention);
                        this.image_data.push({
                            path: mediaPath,
                            height: image_diamention.height,
                            width: image_diamention.width,
                            dimention: { height: items.fileHeight, width: items.fileWidth },
                            fileSizeBytes: items.fileSizeBytes,
                            filename: items.img['default'],
                            regions: (items.regions) ? items.regions : [],
                            selected_image_index: (this.selected_imageId === items._id),
                            name: items.name,
                            projectId: items.projectId, mediaId: items.mediaId, tenantId: items.tenantId,
                            annotated: (items.annotated) ? items.annotated : [],
                            tags: (items.tags) ? items.tags : [], tags_text: (items.tags_text) ? items.tags_text : [], _id: items._id,
                            mediaType: items.mediaType || 'image',
                            createdAt: items.createdAt
                        });
                        // if (imgIndex === 0) { selected_imageId
                        if (this.current_image === items._id) {
                            this.selected_image = [{
                                path: mediaPath,
                                height: image_diamention.height,
                                width: image_diamention.width,
                                dimention: { height: items.fileHeight, width: items.fileWidth },
                                fileSizeBytes: items.fileSizeBytes,
                                filename: items.img['default'],
                                regions: (items.regions) ? items.regions : [],
                                selected_image_index: (this.selected_imageId === items._id),
                                name: items.name,
                                projectId: items.projectId, mediaId: items.mediaId, tenantId: items.tenantId,
                                annotated: (items.annotated) ? items.annotated : [],
                                tags: (items.tags) ? items.tags : [], tags_text: (items.tags_text) ? items.tags_text : [], _id: items._id,
                                mediaType: items.mediaType || 'image',
                                createdAt: items.createdAt
                            }];
                        }
                        imgIndex = imgIndex + 1;
                    });
                //Firefox
                } else {
                    current_image.onload = () => {

                        console.log('current_image', current_image);
                        
                        this.getImageRatio({ naturalHeight: current_image.height, naturalWidth: current_image.width }, image_diamention);
                        this.image_data.push({
                            path: mediaPath,
                            height: image_diamention.height,
                            width: image_diamention.width,
                            dimention: { height: items.fileHeight, width: items.fileWidth },
                            fileSizeBytes: items.fileSizeBytes,
                            filename: items.img['default'],
                            regions: (items.regions) ? items.regions : [],
                            selected_image_index: (this.selected_imageId === items._id),
                            name: items.name,
                            projectId: items.projectId, mediaId: items.mediaId, tenantId: items.tenantId,
                            annotated: (items.annotated) ? items.annotated : [],
                            tags: (items.tags) ? items.tags : [], tags_text: (items.tags_text) ? items.tags_text : [], _id: items._id,
                            mediaType: items.mediaType || 'image',
                            createdAt: items.createdAt
                        });
                        if (this.current_image === items._id) {
                            this.selected_image = [{
                                path: mediaPath,
                                height: image_diamention.height,
                                width: image_diamention.width,
                                dimention: { height: items.fileHeight, width: items.fileWidth },
                                fileSizeBytes: items.fileSizeBytes,
                                filename: items.img['default'],
                                regions: (items.regions) ? items.regions : [],
                                selected_image_index: (this.selected_imageId === items._id),
                                name: items.name,
                                projectId: items.projectId, mediaId: items.mediaId, tenantId: items.tenantId,
                                annotated: (items.annotated) ? items.annotated : [],
                                tags: (items.tags) ? items.tags : [], tags_text: (items.tags_text) ? items.tags_text : [], _id: items._id,
                                mediaType: items.mediaType || 'image',
                                createdAt: items.createdAt
                            }];
                        }
                        imgIndex = imgIndex + 1;

                    };
                  }
                current_image.src = mediaPath;
            }
        });
    }


    // Generate image dimention as per the box heigh and width
    async getImageRatio(objImage, image_diamention) {
        let current_image_width = objImage.naturalWidth;
        let current_image_height = objImage.naturalHeight;
        if (current_image_width > this.slider_image.max_width) {
            // resize image to match the panel width
            const scale_image_width = this.slider_image.max_width / objImage.naturalWidth;
            current_image_width = this.slider_image.max_width;
            current_image_height = objImage.naturalHeight * scale_image_width;
        }
        if (current_image_height > this.slider_image.max_height) {
            // resize further image if its height is larger than the image panel
            const scale_image_height = this.slider_image.max_height / current_image_height;
            current_image_height = this.slider_image.max_height;
            current_image_width = current_image_width * scale_image_height;
        }
        current_image_height = Math.round(current_image_height);
        current_image_width = Math.round(current_image_width);

        // Update image height and width respect to box and image dimention
        image_diamention.height = current_image_height;
        image_diamention.width = current_image_width;
    }

    // Handle slider click event and update package data
    jump_to_image(img_item) {
        this.loading = true;
        this.selected_image = [];
        this.image_data.filter((img_data) => {
            img_data.selected_image_index = false;
        });
        img_item.selected_image_index = true;
        this.current_image = img_item._id;
        setTimeout(() => {
            this.selected_image = [img_item];
        }, 0);
    }

    cancel() {
        this.router.navigate(['projects/media-library', this.projectId]);
    }

    /*Function to receive response emitted by filter*/
    public receivePackageReady(flagPackageReady): void {
        // console.log('flagPackageReady:: ', flagPackageReady);
        this.is_package_ready = flagPackageReady;
        this.loading = false;
    }

    slideNext() {
        if (this.image_data.length < this.queryParams.total) {
            this.search(+this.queryParams.page + 1);
        }
        this.slickModal.slickNext();
    }

    slidePrev() {
        this.slickModal.slickPrev();
    }

    setupNextPrevButton() {
        if (this.queryParams.totalPages <= 1) {
            this.disableNext = true;
            this.disablePrev = true;
        } else {
            this.disableNext = false;
            this.disablePrev = false;
            if (this.queryParams.end === this.queryParams.total) { this.disableNext = true; }
        }
    }

    receiveBackButtonEvent(flagBackHit) {
        if (flagBackHit) {
            this.cancel();
        }

    }

    // afterChange(e) {
    //     const elements = e.event.target.lastChild.firstChild.getElementsByClassName('slick-cloned');
    //     for (const element of elements) {
    //         element.addEventListener('click', this.jump_to_image.bind(this));
    //     }
    // }
}
