/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../../app.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Media } from '../../model/media';
import { ProjectService } from '../../projects.service';

@Component({
    selector: 'app-media-preview',
    templateUrl: './media-preview.component.html',
    styleUrls: [
        './media-preview.component.scss'
    ]
})
export class MediaPreviewComponent implements OnInit {
    @Input() mediaFileInput: Media;
    mediaFile: Media;
    contentSource: string;
    isPreviewSupported = true;

    constructor(
        private appService: AppService,
        public activeModal: NgbActiveModal,
        private pageService: ProjectService
    ) {
        this.appService.setHeader('Annotation Project', ['Image Management', 'Annotation Project', 'Image Library']);
    }

    ngOnInit() {
        this.isPreviewSupported = (['image/jpg', 'image/jpeg'].indexOf(this.mediaFileInput.mediaType.toLowerCase()) > -1);
        this.mediaFile = this.mediaFileInput;
        console.log('mediaFile::', this.mediaFile);
    }
}
