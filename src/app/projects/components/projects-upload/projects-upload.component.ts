/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../../shared/notification.service';
import { Media } from '../../model/media';
import { ProjectService } from '../../projects.service';
import { HttpHeaders, HttpClient, HttpResponse, HttpEventType, HttpRequest } from '@angular/common/http';


@Component({
    selector: 'app-projects-upload',
    templateUrl: './projects-upload.component.html',
    styleUrls: [
        './projects-upload.component.scss'
    ]
})
export class ProjectsUploadComponent implements OnInit {
    readonly baseUrl = '/api/media-files';
    readonly mediaUrl = '/api/uploads/';
    readonly uploadsUrl = '/common/upload-image';
    @ViewChild('inputFile') inputFile: any;
    isFileAdded = false;
    progress = 0;
    isValidating = false; // Fake validation while awaiting the S3 upload completion
    mediaFile = new Media();
    isEditMode = false; savingData = false;
    recordId;
    formData = { projectId: '', images: [], isimageupload: 'image' }; percentDone; isUploading = false;
    backUrl = 'library';
    fileDisabled = false;
    constructor(
        private appService: AppService,
        private router: Router,
        private notificationService: NotificationService,
        private pageService: ProjectService,
        private route: ActivatedRoute,
        private http: HttpClient
    ) {
        this.appService.setHeader('Annotation Project', ['Image Management', 'Annotation Project', 'Upload Image']);
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            console.log('params:: ', params);
            if (params.id !== undefined) {
                this.recordId = params.id;
                this.formData.projectId = this.recordId;
            }

            if (params.origin !== undefined && params.origin === 'projects') {
                this.backUrl = 'projects';
            }
        });
    }

    cancel() {
        if (this.backUrl === 'library') {
            this.router.navigate(['projects/media-library', this.recordId]);
        } else {
            this.router.navigate(['projects']);
        }
    }

    onPhotoChange(event) {
        if (event.target.files && event.target.files.length) {
            const fd = new FormData();
            const selectedFiles = event.target.files;
            const availableFiles = [];
            for (let fl = 0; fl < selectedFiles.length; fl++) {
                if (!(this.validateFile(selectedFiles[fl].name, this.formData.isimageupload))) {
                    const message = 'project.file.invalid';
                    const validExtension = this.pageService.allowImageMediaType.join(', ');
                    /* if (this.formData.isimageupload === 'zip') { // for zip file upload process
                         validExtension = 'zip';
                     }*/
                    const messageValues = { extension: `${validExtension}` };
                    this.notificationService.showError(message, messageValues);
                    this.inputFile.nativeElement.value = '';
                    return false;
                }
                availableFiles.push(selectedFiles[fl]);
                fd.append('file', event.target.files[fl]);
            }

            this.isUploading = true;
            this.fileDisabled = true;
            const uploadPath = 'upload-multiple'; // For image upload process
            /*if (this.formData.isimageupload === 'zip') { // for zip file upload process
                uploadPath = 'upload-zip';
            }*/
            this.uploadPhoto(fd, uploadPath).subscribe((data: any) => {
                // Via this API, you get access to the raw event stream.
                // Look for upload progress events.
                if (data.type === HttpEventType.UploadProgress) {
                    // This is an upload progress event. Compute and show the % done:
                    this.percentDone = Math.round(100 * data.loaded / data.total);
                } else if (data instanceof HttpResponse) {
                    this.isUploading = false;
                    this.inputFile.nativeElement.value = '';
                    this.notificationService.showSuccess('project.file.success');
                    data.body['images'].forEach(files => {
                        this.formData['images'].push({
                            name: files.name,
                            mediaType: files.mediaType,
                            fileSizeBytes: files.fileSizeBytes,
                            status: this.pageService.defaultImageStatus['name'],
                            metadata: [],
                            img: files.img,
                            fileHeight: files.fileHeight,
                            fileWidth: files.fileWidth
                        });
                    });
                    this.fileDisabled = false;
                }
            }, (err) => {
                this.fileDisabled = false;
                this.isUploading = false;
                this.inputFile.nativeElement.value = '';
            });
        }
    }

    removeLogo() {
        this.formData['images'] = undefined;
    }

    // it is used for upload photo valid format or not
    validateFile(name: String, type: String): boolean {
        const ext = name.substring(name.lastIndexOf('.') + 1);
        switch (type) {
            case 'image':
                if (this.pageService.allowImageMediaType.indexOf(ext.toLowerCase()) > -1) {
                    return true;
                }
                break;
            case 'zip':
                if (['zip'].indexOf(ext.toLowerCase()) > -1) {
                    return true;
                }
                break;
            default:
                return false;

        }
        return false;
    }

    uploadPhoto(params: any, url) {
        const headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        return this.http.request(new HttpRequest('POST',
            this.baseUrl + '/' + url, params, {
            headers: headers,
            reportProgress: true
        }));
    }

    /**
     * item: Object of logo
     * type: type of logo `small/medium/large/default`
     */
    getPhoto(item: object, type: string = 'default', size: string = '150x100'): string {
        if (item && item[type]) {
            return (this.mediaUrl + item[type]);
        }
        return ``;
    }

    submitData() {

        this.savingData = true;
        this.pageService.createMediaFileProject(this.formData).subscribe(data => {
            this.savingData = false;
            // this.toastr.success(successMsg, 'Success!');
            // this.router.navigate([this.backURL], { relativeTo: this.route});
            this.notificationService.showSuccess('project.file.save');
            this.router.navigate(['projects/media-library', this.recordId]);
        });
    }

    // Download from s3 bucket api call
    uploadPhotoFromAws() {
        if (!this.fileDisabled) {
            this.isUploading = true;
            this.fileDisabled = true;
            this.pageService.downloadFromAWS({ projectId: this.recordId }).subscribe((data: any) => {

                this.isUploading = false;

                this.notificationService.showSuccess('project.file.success');
                data['images'].forEach(files => {
                    this.formData['images'].push({
                        name: files.name,
                        mediaType: files.mediaType,
                        fileSizeBytes: files.fileSizeBytes,
                        status: this.pageService.defaultImageStatus['name'],
                        metadata: [],
                        img: files.img,
                        fileHeight: files.fileHeight,
                        fileWidth: files.fileWidth
                    });
                });
                this.fileDisabled = false;
            });
        }
    }
}
