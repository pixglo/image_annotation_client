import { Media } from '../../model/media';
const { Plugin } = require('@uppy/core');
const mediaApi = '/v1/media';

export class CustomPreProcessor extends Plugin {
    id: string;
    type: string;

    constructor(uppy, opts) {
        super(uppy, opts);
        this.id = 'CutomPlugin';
        this.type = 'modifier';
        this.prepareUpload = this.prepareUpload.bind(this);
    }

    async prepareUpload() {
        const pageService = this.opts.pageService;
        const file = this.uppy.getFiles()[0];

        let mediaFile = new Media();
        mediaFile.fileName = file.name;
        mediaFile.mediaType = file.type;
        mediaFile.fileSizeBytes = file.size;

        // Create media file
        mediaFile = await pageService.saveMediaFile(mediaFile);

        // Emit newly created media file
        pageService.setMediaFile(mediaFile);

        // Set file upload url
        this.uppy.plugins.uploader[0].opts.endpoint = `${mediaApi}/${mediaFile.id}/operations/upload`;
    }

    install() {
        this.uppy.addPreProcessor(this.prepareUpload);
    }

    uninstall() {
        this.uppy.removePreProcessor(this.prepareUpload);
    }
}
