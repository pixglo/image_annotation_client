/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {BrowserModule, Title} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AppService} from './app.service';
import {LayoutModule} from './layout/layout.module';
import {HomeComponent} from './home/home.component';
import {AuthModule} from './auth/auth.module';
import {SharedModule} from './shared/shared.module';
import {AuthenticationHttpInterceptor} from './auth/authentication-http-interceptor';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {GlobalErrorHandler} from './shared/errors/global-error-handler';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

// *******************************************************************************
// NgBootstrap

// *******************************************************************************
// App

// *******************************************************************************
// Interceptors

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthenticationHttpInterceptor, multi: true },
];

export const errorHandlerProvider = { provide: ErrorHandler, useClass: GlobalErrorHandler};

// *******************************************************************************
//

@NgModule({
  declarations: [
    AppComponent,

    // Pages
    HomeComponent
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    SweetAlert2Module.forRoot(),
    LayoutModule,
    AuthModule,
    SharedModule,
    AppRoutingModule,
    BsDatepickerModule.forRoot() // This should be last to ensure feature module routes get priority
  ],

  providers: [
    Title,
    AppService,
    httpInterceptorProviders,
    errorHandlerProvider
  ],

  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
