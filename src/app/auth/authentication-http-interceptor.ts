/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { EMPTY, Observable, Subject, throwError } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationHttpInterceptor implements HttpInterceptor {

    private isRefreshInProgress = false;
    private tokenRefreshedSource = new Subject();
    private tokenRefreshEvent$ = this.tokenRefreshedSource.asObservable();

    constructor(
        private router: Router,
        private authService: AuthenticationService,
    ) {
    }
    //
    // intercept(req: HttpRequest<any>, next: HttpHandler) {
    //   return next.handle(req).pipe(catchError(async(err) => {
    //     if (err.status === 401) {
    //       if (this.isRefreshAttempt(req)) {
    //         console.log('Refresh attempt also failed. Auto-signing out user');
    //         this.autoSignOut();
    //         return EMPTY;
    //       } else {
    //         return this.refreshAccessToken().pipe(
    //           switchMap(() => {
    //             console.log('Access token refreshed. Retrying...');
    //             next.handle(req);
    //           }),
    //           catchError(() => {
    //             // Retry after 401 failed.
    //             this.autoSignOut();
    //             return EMPTY;
    //         }));
    //       }
    //     }
    //     throw err;
    //   }));
    // }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {

        // Handle response
        return next.handle(request).pipe(catchError(error => {

            // TAG::CLIENT_MERGE Remove below code before client code merge
            return next.handle(request);

            if (error.status === 401) {
                if (this.isRefreshAttempt(request)) {
                    console.log('Refresh attempt also failed. Auto-signing out user');
                    this.autoSignOut();
                    return EMPTY;
                } else {
                    return this.refreshAccessToken().pipe(
                        switchMap(() => {
                            return next.handle(request);
                        }),
                        catchError(() => {
                            this.autoSignOut();
                            return EMPTY;
                        }));
                }
            }

            return throwError(error);
        }));
    }
    /**
     * Returns true if this was an attempt to use the refresh token.
     * @param req
     */
    private isRefreshAttempt(req: HttpRequest<any>): boolean {
        return req.url === '/oauth/token';
    }

    private autoSignOut() {
        // API returned 401, sign out
        this.authService.signOut().then(() => {
            location.reload(true);
        });
    }

    private refreshAccessToken() {
        if (this.isRefreshInProgress) {
            // A refresh is happening - Wait for it to complete
            console.log('Refresh is in progress. Waiting for it to complete');
            return new Observable(obs => {
                this.tokenRefreshEvent$.subscribe(() => {
                    obs.next();
                    obs.complete();
                });
            });
        } else {
            console.log('Requesting access token refresh from authService');
            this.isRefreshInProgress = true;
            return this.authService.refreshAccessToken()
                .pipe(tap(() => {
                    this.isRefreshInProgress = false;
                    this.tokenRefreshedSource.next();
                }));
        }
    }
}
