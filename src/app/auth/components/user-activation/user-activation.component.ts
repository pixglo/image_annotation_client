/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../../app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExtValidators } from '../../../shared/validators/ext-validators';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from '../../../shared/message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from '../../token.service';
import { Token } from '../../model/token';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-activation',
  templateUrl: './user-activation.component.html',
  styleUrls: ['./user-activation.component.scss']
})
export class UserActivationComponent implements OnInit, OnDestroy {
  processing = true;
  form: FormGroup;

  @ViewChild('passwordPopover')
  passwordPopover: NgbPopover;

  @ViewChild('confirmPasswordPopover')
  confirmPasswordPopover: NgbPopover;

  token: Token;
  error: string;
  supportEmail = 'support@pixglo.com';

  appConfigSubscription: Subscription;
  screenHeight = 0;
  licenseContainerHeight = 0;
  isAgreed = false;
  continue = false;
  isPasswordReset = false;

  constructor(
    private appService: AppService,
    private tokenService: TokenService,
    private messageService: MessageService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    if (this.route.routeConfig.path.includes('password-reset')) {
      this.isPasswordReset = true;
    }

    this.appService.pageTitle = this.isPasswordReset ? 'Reset Password' : 'Activate Account';
    this.form = this.buildForm();
    this.appConfigSubscription = this.appService.appConfig$.subscribe((config) => {
      this.supportEmail = config.supportEmail;
    });
    this.onResize();
  }

  async ngOnInit() {
    // Check route to figure out the mode
    this.route.params.subscribe(params => {
      this.fetchToken(params['id']);
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigSubscription) {
      this.appConfigSubscription.unsubscribe();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.licenseContainerHeight = this.screenHeight * 0.6;
  }

  async handleCredentialSubmit() {
    try {
      this.processing = true;

      if (this.isPasswordReset) {
        await this.tokenService.resetCredentials(this.token, this.form.value);
      } else {
        await this.tokenService.activate(this.token, this.form.value);
      }

      this.router.navigate(['/sign-in']);
    } catch (e) {
      this.handleError(e);
    } finally {
      this.processing = false;
    }
  }

  private buildForm() {
    return this.formBuilder.group({
      password: ['',
        Validators.compose([
          Validators.required,
          // check whether the entered password has a number
          ExtValidators.patternValidator(/\d/, {
            hasNumber: true
          }),
          // check whether the entered password has upper case letter
          ExtValidators.patternValidator(/[A-Z]/, {
            hasUpperCase: true
          }),
          // check whether the entered password has a lower case letter
          ExtValidators.patternValidator(/[a-z]/, {
            hasLowerCase: true
          }),
          // check whether the entered password has a special character
          ExtValidators.patternValidator(
            /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
            {
              hasSpecialCharacters: true
            }
          ),
          Validators.minLength(8)
        ])
      ],
      confirmPassword: ['', Validators.compose([Validators.required])]
    },
      {
        // check whether our password and confirm password match
        validator: ExtValidators.passwordMatchValidator
      });
  }

  onPasswordFocus() {
    if (this.passwordPopover) {
      this.passwordPopover.open();
    }
  }

  onPasswordBlur() {
    if (this.passwordPopover) {
      this.passwordPopover.close();
    }
  }

  onConfirmPasswordFocus() {
    const confirmErrors = this.form.controls['confirmPassword'].errors;
    if (this.confirmPasswordPopover && confirmErrors && confirmErrors['NoPasswordMatch']) {
      this.confirmPasswordPopover.open();
    }
  }

  onConfirmPasswordBlur() {
    if (this.confirmPasswordPopover) {
      this.confirmPasswordPopover.close();
    }
  }

  private async fetchToken(token: string) {
    try {
      if (this.isPasswordReset) {
        this.isAgreed = this.continue = true;
        this.token = await this.tokenService.findByPasswordResetTokenValue(token);
      } else {
        this.token = await this.tokenService.findByTokenValue(token);
      }

      console.log('Fetched token : {}', token);
      this.processing = false;
    } catch (e) {
      this.handleError(e);
    }
  }

  private handleError(e: any) {
    if (e.error) {
      // API Error Response - Resolve message code and show it.
      this.error = this.messageService.getMessage(e.error.code);
    } else {
      // Rethrow
      throw e;
    }
  }

  agreeToTOS(e) {
    this.isAgreed = e;
  }

  continueToSignUp() {
    this.continue = true;
  }
}
