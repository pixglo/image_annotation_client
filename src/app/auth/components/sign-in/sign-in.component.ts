/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppService } from '../../../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../authentication.service';
import { Subscription } from 'rxjs';
import { AppConfig } from '../../../shared/model/config/app-config';


/**
 * Sign In Message Types.
 */
enum MessageType {
    ERROR,
    WARNING
}

/**
 * Component that handles signing in a user.
 */
@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: [
        './sign-in.component.scss'
    ]
})
export class SignInComponent implements OnInit, OnDestroy {

    readonly messageTypeEnum = MessageType;  // Used by View to access enum values
    isSigningIn = false;
    email: string;
    password: string;
    message: string;
    messageType: MessageType;
    config = new AppConfig();
    appConfigSubscription: Subscription;

    constructor(
        private appService: AppService,
        private authService: AuthenticationService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.appService.pageTitle = 'Sign-In';
        this.appConfigSubscription = this.appService.appConfig$.subscribe((config) => {
            this.config = config;
        });
    }

    ngOnInit(): void { }

    ngOnDestroy(): void {
        if (this.appConfigSubscription) {
            this.appConfigSubscription.unsubscribe();
        }
    }

    async signIn() {
        console.log('Signin in...');
        this.isSigningIn = true;

        try {
            this.clearMessages();
            await this.authService.signIn(this.email, this.password);
            console.log('Sign in success');

            // this.router.navigate(this.getReturnUrl());
            // TAG::CLIENT_MERGE Remove it before client code merge and remove comment from above code
            location.replace('/home');
        } catch (e) {
            this.handleError(e);
        } finally {
            this.isSigningIn = false;
        }
    }

    private getReturnUrl() {
        const target = this.route.snapshot.queryParams['returnUrl'];
        if (target) {
            console.log('Routing to target return URL', target);
            return [target];
        } else {
            // Default target is home
            console.log('Routing to default target URL (/home)');
            return ['/home'];
        }
    }

    private handleError(e: any) {
        console.log('Handling sign-in error', e);

        switch (e.status) {
            case 404:
            case 503:
            case 504:
                this.showWarning('Server temporarily unavailable. Please try again later.');
                break;
            case 400:
                if (e.error && e.error.error === 'invalid_grant') {
                    this.showError(e.error.error_description); // Shows backend error message
                    break;
                }
            /* falls through (for non invalid-grant 400 errors) */
            default:
                this.showWarning('Sorry, something went wrong. Please try again later.');
                console.error('Error during sign-in', e);
        }
    }

    private showError(message: string) {
        this.message = message;
        this.messageType = MessageType.ERROR;
    }

    private showWarning(message: string) {
        this.message = message;
        this.messageType = MessageType.WARNING;
    }

    private clearMessages() {
        this.message = '';
    }
}
