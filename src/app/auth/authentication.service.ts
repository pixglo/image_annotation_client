/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthenticatedUser } from './model/authenticated-user';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private readonly localStorageAuthenticatedKey = 'app-auth';
    private readonly localStorageUserKey = 'app-auth-user';

    private readonly tokenApi = '/oauth/token';
    private readonly tokenRevocationApi = '/oauth/web/signout';

    private readonly usersApi = '/v1/users';

    private user: AuthenticatedUser;

    constructor(private httpClient: HttpClient) {
    }

    /**
     * Authenticates a user and populates the local storage with authentication context.
     *
     * @param email
     * @param password
     */
    async signIn(email: string, password: string) {
        /*const params = new HttpParams()
          .set('username', encodeURI(email))
          .set('password', password)
          .set('scope', 'USER')
          .set('grant_type', 'password')
          .toString()
          .replace(/\+/g, '%2B');
    
        const response = await this.httpClient.post<AuthenticatedUser>(this.tokenApi, params, {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        }).toPromise();
    
        const user = AuthenticatedUser.from(response);
    
        // Obtain effective permissions of the logged in user.
        const permissionsResponse = await this.httpClient.get<any>(`${this.usersApi}/${user.userId}/permissions`).toPromise();
        user.permissions = permissionsResponse.permissions;*/

        // TAG::CLIENT_MERGE Remove it before client code merge and remove comment from above code
        // tslint:disable-next-line:max-line-length
        const user = { 'userId': '5ccaa760eac1b6f90452e264', 'email': 'pixglouser+jd@gmail.com', 'firstName': 'John', 'lastName': 'Doe', 'tenantId': '1e8d86280000000000000000', 'permissions': ['USERS_CREATE_OR_UPDATE', 'USERS_DELETE', 'USERS_VIEW', 'TENANT_PROFILE', 'TEMPLATE_CREATE_OR_UPDATE', 'TEMPLATE_DELETE', 'TEMPLATE_VIEW', 'MEDIA_CREATE_OR_UPDATE', 'MEDIA_DELETE', 'MEDIA_VIEW'], 'token_type': 'bearer', 'expires_in': 863999, 'scope': 'USER', 'jti': '4500a973-f438-49c4-a517-95814ec716ba' }

        this.setAuthenticationContext(user);

        console.log('Authenticated: ' + JSON.stringify(user));
    }

    refreshAccessToken(): Observable<any> {
        console.log('Refreshing access token');

        // Note: We do not have to send the actual refresh token and grant_type as params here since its automatically
        // added by the filters in the server based on the cookies

        return this.httpClient.post<AuthenticatedUser>(this.tokenApi, {}, {
            headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    /**
     * Signs out the currently authenticated user.
     */
    async signOut() {
        // Remove authentication context
        this.clearAuthenticationContext();
        await this.httpClient.post(this.tokenRevocationApi, {}).toPromise();
    }

    /**
     * Returns the currently authenticated user. Returns null if not authenticated.
     */
    getUser(): AuthenticatedUser {
        // TAG::CLIENT_MERGE Remove it before client code merge and remove comment from above code
        if (!this.user) {
            this.user = AuthenticatedUser.from(JSON.parse(localStorage.getItem(this.localStorageUserKey)));
        }
        return this.user;
        if (this.isAuthenticated()) {
            if (!this.user) {
                this.user = AuthenticatedUser.from(JSON.parse(localStorage.getItem(this.localStorageUserKey)));
            }
        } else {
            this.user = null;
        }
        return this.user;
    }

    /**
     * Returns true if the current user is authenticated.
     */
    isAuthenticated(): boolean {
        return localStorage.getItem(this.localStorageAuthenticatedKey) === 'true';
    }

    /**
     * Sets the authentication context.
     * @param user user object
     */
    private setAuthenticationContext(user) {
        localStorage.setItem(this.localStorageAuthenticatedKey, 'true');
        localStorage.setItem(this.localStorageUserKey, JSON.stringify(user));
        this.user = user;
    }

    /**
     * Clears authentication context.
     */
    private clearAuthenticationContext() {
        console.log('Clearing authentication context');
        localStorage.setItem(this.localStorageAuthenticatedKey, 'false');
        localStorage.removeItem(this.localStorageUserKey);
        this.user = null;
    }

}
