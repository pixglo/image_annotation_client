/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../shared/notification.service';
import { Token } from './model/token';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private apiBase = '/v1/public/tokens';

  constructor(private httpClient: HttpClient,
    private notificationService: NotificationService) {
  }

  async findByTokenValue(tokenValue: string): Promise<Token> {
    if (!tokenValue) {
      throw new Error('\'tokenValue\' is required for findByTokenValue');
    }
    return <Token>await this.httpClient.get(`${this.apiBase}/${tokenValue}`).toPromise();
  }

  async findByPasswordResetTokenValue(tokenValue: string): Promise<Token> {
    if (!tokenValue) {
      throw new Error('\'tokenValue\' is required for findByTokenValue');
    }
    return <Token>await this.httpClient.get(`${this.apiBase}/${tokenValue}/password-reset`).toPromise();
  }

  async activate(token: Token, value: any) {
    await this.httpClient.put(`${this.apiBase}/${token.token}/credentials`, value).toPromise();
    await this.notificationService.showSuccess('user.token.account.activated');
  }

  async resetCredentials(token: Token, value: any) {
    await this.httpClient.put(`${this.apiBase}/${token.token}/credentials/true`, value).toPromise();
    await this.notificationService.showSuccess('user.token.credentials.reset');
  }
}
