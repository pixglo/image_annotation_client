/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
  'user.token.not.found' : 'Sorry, this activation link is not valid.',
  'user.token.expired' : '<p>Sorry, this activation link has expired</p>' +
    '<p class="text-muted">Please contact your system administrator to get a new link.</p>',
  'user.token.already.used' : '<p>You have used  this link already.</p>' +
    '<p class="text-muted">Please <a href="/sign-in">sign in</a> using your credentials.</p>',
  'user.token.account.activated': 'Your account is now active. Redirecting you to sign-in...',
  'user.token.credentials.reset': 'Your credentials are now reset. Redirecting you to sign-in...',
  'user.token.account.activated.title': 'Account Activated',
};
