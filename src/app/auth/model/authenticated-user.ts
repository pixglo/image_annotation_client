/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export class AuthenticatedUser {

  static from(obj: any): AuthenticatedUser {
    const user = new AuthenticatedUser();
    Object.assign(user, obj);
    return user;
  }

  constructor(
    public userId?: string,
    public email?: string,
    public firstName?: string,
    public lastName?: string,
    public tenantId?: string,
    public permissions?: string[]
  ) {
  }

  /**
   * Combine first name and last name to build the full name.
   * Either of the name parts can be null.
   *
   * @returns {string}
   */
  getFullName(): string {
    const nameTokens = [];
    if (this.firstName && this.firstName.length > 0) {
      nameTokens.push(this.firstName);
    }

    if (this.lastName && this.lastName.length > 0) {
      nameTokens.push(this.lastName);
    }
    return nameTokens.join(' ');
  }

  /**
   * Returns true if this user has the given permission.
   * @param permission
   */
  hasPermission(permission: string) {
    return this.permissions.indexOf(permission.trim()) !== -1;
  }

}
