/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Page} from '../shared/model/paging/page';
import {Role} from './model/role';
import {NotificationService} from '../shared/notification.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private readonly apiBase = '/v1/roles';

  constructor(
    private httpClient: HttpClient,
    private notificationService: NotificationService
  ) { }


  async search(query?: string, page?: number, size?: number, sort?: string): Promise<Page<Role>> {

    let params = new HttpParams();
    if (query) { params = params.set('query', query); }
    if (page) { params = params.set('page', page.toString()); }
    if (size) { params = params.set('size', size.toString()); }
    if (sort) { params = params.set('sort', sort); }

    console.log('Searching for roles with params', params.toString());
    const response = await this.httpClient.get(this.apiBase, { params: params}).toPromise();
    return Page.build(response, Role.from);
  }

  async create(role: Role) {
    await this.httpClient.post(this.apiBase, role).toPromise();
    this.notificationService.showSuccess('role.create.success');
    console.log('Created role', role);
  }

  async update(role: Role) {
    await this.httpClient.put(`${this.apiBase}/${role.id}`, role).toPromise();
    this.notificationService.showSuccess('role.update.success');
    console.log('Updated role', role);
  }

  async del(role: Role) {
    const message = 'role.delete.confirm';
    const messageValues = {name : role.name};
    await this.notificationService.showDeleteConfirmation(message, messageValues).then(async () => {
      await this.httpClient.delete(`${this.apiBase}/${role.id}`).toPromise();
      this.notificationService.showSuccess('role.delete.success', {name : role.name});
      console.log('Deleted role', role);
    });
  }

  async findById(roleId: string) {
    if (! roleId) {
      throw new Error('Role ID is required for findById');
    }
    return this.httpClient.get(`${this.apiBase}/${roleId}`).toPromise();
  }

  async findRoles() {
    return <Array<any>> await this.httpClient.get(`${this.apiBase}/list`, {}).toPromise();
  }

}
