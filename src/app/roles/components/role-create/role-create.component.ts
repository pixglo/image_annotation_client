/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Component, OnInit} from '@angular/core';
import {AppService} from '../../../app.service';
import {RoleService} from '../../role.service';
import {PermissionService} from '../../permission.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../shared/notification.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Role} from '../../model/role';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.scss']
})
export class RoleCreateComponent implements OnInit {

  permissions = [];
  form: FormGroup;
  isEditMode = true;
  isSystemRole = false;
  currentRole: Role; // In Edit Mode

  constructor(
    private appService: AppService,
    private roleService: RoleService,
    private permissionService: PermissionService,
    private notificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.form = this.buildForm();
  }

  async ngOnInit() {
    // Note: We await here so that we have all the dependant data loaded before subscribing to route
    // which triggers the edit mode
    this.permissions = await this.permissionService.getPermissions();

    // Check route to figure out the mode
    this.route.params.subscribe(params => {
      const roleId = params['id'];
      if (roleId) {
        this.activateEditMode(roleId);
      } else {
        this.activateCreateMode();
      }
    });

  }

  async save() {

    if (this.form.invalid) {
      return;
    }

    if (this.isEditMode) {
      await this.update();
    } else {
      await this.create();
    }
  }


  async del() {
    await this.roleService.del(this.currentRole);
    this.router.navigate(['roles']);
  }

  async cancel() {
    if (this.form.dirty) {
      await this.notificationService.showChangesConfirmation();
      // This would not return if the user cancels the 'cancel' confirmation :).
    }
    this.router.navigate(['/roles']);
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      id : [''],
      name: ['', Validators.required],
      permissions: ['', Validators.required]
    });
  }


  private async create() {
    await this.roleService.create(this.form.value);
    this.router.navigate(['roles']);
  }

  private async update() {
    await this.roleService.update(this.form.value);
    this.router.navigate(['roles']);
  }


  private activateCreateMode() {
    this.isEditMode = false;
    this.appService.setHeader('Roles', ['Access Control', 'Roles', 'Create Role']);
  }

  private async activateEditMode(roleId: string) {
    this.isEditMode = true;
    this.appService.setHeader('Roles', ['Access Control', 'Roles', 'Edit Role']);

    // Load Role
    this.currentRole = <Role> await this.roleService.findById(roleId);
    console.log('Loading role', this.currentRole);
    this.form.patchValue(this.currentRole);

    // If system role - disable controls so that user cannot change it.
    if (this.currentRole.system) {
      this.isSystemRole = true;
      this.form.disable();
    }
  }

}
