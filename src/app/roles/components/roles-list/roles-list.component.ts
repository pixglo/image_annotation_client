/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AppService} from '../../../app.service';
import {RoleService} from '../../role.service';
import {PageMetadata} from '../../../shared/model/paging/page-metadata';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, mergeMap} from 'rxjs/operators';
import {Role} from '../../model/role';
import {NotificationService} from '../../../shared/notification.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.scss']
})
export class RolesListComponent implements OnInit, OnDestroy {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  loading = true;
  content = [];
  page = PageMetadata.empty(10, 'name');

  // Search
  searchQuery: string;
  searchTextChanged = new Subject<string>();
  searchSubscription;

  constructor(
    private appService: AppService,
    private roleService: RoleService,
    private notificationService: NotificationService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.appService.setHeader('Roles', ['Access Control', 'Roles']);
  }

  async ngOnInit() {
    // Debounce Search
    this.searchSubscription = this.searchTextChanged
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .pipe(mergeMap(search => this.search(this.page.page)))
      .subscribe(() => {
      });
    this.search();
  }

  ngOnDestroy() {
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
  }

  async search(page = 1) {
    this.loading = true;
    try {
      const pageResponse = await this.roleService.search(this.searchQuery, page, this.page.size, this.page.sort);
      this.content = pageResponse.content;
      this.page = pageResponse.metadata;
    } finally {
      this.loading = false;
    }
  }

  async del(role: Role) {
    await this.roleService.del(role);
    this.search();
  }

  edit(role: Role) {
    this.router.navigate(['edit', role.id], {relativeTo: this.route});
  }

  triggerSearch($event) {
    // Called by the search bar
    this.searchTextChanged.next($event.target.value);
  }
}
