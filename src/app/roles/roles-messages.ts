/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
  'role.not.found' : 'Role (${value}) does not exist.',
  'role.in.use.by.users' : 'Cannot delete this role as it is being used by ${userCount} users.',
  'role.system.role.cannot.delete' : 'System roles cannot be deleted.',
  'role.name.already.exists' : 'Role name "${value}" is in use. Please use a different name.',

  'role.create.success' : 'Role was created successfully.',
  'role.create.success.title' : 'Role Created',
  'role.update.success' : 'Role was updated successfully.',
  'role.update.success.title' : 'Role Updated',

  'role.delete.confirm' : 'Are you sure to delete the role "${name}" ?',
  'role.delete.confirm.title' : 'Delete Role',
  'role.delete.success' : 'Role "${name}" was deleted successfully.',
  'role.delete.success.title' : 'Role Deleted',
};
