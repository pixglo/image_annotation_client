/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RolesRoutingModule} from './roles-routing.module';
import {LayoutModule} from '../layout/layout.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {RolesListComponent} from './components/roles-list/roles-list.component';
import {RoleCreateComponent} from './components/role-create/role-create.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MessageService} from '../shared/message.service';
import {messages} from './roles-messages';

@NgModule({
  declarations: [RolesListComponent, RoleCreateComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    SharedModule,
    RolesRoutingModule
  ]
})
export class RolesModule {

  constructor(
    private messageService: MessageService
  ) {
    this.messageService.register(messages);
  }
}
