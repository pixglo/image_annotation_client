/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {PageMetadata} from '../../model/paging/page-metadata';

@Component({
  selector: 'app-data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.scss']
})
export class DataGridComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  @Input() loading;
  @Input() content: any[];
  @Input() page: PageMetadata;

  @Output("page") pageChange = new EventEmitter(); // tslint:disable-line

  @ContentChild(TemplateRef) template: TemplateRef<any>;

  constructor() { }

  ngOnInit() {
  }


  loadPage(event) {

    console.log('Template', this.template);

    const nextPage = event.offset + 1; // Since our backend is 1 based
    console.log('Load Page: ' + nextPage);
    this.pageChange.emit(nextPage);
  }

}
