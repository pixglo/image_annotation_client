/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {MoverOptionGroup} from './mover-option-group';
import {MoverOption} from './mover-option';

/**
 * Wrapper class that holds a list of MoverOptionGroups.
 */
export class MoverOptionGroupList {

  public groups: MoverOptionGroup[] = [];

  constructor() {
  }

  /**
   * Adds an option to this list of grouped options.
   * @param option
   */
  public addOption(option: MoverOption) {

    // Find the right group for this option
    let group = this.getGroupByName(option.group);

    if (!group) {
      // Group doesn't exist - Create a new one
      group = new MoverOptionGroup(option.group);
      this.groups.push(group);
    }

    // Add to that group
    group.addOption(option);
  }

  /**
   * Remove option from this list.
   * @param option
   */
  public removeOption(option: MoverOption) {
    const group = this.getGroupByName(option.group);
    group.removeOption(option);
    if (group.options.length === 0) {
      this.removeGroup(group);
    }
  }

  /**
   * Returns all options contained in this grouped list.
   */
  public getAllOptions(): MoverOption[] {
    const options = [];
    this.groups.forEach((group) => {
      group.options.forEach((opt) => {
        options.push(opt);
      });
    });
    return options;
  }

  /**
   * Finds an option for the given ID if exists, and returns it. Otherwise returns null.
   * @param optionId
   */
  public findOptionById(optionId: string): MoverOption {
    for (const group of this.groups) {
      for (const option of group.options) {
        if (option.id === optionId) {
          return option;
        }
      }
    }
    return null;
  }

  /**
   * Sorts this grouped collection.
   */
  public sort() {
    this.groups.sort(this.compareGroups);
    this.groups.forEach((group) => group.sort());
  }
  /**
   * Clears this grouped list.
   */
  public clear() {
    this.groups = [];
  }

  /**
   * Clones this grouped list. This clone operation will duplicate the nested group structures as well.
   */
  public clone(): MoverOptionGroupList {
    const target = new MoverOptionGroupList();
    this.groups.forEach((group) => {
      target.groups.push(group.clone());
    });
    return target;
  }

  /**
   * Returns the group reference for the given group name if exists. Otherwise returns null.
   * @param name
   */
  private getGroupByName(name: string) {
    return this.groups.find((g) => g.name === name);
  }

  /**
   * Comparator function for sorting groups.
   * @param left
   * @param right
   */
  private compareGroups(left: MoverOptionGroup, right: MoverOptionGroup) {
    // Just the label for now
    return left.name.localeCompare(right.name);
  }

  private removeGroup(group: MoverOptionGroup) {
    const index = this.groups.map((g) => g.name).indexOf(group.name);
    this.groups.splice(index, 1);
  }
}
