/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {MoverOption} from './mover-option';

/**
 * Wrapper class for a single group of options.
 */
export class MoverOptionGroup {

  public options: MoverOption[] = [];

  constructor(public name: string) {
  }

  /**
   * Adds an option to this group.
   * @param option
   */
  public addOption(option: MoverOption) {
    this.options.push(option);
  }

  /**
   * Removes an option from this group.
   * @param option
   */
  public removeOption(option: MoverOption) {
    const index = this.options.map((opt) => opt.id).indexOf(option.id);
    if (index !== -1) {
      this.options.splice(index, 1);
    }
  }

  /**
   * Clones this group. This will create a copy of the nested array object as well.
   */
  public clone(): MoverOptionGroup {
    const target = new MoverOptionGroup(this.name);
    target.options = this.options.slice(0);
    return target;
  }

  /**
   * Sorts the options within this group.
   */
  public sort() {
    this.options.sort(this.compareOptions);
  }

  /**
   * Comparator function for sorting the options.
   * @param left
   * @param right
   */
  private compareOptions(left: MoverOption, right: MoverOption) {
    // Just the label for now
    return left.label.localeCompare(right.label);
  }
}
