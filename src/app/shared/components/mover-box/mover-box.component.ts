/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {MoverOption} from './model/mover-option';
import {MoverOptionGroupList} from './model/mover-option-group-list';

@Component({
  selector: 'app-mover-box',
  templateUrl: './mover-box.component.html',
  styleUrls: ['./mover-box.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR, useExisting: MoverBoxComponent, multi: true
    }
  ]
})
export class MoverBoxComponent implements OnInit, AfterViewInit, OnChanges, ControlValueAccessor {

  // Original options
  @Input('options') rawOptions: any[] = []; // tslint:disable-line

  @Input() title: string; // Title of what's contained in this (used for labeling).
  @Input() idField: string; // Name of the field that should be used as ID
  @Input() labelField: string; // Name of the field that should be used for the label
  @Input() groupField: string; // Optional. Grouping field.

  disabled: boolean; // Control disabled flag

  // Internal structures for rendering / tracking.
  allOptions: MoverOptionGroupList = new MoverOptionGroupList();

  selected: MoverOptionGroupList = new MoverOptionGroupList();
  available: MoverOptionGroupList = new MoverOptionGroupList();

  // Event callbacks of ControlValueAccessor
  private onChange: (value: string[]) => void;
  private onTouched: () => void;

  filterTerm$ = new Subject<string>();
  filterTerm: string;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.buildMoverOptions(this.rawOptions);
    this.filterTerm$
      .pipe(debounceTime(400))
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        this.filterTerm = value;
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.rawOptions) {
      this.buildMoverOptions(this.rawOptions);
    }
  }

  registerOnChange(onChange: (value: string[]) => void ) {
    this.onChange = onChange;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(selectedIds: string[]): void {

    if (! selectedIds) {
      selectedIds = [];
    }

    // Identify the MoverOption instances for the selected IDs.
    const selectedOptions = selectedIds.map((id) => this.allOptions.findOptionById(id));


    // Reset the control and set the selected values
    this.selected.clear();
    this.available = this.allOptions.clone();

    selectedOptions.forEach((opt) => {
      this.available.removeOption(opt);
      this.selected.addOption(opt);
    });

    this.selected.sort();
  }

  /**
   * Selects an option.
   * @param option
   */
  selectOption(option: MoverOption) {
    if (this.disabled) { return; }

    this.available.removeOption(option);
    this.selected.addOption(option);
    this.selected.sort();

    this.emitChanges();
    this.emitTouched();
  }

  /**
   * Deselects an option.
   * @param option
   */
  deselectOption(option: MoverOption) {
    if (this.disabled) { return; }


    this.selected.removeOption(option);
    this.available.addOption(option);
    this.available.sort();

    this.emitChanges();
    this.emitTouched();
  }

  /**
   * Selects all options.
   */
  selectAll() {
    if (this.disabled) { return; }

    this.selected = this.allOptions.clone();
    this.available.clear();

    this.emitChanges();
    this.emitTouched();
  }

  /**
   * Deselects all options.
   */
  deselectAll() {
    if (this.disabled) { return; }

    this.available = this.allOptions.clone();
    this.selected.clear();

    this.emitChanges();
    this.emitTouched();
  }

  /**
   * Emits the changes through the change listener.
   */
  private emitChanges() {
    if (this.onChange) {
      const selectedIds = this.selected.getAllOptions().map((opt) => opt.id);
      this.onChange(selectedIds);
    }

  }

  /**
   * Emits the touch event through the listener.
   */
  private emitTouched() {
    if (this.onTouched) {
      this.onTouched();
    }
  }

  /**
   * Builds the MoverOption instances out of the raw options given.
   * @param rawOptions
   */
  private buildMoverOptions(rawOptions: any[]) {

    const list = new MoverOptionGroupList();

    rawOptions.forEach((raw) => {
      const id = raw[this.idField];
      const label = raw[this.labelField];
      const group = this.groupField ? raw[this.groupField] : '';
      list.addOption(new MoverOption(id, label, group, raw));
    });

    this.allOptions = list;
    this.allOptions.sort();
    this.available = this.allOptions.clone();
  }

}
