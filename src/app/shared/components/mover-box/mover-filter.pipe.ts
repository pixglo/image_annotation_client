/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Pipe, PipeTransform} from '@angular/core';
import {MoverOptionGroupList} from './model/mover-option-group-list';
import {MoverOption} from './model/mover-option';

@Pipe({
  name: 'moverFilter',
  pure: false
})
export class MoverFilterPipe implements PipeTransform {

  transform(list: MoverOptionGroupList, filterTerm: string): MoverOptionGroupList {
    if (! filterTerm || filterTerm.trim().length === 0) {
      // Nothing to filter
      return list;
    }

    const filteredList = new MoverOptionGroupList();
    const lowerFilterText = filterTerm.toLowerCase();

    list.getAllOptions()
      .filter((opt) => this.isFilterMatch(lowerFilterText, opt))
      .forEach((opt) => {
        filteredList.addOption(opt);
      });

    return filteredList;
  }

  private isFilterMatch(filterText: string, opt: MoverOption) {
    return (opt.label.toLowerCase().indexOf(filterText) >= 0) || opt.group.toLowerCase().indexOf(filterText) >= 0;
  }

}
