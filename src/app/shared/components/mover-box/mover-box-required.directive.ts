/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Directive } from '@angular/core';
import {FormControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';

@Directive({
  selector: 'app-mover-box[required]', // tslint:disable-line
  providers: [
    {
      provide: NG_VALIDATORS, useExisting: MoverBoxRequiredDirective, multi: true
    }
  ]
})
export class MoverBoxRequiredDirective implements  Validator {

  constructor() { }

  validate(control: FormControl): ValidationErrors | null {

    if (! control.value || (<Array<any>>control.value).length === 0) {
      return {
        'required' : 'Please select a value.'
      };
    }
    return null;
  }



}
