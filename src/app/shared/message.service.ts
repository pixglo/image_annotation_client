/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import { messages } from './shared-messages';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private messageMap = new Map<string, string>();

  constructor() {
    // Register the shared messages
    this.register(messages);
  }

  /**
   * Allows modules to register their messages with the Message Service.
   * @param newMessages
   */
  register(newMessages): void {
    for (const code of Object.keys(newMessages)) {
      if (this.messageMap.has(code)) {
        console.warn(`Overriding message for code ${code}.` +
        `Previous Message: '${this.messageMap.get(code)}' | New Message: '${newMessages[code]}'`);
      }
      this.messageMap.set(code, newMessages[code]);
    }
  }

  /**
   * Returns the formatted message for the given code.
   * If the code was not found in the message map, returns the code as is.
   *
   * @param code Message code
   * @param placeholders Message placeholder values (if any).
   */
  getMessage(code: string, placeholders?: any) {
    const template = this.messageMap.get(code);

    if (! template) {
      console.warn(`Looked up for a message with code ${code} which is not known.`);
      return code;
    }
    return this.format(template, placeholders);
  }

  /**
   * A basic string interpolation implementation which replaces placeholders in the format ${XXX}
   * with relevant values from the placeholders map.
   * @param template
   * @param placeholders
   */
  private format(template: string, placeholders?: any) {
    if (! placeholders) { return template; }

    let formatted = template;
    for (const key of Object.keys(placeholders)) {
      formatted = formatted.replace(new RegExp(`\\$\\{${key}\\}`, 'g'), placeholders[key]);
    }
    return formatted;
  }


  /**
   * Returns the message title for a given message code. By convention, a message title should have the suffix
   * .title after the code name. If the title is not found, null will be returned.
   *
   * Ex. If the code is 'user.saved', then the title is expected to be in the same messages file with the code
   * 'user.saved.title'.
   *
   * @param code
   * @param placeholders
   */
  getMessageTitle(code: string, placeholders?: any) {
    const template = this.messageMap.get(`${code}.title`);

    if (! template) {
      return null;
    }

    return this.format(template, placeholders);
  }


}
