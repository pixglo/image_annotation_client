/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export class AppConfig {

    constructor(
        public environment = '',
        public version = '',
        public baseUrl = '',
        public applicationName = 'Pixglo Studio',
        public buildNumber = '',
        public buildTime = '',
        public gitCommit = '',
        public gitCommitTime = '',
        public applicationOrganization = '',
        public supportEmail = '',
        public maxFileSizeDefaultMB = 10,
        public maxFileSizeMediaMB = 20,
        public maxFileSizeZIPMB = 200,
        public uploadMediaPath = 'http://104.237.4.152:9171/uploads/',
        public allowImageMediaType = ['jpg', 'jpeg', 'png', 'zip'],
        public imageProcessStats = {
            'PENDING': { name: 'PENDING', class: 'primary' },
            'IN_PROGRESS': { name: 'IN_PROGRESS', class: 'danger' },
            'COMPLETED': { name: 'COMPLETED', class: 'success' }
        },
        public defaultImageStatus = imageProcessStats.PENDING,
        public datePickerFormat: String = 'DD/MM/YYYY'

    ) { }
}
