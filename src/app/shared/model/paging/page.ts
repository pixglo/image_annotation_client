/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {PageMetadata} from './page-metadata';

export class Page<T> {

  constructor(
    public content?: [T],
    public metadata?: PageMetadata
  ) {}


  static build <T> (jsonObj: any, itemTransformer: (x: any) => T): Page<T> {
    const items = jsonObj.content.map(itemObj => itemTransformer(itemObj));
    const metadata = PageMetadata.from(jsonObj.metadata);
    return new Page<T>(items, metadata);
  }
}
