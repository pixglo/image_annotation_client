/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export class PageMetadata {
  constructor(public total: number,
              public size: number,
              public page: number,
              public totalPages: number,
              public start: number,
              public end: number,
              public first: boolean,
              public last: boolean,
              public sort: string
  ) {
  }

  static empty(size = 10, sort = ''): PageMetadata {
    return new PageMetadata(
      0,
      size,
      1,
      0,
      0,
      0,
      false,
      false,
      sort
    );
  }



  static from(obj): PageMetadata {
    return new PageMetadata(
      obj.total,
      obj.size,
      obj.page,
      obj.totalPages,
      obj.start,
      obj.end,
      obj.first,
      obj.last,
      obj.sort
    );
  }
}
