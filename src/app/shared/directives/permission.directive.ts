/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { AuthenticationService } from '../../auth/authentication.service';
import { AuthenticatedUser } from '../../auth/model/authenticated-user';

@Directive({
    selector: '[app-permission]' // tslint:disable-line
})
export class PermissionDirective implements OnInit {

    private readonly SIDENAV_ROUTER_LINK_TAG = 'SIDENAV-ROUTER-LINK';

    /*
     * This directive allows us to enable / disable or show / hide buttons and other elements
     * based on the logged in user's permissions.
     */
    @Input('app-permission') permission: string;

    /*
     * Should be 'hide' (default) or 'disable.
     */
    @Input('app-permission-action') action = 'hide'; //tslint:disable-line

    private authUser: AuthenticatedUser;
    private element;

    constructor(
        private el: ElementRef,
        private renderer: Renderer2,
        private authService: AuthenticationService) {
        this.element = el;
        this.authUser = this.authService.getUser();
    }

    ngOnInit(): void {

        // TAG::CLIENT_MERGE Remove below code before client code merge
        return;
        if (!this.authUser.hasPermission(this.permission)) {
            if (this.action === 'hide') {
                this.hideElement();
            } else {
                this.disableElement();
            }
        }
    }

    private hideElement() {
        this.renderer.addClass(this.element.nativeElement, 'force-hidden');
    }

    private disableElement() {
        this.renderer.setAttribute(this.element.nativeElement, 'disabled', 'true');

        // Unsupported elements
        if (this.element.nativeElement.tagName === this.SIDENAV_ROUTER_LINK_TAG) {
            throw Error('Side Nav Router Links cannot be disabled. It should be made hidden');
        }
    }
}
