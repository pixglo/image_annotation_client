/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Directive, HostBinding, Self} from '@angular/core';
import {NgControl} from '@angular/forms';

/**
 * This directive automatically adds the is-invalid CSS class to any control that is in invalid
 * state after form validation. This triggers the Bootstrap 4 validation error messages to show up.
 */
@Directive({
  selector: '[formControlName],[ngModel],[formControl]' // tslint:disable-line
})
export class BootstrapValidationCssDirective {

  constructor(@Self() private cd: NgControl) {}

  @HostBinding('class.is-invalid')
  get isInvalid(): boolean {
    const control = this.cd.control;
    return control ? control.invalid && control.touched : false;
  }
}
