/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

export const messages = {
  'generic.error.title' : 'Unexpected Error',
  'generic.error' : 'Sorry, something went wrong and we couldn\'t complete your request.',
  'internal.error.title' : 'Unexpected Error',
  'internal.error' : 'Sorry, something went wrong and we couldn\'t complete your request.',
  'app.offline' : 'Sorry, we could not execute that operation. It seems you are offline.',
  'unsaved.changes' : 'Your unsaved changes will be lost if you proceed. Do you wish to continue ?',
  'unsaved.changes.title' : 'Unsaved Changes',
};
