/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppConfig } from '../model/config/app-config';
import { NotificationService } from '../notification.service';
import { MessageService } from '../message.service';
import { messages } from './filter-messages';
import { AppService } from '../../app.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-filter-view',
    templateUrl: './filter-view.component.html',
    styleUrls: ['./filter-view.component.scss']
})

export class FilterViewComponent implements OnInit {
    isFilterOpen = false;
    objFilter = [];
    objEleRef = [];
    objEleRefValues = [];
    objEleRefMixedVal = [];
    objStatusValues;
    isFilterApplied = false;
    dataFromStatic;
    bodayClass = 'hide';
    dateFormat;
    @Input() objHeader;
    @Output() filterResponse = new EventEmitter();

    configs = new AppConfig();

    appConfigSubscription: Subscription;

    constructor(
        private messageService: MessageService,
        private notificationService: NotificationService,
        private appService: AppService
    ) {
        this.objStatusValues = [
            { _id: true, name: 'Active' },
            { _id: false, name: 'In Active' }];
        this.dateFormat = this.configs.datePickerFormat;
        this.messageService.register(messages);

        this.appConfigSubscription = this.appService.appConfig$.subscribe((configs) => {
            this.configs = configs;
        });
    }

    ngOnInit() {
        this.setupFilterElement();
    }

    setupFilterElement() {
        this.dataFromStatic = false;
        // Setup Start date and End date for CREATE AT
        this.objFilter['startDate'] = '';
        this.objFilter['endDate'] = '';
        this.objFilter['publishStartDate'] = '';
        this.objFilter['publishEndDate'] = '';

        this.objHeader.forEach(item => {
            this.objFilter[item.val] = '';
            // Handle select option referance to populate data.
            if (item.elementRef) {
                this.objEleRef[item.val] = item.elementRef;
                this.objEleRefValues[item.elementRef] = item.elementData || [];
                if (item.elementData) {
                    this.dataFromStatic = true;
                }
            }
        });
    }

    toggleFilterNav() {
        this.isFilterOpen = !this.isFilterOpen;
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('hide');
        body.classList.remove('filter-show');
        this.bodayClass = 'hide';
        if (this.isFilterOpen) {
            this.bodayClass = 'filter-show';
            if (this.objFilter['createdAt']) {
                this.objFilter['startDate'] = this.objFilter['createdAt']['from'] ? (new Date(this.objFilter['createdAt']['from'])) : null;
                this.objFilter['endDate'] = this.objFilter['createdAt']['to'] ? (new Date(this.objFilter['createdAt']['to'])) : null;
            }
            if (this.objFilter['publishedAt']) {
                this.objFilter['publishStartDate'] =
                    this.objFilter['publishedAt']['from'] ? (new Date(this.objFilter['publishedAt']['from'])) : null;
                // tslint:disable-next-line:max-line-length
                this.objFilter['publishEndDate'] = this.objFilter['publishedAt']['to'] ? (new Date(this.objFilter['publishedAt']['to'])) : null;
            }
        }
        body.classList.add(this.bodayClass);
    }

    // Function to validate form and emit response to parent
    submitFilter() {
        this.objFilter = { ...this.objFilter };
        this.objHeader.forEach(item => {
            // except isActive filters removed, if value null
            if (item.val !== 'isActive' && !this.objFilter[item.val]) {
                delete this.objFilter[item.val];
            }

            // Filter for createdAt
            if (item.val === 'isActive' && typeof this.objFilter[item.val] !== 'boolean') {
                delete this.objFilter[item.val];
            } else if (item.val === 'createdAt' && (this.objFilter['startDate'] || this.objFilter['endDate'])) {
                this.objFilter[item.val] = {
                    from: this.objFilter['startDate'] ? (new Date(this.objFilter['startDate']).setHours(0, 0, 0, 0)) : null,
                    to: this.objFilter['endDate'] ? (new Date(this.objFilter['endDate']).setHours(23, 59, 59, 59)) : null
                };
            } else if (item.val === 'createdAt' && (this.objFilter['startDate'] === null || this.objFilter['endDate'] === null)) {
                delete this.objFilter[item.val];
            }

            // Filter for publishedAt
            if (item.val === 'isActive' && typeof this.objFilter[item.val] !== 'boolean') {
                delete this.objFilter[item.val];
            } else if (item.val === 'publishedAt' && (this.objFilter['publishStartDate'] || this.objFilter['publishEndDate'])) {
                this.objFilter[item.val] = {
                    from: this.objFilter['publishStartDate'] ? (new Date(this.objFilter['publishStartDate']).setHours(0, 0, 0, 0)) : null,
                    to: this.objFilter['publishEndDate'] ? (new Date(this.objFilter['publishEndDate']).setHours(23, 59, 59, 59)) : null
                };
                // tslint:disable-next-line:max-line-length
            } else if (item.val === 'publishedAt' && (this.objFilter['publishStartDate'] === null || this.objFilter['publishEndDate'] === null)) {
                delete this.objFilter[item.val];
            }
        });

        // Remove from start/end date from object
        delete this.objFilter['startDate'];
        delete this.objFilter['endDate'];
        delete this.objFilter['publishStartDate'];
        delete this.objFilter['publishEndDate'];
        if (Object.keys(this.objFilter).length === 0) {
            this.notificationService.showError('filter.select.invalid');
        } else {
            // Emit data to parent component
            this.filterResponse.emit(this.objFilter);
            this.toggleFilterNav();
            this.isFilterApplied = true;
        }
    }

    resetFilter(fromFront: boolean) {
        this.setupFilterElement();
        this.filterResponse.emit({});
        if (!fromFront) {
            this.toggleFilterNav();
        }
        this.isFilterApplied = false;
    }

    // search dropdowns
    getElementReferanceData(text, eleRef) {
        if (eleRef && !this.dataFromStatic) {
            const params = { q: text, sortBy: 'name', isparent: true };
            this.appService.getSearchFields(params, eleRef).subscribe((data) => {
                this.objEleRefValues[eleRef] = data['list'];
            }, (err) => {
                // Setup localization server massage
                this.notificationService.serverMessageHandler(err);
            });
        }
    }
    // removed duplicate.
    uniqueArray(arr1, arr2) {
        return this.uniqueArrayBuild(arr1, arr2);
    }

    public restrictKeyboard(event: KeyboardEvent) {
        event.returnValue = false;
        event.preventDefault();
    }

    // Use for return the array - merge two array and removed duplicate
    uniqueArrayBuild(a, b) {
        if (a && b && a.length === 0 && b.length === 0) {
            return [];
        } else if (a && a.length === 0) {
            return b;
        } else if (b && b.length === 0) {
            return a;
        }

        const temp = a.filter((el) => {
            return el._id === b[0]._id;
        });

        if (temp.length === 0) { a.push(b[0]); }
        return a;
    }
}
