/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootstrapValidationCssDirective } from './directives/bootstrap-validation-css.directive';
import { MoverBoxComponent } from './components/mover-box/mover-box.component';
import { MoverBoxRequiredDirective } from './components/mover-box/mover-box-required.directive';
import { MoverFilterPipe } from './components/mover-box/mover-filter.pipe';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataGridComponent } from './components/data-grid/data-grid.component';
import { DefaultErrorPageComponent } from './errors/pages/default-error-page/default-error-page.component';
import { NotFoundErrorPageComponent } from './errors/pages/not-found-error-page/not-found-error-page.component';
import { PermissionDirective } from './directives/permission.directive';
import { AccessDeniedPageComponent } from './errors/pages/access-denied-page/access-denied-page.component';
import { SidenavGroupComponent } from './sidenav/sidenav-group/sidenav-group.component';
import { FileSizePipe } from './pipes/file-size.pipe';
import { DurationPipe } from './pipes/duration.pipe';
import { BitratePipe } from './pipes/bitrate.pipe';
import { FilterViewComponent } from './filter-view/filter-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex'; // https://www.npmjs.com/package/ngx-select-ex
import { BsDatepickerModule } from 'ngx-bootstrap';

const CustomSelectOptions: INgxSelectOptions = { // Check the interface for more options
    optionValueField: '_id',
    keepSelectedItems: false,
    optionTextField: 'name'
};
@NgModule({
    declarations: [
        BootstrapValidationCssDirective,
        MoverBoxComponent,
        MoverBoxRequiredDirective,
        MoverFilterPipe,
        DataGridComponent,
        DefaultErrorPageComponent,
        NotFoundErrorPageComponent,
        PermissionDirective,
        AccessDeniedPageComponent,
        SidenavGroupComponent,
        FileSizePipe,
        DurationPipe,
        BitratePipe,
        FilterViewComponent
    ],
    imports: [
        CommonModule,
        NgxDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSelectModule.forRoot(CustomSelectOptions),
        BsDatepickerModule.forRoot()
    ],
    exports: [
        BootstrapValidationCssDirective,
        MoverBoxRequiredDirective,
        PermissionDirective,
        MoverBoxComponent,
        DataGridComponent,
        DefaultErrorPageComponent,
        SidenavGroupComponent,
        FileSizePipe,
        DurationPipe,
        BitratePipe,
        FilterViewComponent
    ]
})
export class SharedModule {
}
