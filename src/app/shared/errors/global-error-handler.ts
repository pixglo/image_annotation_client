/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationService} from '../notification.service';
import {Router} from '@angular/router';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) {
  }

  handleError(sourceError: Error | HttpErrorResponse): void {

    // This is to handle Promise rejection errors. In case of those, we have to get the original error
    // via 'rejection' field.
    const error = sourceError['rejection'] || sourceError;

    if (error instanceof HttpErrorResponse) {
      this.handleHttpErrorResponse(error);
    } else {
      this.handleClientSideError(error);
    }

    console.error('[Global Error Handler] Observed Error: ', error);
  }

  private handleHttpErrorResponse(error: HttpErrorResponse) {
    console.log('Handling server side error');

    if (this.isApiErrorResponse(error)) {
      // API error responses
      const apiError = error.error;
      this.getNotificationService().showError(apiError.code, apiError.details);
    } else {
      if (!navigator.onLine) {
        // Connection errors
        this.getNotificationService().showOfflineError();
      } else if (error.status === 504) {
        // Gateway Timeout
        // TODO Handle server down?
        this.getNotificationService().showServiceUnavailableError();

      } else if (error.status === 400 && error.error && error.error.errors && error.error.errors[0]) {
        if(error.error.errors[0] && error.error.errors[0].msg){
          const msg = error.error.errors[0].msg;
          const dataCount = error.error.errors[0].data;
          this.getNotificationService().showCustomError(msg, dataCount);
        }else {
          this.getNotificationService().showGenericError();
        }
      } else {
        // All other HTTP errors
        this.getNotificationService().showGenericError();
      }
    }
  }

  private handleClientSideError(error: Error) {
    console.log('Handling client side error', error);
    this.getRouter().navigate(['error']);
  }

  private isApiErrorResponse(error: HttpErrorResponse) {
    return error.error && error.error.code;
  }

  private getNotificationService(): NotificationService {
    return this.injector.get(NotificationService);
  }

  private getRouter(): Router {
    return this.injector.get(Router);
  }
}
