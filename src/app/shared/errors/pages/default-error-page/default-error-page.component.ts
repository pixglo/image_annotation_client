/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-default-error-page',
  templateUrl: './default-error-page.component.html',
  styleUrls: ['./default-error-page.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class DefaultErrorPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
