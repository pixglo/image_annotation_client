/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {AfterViewInit, ChangeDetectorRef, Component, ContentChildren, Input, QueryList} from '@angular/core';
import {SidenavRouterLinkComponent} from '../../../../vendor/libs/sidenav/sidenav-router-link.component';

@Component({
  selector: 'app-sidenav-group',
  templateUrl: './sidenav-group.component.html',
  styleUrls: ['./sidenav-group.component.scss']
})
export class SidenavGroupComponent implements AfterViewInit {

  private readonly forceHiddenClass = 'force-hidden';

  @Input()
  title: string;

  hidden = false;

  @ContentChildren(SidenavRouterLinkComponent)
  childLinks: QueryList<SidenavRouterLinkComponent>;

  constructor(private detector: ChangeDetectorRef) { }

  ngAfterViewInit(): void {
    // Count the number of visible child elements
    const visibleCount = this.childLinks.map((link: SidenavRouterLinkComponent) => {
      // If the element has the 'force-hidden' class (added by permissions directive), then we consider it to be hidden
      const classes: DOMTokenList = link.elementRef.nativeElement.classList;
      return <number> (classes.contains(this.forceHiddenClass) ? 0 : 1);
    }).reduce((previousValue, currentValue) => {
      return previousValue + currentValue;
    });

    if (visibleCount === 0) {
      // All children are hidden. Hide self too.
      console.log('Hiding : ', this.title);
      this.hidden = true;

      // We have to fire Angular change detection to reflect the changes since we are in after view init
      if (this.detector && this.detector.detectChanges) {
        this.detector.detectChanges();
      }
    }

  }
}
