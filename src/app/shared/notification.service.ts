/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { MessageService } from './message.service';

// tslint:disable-next-line
const appSwal = Swal.mixin({
    confirmButtonClass: 'btn btn-primary',
    cancelButtonClass: 'btn btn-default',
    customClass: 'app-notification',
    buttonsStyling: false,
});

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    // How long a toast-type message should be displayed before auto-closing
    private readonly toastTimerMs = 2000;

    constructor(
        private messageService: MessageService
    ) {
    }

    showGenericError(): Promise<any> {
        return this.showError('generic.error');
    }

    showCustomError(message, dataCount): Promise<any> {
        // const message = this.messageService.getMessage(msg, dataCount);
        return appSwal({
            title: 'Error Message',
            type: 'error',
            html: message,
            showConfirmButton: true
        });
    }

    

    showSuccess(code: string, placeholders?: any, confirm = false): Promise<any> {

        const title = this.messageService.getMessageTitle(code) || 'Success';
        const message = this.messageService.getMessage(code, placeholders);
        const customClasses = confirm ? 'app-notification' : 'app-notification toast-mode';

        return appSwal({
            title: title,
            type: 'success',
            html: message,
            customClass: customClasses,
            showConfirmButton: confirm,
            timer: confirm ? null : this.toastTimerMs
        });
    }

    showDeleteConfirmation(code: string, placeholders?: any): Promise<boolean> {
        const title = this.messageService.getMessageTitle(code) || 'Delete';
        const message = this.messageService.getMessage(code, placeholders);

        // TODO : Custom Icon for Message? Button Icon for Delete Action?

        return new Promise<boolean>((resolve) => {
            appSwal({
                title: title,
                type: 'question',
                html: message,
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger',
                confirmButtonText: 'Delete'
            }).then((result) => {
                if (result.value) {
                    resolve(true);
                }
            });
        });
    }


    /**
     * Shows a Yes / No confirmation dialog.
     */
    showYesNoConfirmation(code: string, placeholders?: any): Promise<boolean> {

        const title = this.messageService.getMessageTitle(code) || 'Confirm Action';
        const message = this.messageService.getMessage(code, placeholders);

        return new Promise<boolean>((resolve) => {
            appSwal({
                title: title,
                type: 'question',
                html: message,
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    resolve(true);
                }
            });
        });
    }

    /**
     * Shows a Changes will be lost message.
     */
    showChangesConfirmation(): Promise<boolean> {
        const code = 'unsaved.changes';
        return this.showYesNoConfirmation(code);
    }

    showError(code: string, placeholders?: any): Promise<any> {

        const title = this.messageService.getMessageTitle(code, placeholders);
        const message = this.messageService.getMessage(code, placeholders);

        console.log('[Notifications] Error :', message);

        return appSwal({
            title: title,
            type: 'error',
            html: message,
            showConfirmButton: true
        });
    }

    showOfflineError() {
        const message = this.messageService.getMessage('app.offline');
        console.log('[Notifications] Offline Error : ' + message);
        //  TODO Implement
    }

    showServiceUnavailableError() {
        const message = this.messageService.getMessage('app.unavailable');
        console.log('[Notifications] Service Unavailable Error : ' + message);
        //  TODO Implement
    }

    serverMessageHandler(message) {
        return appSwal({
            title: 'Server Error',
            type: 'error',
            html: message,
            showConfirmButton: true
        });
    }
}
