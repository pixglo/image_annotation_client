/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {
    Component,
    Input,
    ChangeDetectionStrategy,
    AfterViewInit,
    HostBinding,
    OnInit,
    ElementRef,
    ViewChild, Renderer2
} from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../../app.service';
import { LayoutService } from '../layout.service';
import { TenantService } from '../../tenants/tenant.service';

@Component({
    selector: 'app-layout-sidenav',
    templateUrl: './layout-sidenav.component.html',
    styles: [':host { display: block; } .app-brand { background-color: #ffffff !important;}'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutSidenavComponent implements OnInit, AfterViewInit {
    @Input() orientation = 'vertical';

    @HostBinding('class.layout-sidenav') private hostClassVertical = false;
    @HostBinding('class.layout-sidenav-horizontal') private hostClassHorizontal = false;
    @HostBinding('class.flex-grow-0') private hostClassFlex = false;

    private authUser;
    private imgUrl;

    @ViewChild('logo', { read: ElementRef }) private logo: ElementRef;
    @ViewChild('logo_admin', { read: ElementRef }) private logoAdmin: ElementRef;


    constructor(
        private router: Router,
        private appService: AppService,
        private layoutService: LayoutService,
        private tenantService: TenantService,
        private renderer: Renderer2
    ) {
        // Set host classes
        this.hostClassVertical = this.orientation !== 'horizontal';
        this.hostClassHorizontal = !this.hostClassVertical;
        this.hostClassFlex = this.hostClassHorizontal;
    }

    async ngOnInit() {
        this.authUser = JSON.parse(localStorage.getItem('app-auth-user'));

        if (this.authUser.tenantId !== null) {
            this.imgUrl = `/v1/tenants/${this.authUser.tenantId}/logo`;
        } else {
            this.imgUrl = 'assets/img/logos/pixglo_100.png';
        }
    }

    ngAfterViewInit() {
        // Safari bugfix
        this.layoutService._redrawLayoutSidenav();
    }

    getClasses() {
        let bg = this.appService.layoutSidenavBg;

        if (this.orientation === 'horizontal' && (bg.indexOf(' sidenav-dark') !== -1 || bg.indexOf(' sidenav-light') !== -1)) {
            bg = bg
                .replace(' sidenav-dark', '')
                .replace(' sidenav-light', '')
                .replace('-darker', '')
                .replace('-dark', '');
        }

        return `${this.orientation === 'horizontal' ? 'container-p-x ' : ''} bg-${bg}`;
    }

    isActive(url) {
        return this.router.isActive(url, true);
    }

    isMenuActive(url) {
        return this.router.isActive(url, false);
    }

    isMenuOpen(url) {
        return this.router.isActive(url, false) && this.orientation !== 'horizontal';
    }

    toggleSidenav() {
        this.layoutService.toggleCollapsed();
    }

}
