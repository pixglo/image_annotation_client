/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import {Component, HostBinding, OnDestroy} from '@angular/core';
import { AppService } from '../../app.service';
import {Subscription} from 'rxjs';
import {AppConfig} from '../../shared/model/config/app-config';

@Component({
  selector: 'app-layout-footer',
  templateUrl: './layout-footer.component.html',
  styles: [':host { display: block; }']
})
export class LayoutFooterComponent implements OnDestroy {

  @HostBinding('class.layout-footer') private hostClassMain = true;

  appConfigSubscription: Subscription;
  config = new AppConfig();
  year = new Date().getFullYear();

  constructor(private appService: AppService) {
    this.appConfigSubscription = this.appService.appConfig$.subscribe((config) => {
      this.config = config;
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigSubscription) {
      this.appConfigSubscription.unsubscribe();
    }
  }

  currentBg() {
    return `bg-${this.appService.layoutFooterBg}`;
  }
}
