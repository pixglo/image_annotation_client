/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { Component, Input, HostBinding, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import { LayoutService } from '../../layout/layout.service';
import { AuthenticationService } from '../../auth/authentication.service';
import { AuthenticatedUser } from '../../auth/model/authenticated-user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout-navbar',
  templateUrl: './layout-navbar.component.html',
  styles: [':host { display: block; }']
})
export class LayoutNavbarComponent implements OnInit {

  isExpanded = false;
  isRTL: boolean;
  authenticatedUser: AuthenticatedUser;
  currentHeader = [];
  tenantId: string;
  @Input() sidenavToggle = true;

  @HostBinding('class.layout-navbar') private hostClassMain = true;

  constructor(
    private appService: AppService,
    private layoutService: LayoutService,
    private router: Router,
    private authService: AuthenticationService
  ) {
    this.isRTL = appService.isRTL;
    console.log('Subscribing to header change');
    this.appService.headerChangeSubject.subscribe((headerValues) => {
      console.log('Updating header to ', headerValues);
      this.currentHeader = headerValues;
    });
  }

  ngOnInit(): void {
    this.authenticatedUser = this.authService.getUser();
    this.tenantId = this.authenticatedUser.tenantId;
  }

  currentBg() {
    return `bg-${this.appService.layoutNavbarBg}`;
  }

  toggleSidenav() {
    this.layoutService.toggleCollapsed();
  }

  async signOut() {
    console.log('Signing out...');
    await this.authService.signOut();
    this.router.navigate(['/sign-in']);
  }

  viewMyProfile() {
    this.router.navigate([`/users/profile`, this.authenticatedUser.userId ]);
  }

  viewTenantProfile() {
    this.router.navigate(['/tenants', this.tenantId, 'profile']);
  }

}
