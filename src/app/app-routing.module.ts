/**********************************************************************************************************************
 * Copyright (C) 2018 Pixglo Inc - All Rights Reserved                                                                *
 *                                                                                                                    *
 * CONFIDENTIAL                                                                                                       *
 *                                                                                                                    *
 * All information contained herein is, and remains the property of Pixglo Inc and its partners,                      *
 * if any.  The intellectual and technical concepts contained herein are proprietary to Pixglo Inc  and its           *
 * partners and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or  *
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless     *
 * prior written permission is obtained from Pixglo Inc.                                                              *
 *                                                                                                                    *
 **********************************************************************************************************************/

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { Layout2Component } from './layout/layout-2/layout-2.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth/auth.guard';
import { DefaultErrorPageComponent } from './shared/errors/pages/default-error-page/default-error-page.component';
import { NotFoundErrorPageComponent } from './shared/errors/pages/not-found-error-page/not-found-error-page.component';
import { AccessDeniedPageComponent } from './shared/errors/pages/access-denied-page/access-denied-page.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: Layout2Component,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        children: [
            { path: '', component: HomeComponent },
        ]
    },
    {
        path: 'media',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './media/media.module#MediaModule'
    },
    {
        path: 'tenants',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './tenants/tenants.module#TenantsModule'
    },

    {
        path: 'users',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './users/users.module#UsersModule'
    },
    {
        path: 'roles',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './roles/roles.module#RolesModule'
    },
    {
        path: 'templates',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './templates/templates.module#TemplatesModule'
    },
    {
        path: 'executions',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './executions/executions.module#ExecutionsModule'
    },
    {
        path: 'rendering-jobs',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './rendering-jobs/rendering-jobs.module#RenderingJobsModule'
    },
    {
        path: 'tags',
        // canActivate: [AuthGuard],
        // canActivateChild: [AuthGuard],
        loadChildren: './tags/tags.module#TagsModule'
    },
    {
        path: 'projects',
        // canActivate: [AuthGuard],
        // canActivateChild: [AuthGuard],
        loadChildren: './projects/projects.module#ProjectsModule'
    },
    {
        path: 'error',
        component: DefaultErrorPageComponent
    },
    {
        path: 'access-denied',
        component: AccessDeniedPageComponent
    },
    {
        path: '**',
        component: NotFoundErrorPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        preloadingStrategy: PreloadAllModules
    })],
    exports: [RouterModule]
})

export class AppRoutingModule { }
