const express = require('express');
const path = require('path');
const fs = require('fs');
const app = express();
if (!fs.existsSync(path.join(__dirname, 'server_conf.json'))) {
    fs.writeFileSync(path.join(__dirname, 'server_conf.json'), '{"port": 9171}');
}
const conf = JSON.parse(fs.readFileSync(path.join(__dirname, 'server_conf.json')).toString());
app.use('/', express.static(path.join(__dirname, '/dist')));
app.use((req, res, next) => {
    return res.sendFile(path.join(__dirname, '/dist/index.html'));
});
app.listen(conf.port, '0.0.0.0', () => {
    console.log(`Server started listening on http://0.0.0.0:${conf.port}/`)
});